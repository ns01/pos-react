import * as types from './types'


export const createCustomer = (
  name,
  email,
  number,
  note,
) => ({
  type: types.CREATE_CUSTOMER,
  name,
  email,
  number,
  note,
})

export const createCustomerFail = (data = {}, message = '', code = null) => ({
  type: types.CREATE_CUSTOMER_FAIL,
  error: {
    data,
    message,
    code,
  },
})

export const createCustomerSuccess = customer => ({
  type: types.CREATE_CUSTOMER_SUCCESS,
  customer,
})

export const createCustomerReset = () => ({
  type: types.CREATE_CUSTOMER_RESET,
})

export const changeFormName = name => ({
  type: types.CHANGE_FORM_NAME,
  name,
})

export const changeFormEmail = email => ({
  type: types.CHANGE_FORM_EMAIL,
  email,
})

export const changeFormNumber = number => ({
  type: types.CHANGE_FORM_NUMBER,
  number,
})

export const changeFormNote = note => ({
  type: types.CHANGE_FORM_NOTE,
  note,
})

export const openAddCustomerDialog = () => ({
  type: types.OPEN_CREATE_CUSTOMER_DIALOG,
})

export const closeAddCustomerDialog = () => ({
  type: types.CLOSE_CREATE_CUSTOMER_DIALOG,
})
