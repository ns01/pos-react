// Core
import api from 'posModules/app/axios'
import { Observable } from 'rxjs'
import { mergeMap, debounceTime } from 'rxjs/operators'
import { ofType } from 'redux-observable'
import camelcaseKeys from 'camelcase-keys'
import { PhoneNumberUtil } from 'google-libphonenumber'
// Operations
import { createCustomerSuccess, createCustomerFail } from '../operations'

// Types
import { CREATE_CUSTOMER } from '../types';

const createCustomerEpic = (action$, state$) => action$.pipe(
  ofType(CREATE_CUSTOMER),
  debounceTime(1000),
  mergeMap(action => (
    Observable.create((observer) => {
      const formErrors = {}

      if (action.name.trim() === '') {
        formErrors.name = 'name is required'
      }

      if (action.number !== '') {
        const phoneUtil = PhoneNumberUtil.getInstance()
        const number = phoneUtil.parse(action.number, state$.value.userData.user.country)

        if (!phoneUtil.isValidNumber(number)) {
          formErrors.number = 'number is invalid'
        }
      }

      if (Object.getOwnPropertyNames(formErrors).length > 0) {
        observer.next(createCustomerFail(formErrors))
        observer.complete()
        return
      }

      api.post('/v1/users/customers/', {
        name: action.name,
        email: action.email,
        number: action.number,
        note: action.note,
      }).then((response) => {
        const customer = camelcaseKeys(response.data.data, { deep: true })

        observer.next(createCustomerSuccess(customer))
        observer.complete()
      }).catch((error) => {
        const errorResponse = camelcaseKeys(error.response.data.errors, { deep: true })

        const errors = {
          data: errorResponse.data || {},
          message: errorResponse.message,
          code: errorResponse.code,
        }

        observer.next(createCustomerFail(errors.data, errors.message, errors.code))
        observer.complete()
      })
    })
  )),
)

export default createCustomerEpic
