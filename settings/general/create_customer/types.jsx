export const CREATE_CUSTOMER = 'pos/customers/create_customer/CREATE_CUSTOMER'
export const CREATE_CUSTOMER_FAIL = 'pos/customers/create_customer/CREATE_CUSTOMER_FAIL'
export const CREATE_CUSTOMER_SUCCESS = 'pos/customers/create_customer/CREATE_CUSTOMER_SUCCESS'
export const CREATE_CUSTOMER_RESET = 'pos/customers/create_customer/CREATE_CUSTOMER_RESET'

// Inputs
export const CHANGE_FORM_NAME = 'pos/customers/create_customer/CHANGE_FORM_NAME'
export const CHANGE_FORM_EMAIL = 'pos/customers/create_customer/CHANGE_FORM_EMAIL'
export const CHANGE_FORM_NUMBER = 'pos/customers/create_customer/CHANGE_FORM_NUMBER'
export const CHANGE_FORM_NOTE = 'pos/customers/create_customer/CHANGE_FORM_NOTE'

export const OPEN_CREATE_CUSTOMER_DIALOG = 'pos/customers/create_customer/OPEN_CREATE_CUSTOMER_DIALOG'
export const CLOSE_CREATE_CUSTOMER_DIALOG = 'pos/customers/create_customer/CLOSE_CREATE_CUSTOMER_DIALOG'
