// Core
import reduceReducers from 'reduce-reducers'

// Action types
import * as types from './types'


const createCustomerReducer = (state, action) => {
  switch (action.type) {
    case types.CREATE_CUSTOMER:
      return {
        ...state,

        createCustomerForm: {
          ...state.createCustomerForm,

          error: {
            message: '',
            data: {},
            code: null,
          },

          creatingCustomer: true,
          createCustomerFailed: false,
          createCustomerSucceeded: false,
        },
      }
    case types.CREATE_CUSTOMER_FAIL:
      return {
        ...state,

        createCustomerForm: {
          ...state.createCustomerForm,

          error: {
            message: action.error.message,
            data: action.error.data,
            code: action.error.code,
          },

          creatingCustomer: false,
          createCustomerFailed: true,
          createCustomerSucceeded: false,
        },
      }
    case types.CREATE_CUSTOMER_SUCCESS: {
      const { result } = state.customersData.customers

      result.unshift(action.customer.id)

      return {
        ...state,

        customersData: {
          ...state.customersData,

          customers: {
            data: {
              ...state.customersData.customers.data,

              [action.customer.id]: action.customer,
            },

            result,
          },
        },

        createCustomerForm: {
          ...state.createCustomerForm,

          error: {
            message: '',
            data: {},
            code: null,
          },

          creatingCustomer: false,
          createCustomerFailed: false,
          createCustomerSucceeded: true,
        },
      }
    }
    case types.CREATE_CUSTOMER_RESET:
      return {
        ...state,

        createCustomerForm: {
          isAddCustomerDialogOpened: false,

          name: '',
          email: '',
          number: '',
          note: '',

          error: {
            message: '',
            data: {},
            code: null,
          },

          creatingCustomer: false,
          createCustomerFailed: false,
          createCustomerSucceeded: false,
        },
      }
    case types.CHANGE_FORM_NAME:
      return {
        ...state,

        createCustomerForm: {
          ...state.createCustomerForm,

          name: action.name,
        },
      }
    case types.CHANGE_FORM_EMAIL:
      return {
        ...state,

        createCustomerForm: {
          ...state.createCustomerForm,

          email: action.email,
        },
      }
    case types.CHANGE_FORM_NUMBER:
      return {
        ...state,

        createCustomerForm: {
          ...state.createCustomerForm,

          number: action.number,
        },
      }
    case types.CHANGE_FORM_NOTE:
      return {
        ...state,

        createCustomerForm: {
          ...state.createCustomerForm,

          note: action.note,
        },
      }
    case types.OPEN_CREATE_CUSTOMER_DIALOG:
      return {
        ...state,

        createCustomerForm: {
          ...state.createCustomerForm,

          isAddCustomerDialogOpened: true,
        },
      }
    case types.CLOSE_CREATE_CUSTOMER_DIALOG:
      return {
        ...state,

        createCustomerForm: {
          ...state.createCustomerForm,

          isAddCustomerDialogOpened: false,
        },
      }
    default:
      return state
  }
}


export default reduceReducers(createCustomerReducer)
