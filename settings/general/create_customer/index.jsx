import reducer from './reducers'

import * as createCustomerOperations from './operations'


export { createCustomerOperations }

export default reducer
