// Core
import reduceReducers from 'reduce-reducers'

// Action types
import * as types from './types'


const readUserEmailReducer = (state, action) => {
  switch (action.type) {
    case types.READ_USER_EMAIL:
      return {
        ...state,

        userData: {
          ...state.userData,

          error: {
            message: '',
            data: {},
            code: null,
          },

          readingUserEmail: true,
          readUserEmailFailed: false,
          readUserEmailSucceeded: false,
        },
      }
    case types.READ_USER_EMAIL_FAIL:
      return {
        ...state,

        userData: {
          ...state.userData,

          error: {
            message: action.errors.message,
            data: action.errors.data,
            code: action.errors.code,
          },

          readingUserEmail: false,
          readUserEmailFailed: true,
          readUserEmailSucceeded: false,
        },
      }
    case types.READ_USER_EMAIL_SUCCESS:
      return {
        ...state,

        userData: {
          ...state.userData,

          user: {
            result: action.userData.result,
            data: action.userData.data,
          },

          error: {
            message: '',
            data: {},
            code: null,
          },

          readingUserEmail: false,
          readUserEmailFailed: false,
          readUserEmailSucceeded: true,
        },
      }
    default:
      return state
  }
}


export default reduceReducers(readUserEmailReducer)
