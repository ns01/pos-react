export const READ_USER_EMAIL = 'settings/general/read_user_email/READ_USER_EMAIL'
export const READ_USER_EMAIL_FAIL = 'settings/general/read_user_email/READ_USER_EMAIL_FAIL'
export const READ_USER_EMAIL_SUCCESS = 'settings/general/read_user_email/READ_USER_EMAIL_SUCCESS'
