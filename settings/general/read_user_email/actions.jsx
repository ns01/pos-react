import * as types from './types'


export const readUserEmail = () => ({
  type: types.READ_USER_EMAIL,
})


export const readUserEmailFail = (data = {}, message = '', code = null) => ({
  type: types.READ_USER_EMAIL_FAIL,
  errors: {
    data,
    message,
    code,
  },
})


export const readUserEmailSuccess = userData => ({
  type: types.READ_USER_EMAIL_SUCCESS,
  userData,
})
