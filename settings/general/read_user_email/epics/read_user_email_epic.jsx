// Core
import api from 'posModules/app/axios'
import { Observable } from 'rxjs'
import { mergeMap } from 'rxjs/operators'
import { ofType } from 'redux-observable'
import normalize from 'posModules/app/helper'
import camelcaseKeys from 'camelcase-keys'

// Types
import { READ_USER_EMAIL } from '../types';

// Operations
import { readUserEmailSuccess, readUserEmailFail } from '../operations'


const readUserEmailEpic = action$ => action$.pipe(
  ofType(READ_USER_EMAIL),
  mergeMap(() => (
    Observable.create((observer) => {
     const { userId } = state$.value.userData

      api.get(`/v1/settings/general/email/${userId}`).then((response) => {
      // api.get('/v1/settings/general/email').then((response) => {
        const user = camelcaseKeys(response.data.data, { deep: true })
        const userData = normalize(user, 'id')

        observer.next(readUserEmailSuccess(userData))
        observer.complete()
      }).catch((error) => {
        const errorResponse = camelcaseKeys(error.response.data.errors, { deep: true })

        const errors = {
          data: errorResponse.data || {},
          message: errorResponse.message,
          code: errorResponse.code,
        }

        observer.next(readUserEmailFail(errors.data, errors.message, errors.code))
        observer.complete()
      })
    })
  )),
)

export default readUserEmailEpic
