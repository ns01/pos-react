// Core
import reduceReducers from 'reduce-reducers'

// Reducers
import readUserEmailReducer from './read_user_email'
import updateUserEmailReducer from './update_user_email'
import updateBusinessNameReducer from './update_business_name'


// State shape
const initialState = {
  userData: {
    // Data
    email: {
      result: [],
      data: {},
    },

    businessName: {
      result: [],
      data: {},
    },

    // Error
    error: {
      message: '',
      data: {},
      code: null,
    },

    // Status
  //  readingUserEmail: false,
  //  readUserEmailFailed: false,
  //  readUserEmailSucceeded: false,
  },

  updateBusinessNameForm: {
    isUpdateBusinessNameDialogOpened: false,

    // Input
    // user: {},
    user: {},
    email: '',
    businessName: '',

    // Error
    error: {
      message: '',
      data: {},
      code: null,
    },

    // Status
    updatingUserEmail: false,
    updateUserEmailFailed: false,
    updateUserEmailSucceeded: false,
    updatingBusinessName: false,
    updateBusinessNameFailed: false,
    updateBusinessNameSucceeded: false,
  },

}

// Initial reducer
const initialReducer = (state = initialState) => (
  state
)

export default reduceReducers(
  initialReducer,
  readUserEmailReducer,
  updateUserEmailReducer,
  updateBusinessNameReducer,
)
