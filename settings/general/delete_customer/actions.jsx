import * as types from './types'


export const deleteCustomer = customerId => ({
  type: types.DELETE_CUSTOMER,
  customerId,
})

export const deleteCustomerFail = (data = {}, message = '', code = null) => ({
  type: types.DELETE_CUSTOMER_FAIL,
  error: {
    data,
    message,
    code,
  },
})

export const deleteCustomerSuccess = customerId => ({
  type: types.DELETE_CUSTOMER_SUCCESS,
  customerId,
})

export const deleteCustomerReset = () => ({
  type: types.DELETE_CUSTOMER_RESET,
})

export const setCustomer = customer => ({
  type: types.SET_CUSTOMER,
  customer,
})

export const openDeleteCustomerDialog = () => ({
  type: types.OPEN_DELETE_CUSTOMER_DIALOG,
})

export const closeDeleteCustomerDialog = () => ({
  type: types.CLOSE_DELETE_CUSTOMER_DIALOG,
})
