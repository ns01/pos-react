import {
  deleteCustomer,
  deleteCustomerFail,
  deleteCustomerSuccess,
  deleteCustomerReset,

  setCustomer,

  openDeleteCustomerDialog,
  closeDeleteCustomerDialog,
} from './actions'


export {
  deleteCustomer,
  deleteCustomerFail,
  deleteCustomerSuccess,
  deleteCustomerReset,

  setCustomer,

  openDeleteCustomerDialog,
  closeDeleteCustomerDialog,
}
