// Core
import api from 'posModules/app/axios'
import { Observable } from 'rxjs'
import { mergeMap, debounceTime } from 'rxjs/operators'
import { ofType } from 'redux-observable'
import camelcaseKeys from 'camelcase-keys'

// Operations
import { updateUserEmailSuccess, updateUserEmailFail } from '../operations'

// Types
import { UPDATE_USER_EMAIL } from '../types';


const updateUserEmailEpic = action$ => action$.pipe(
  ofType(UPDATE_USER_EMAIL),
  debounceTime(1000),
  mergeMap(action => (
    Observable.create((observer) => {
      const formErrors = {}

      if (Object.getOwnPropertyNames(formErrors).length > 0) {
        observer.next(updateUserEmailFail(formErrors))
        observer.complete()
        return
      }

      api.put(`/v1/settings/general/email/${action.userId}`, {
        email: action.email,
      }).then((response) => {
        const user = camelcaseKeys(response.data.data, { deep: true })

        observer.next(updateUserEmailSuccess(user))
        observer.complete()
      }).catch((error) => {
        const errorResponse = camelcaseKeys(error.response.data.errors, { deep: true })

        const errors = {
          data: errorResponse.data || {},
          message: errorResponse.message,
          code: errorResponse.code,
        }

        observer.next(updateUserEmailFail(errors.data, errors.message, errors.code))
        observer.complete()
      })
    })
  )),
)

export default updateUserEmailEpic
