import * as types from './types'


export const updateUserEmail = (
  userId,

  email,
  // number,
  // note,
) => ({
  type: types.UPDATE_USER_EMAIL,
  userId,

  // name,
  email,
  // number,
  // note,
})

export const updateUserEmailFail = (data = {}, message = '', code = null) => ({
  type: types.UPDATE_USER_EMAIL_FAIL,
  error: {
    data,
    message,
    code,
  },
})

export const updateUserEmailSuccess = user => ({
  type: types.UPDATE_USER_EMAIL_SUCCESS,
  user,
})

export const updateUserEmailReset = () => ({
  type: types.UPDATE_USER_EMAIL_RESET,
})

export const setUserEmail = user => ({
  type: types.SET_USER_EMAIL,
  user,
})

export const changeFormEmail = email => ({
  type: types.CHANGE_FORM_EMAIL,
  email,
})

export const openUpdateUserEmailDialog = () => ({
  type: types.OPEN_UPDATE_USER_EMAIL_DIALOG,
})

export const closeUpdateUserEmailDialog = () => ({
  type: types.CLOSE_UPDATE_USER_EMAIL_DIALOG,
})
