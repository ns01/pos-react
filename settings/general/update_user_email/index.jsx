import reducer from './reducers'

import * as updateUserEmailOperations from './operations'


export { updateUserEmailOperations }

export default reducer
