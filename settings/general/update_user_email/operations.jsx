import {
  updateUserEmail,
  updateUserEmailFail,
  updateUserEmailSuccess,
  updateUserEmailReset,

  setUserEmail,

  // changeFormName,
  changeFormEmail,
  // changeFormNumber,
  // changeFormNote,

  openUpdateUserEmailDialog,
  closeUpdateUserEmailDialog,
} from './actions'


export {
  updateUserEmail,
  updateUserEmailFail,
  updateUserEmailSuccess,
  updateUserEmailReset,

  setUserEmail,

  // changeFormName,
  changeFormEmail,
  // changeFormNumber,
  // changeFormNote,

  openUpdateUserEmailDialog,
  closeUpdateUserEmailDialog,
}
