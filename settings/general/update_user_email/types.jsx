export const UPDATE_USER_EMAIL = 'settings/general/update_user_email/UPDATE_USER_EMAIL'
export const UPDATE_USER_EMAIL_FAIL = 'settings/general/update_user_email/UPDATE_USER_EMAIL_FAIL'
export const UPDATE_USER_EMAIL_SUCCESS = 'settings/general/update_user_email/UPDATE_USER_EMAIL_SUCCESS'
export const UPDATE_USER_EMAIL_RESET = 'settings/general/update_user_email/UPDATE_USER_EMAIL_RESET'

export const SET_USER_EMAIL = 'settings/general/update_user_email/SET_USER_EMAIL'

// Inputs
// export const CHANGE_FORM_NAME = 'settings/general/update_user_email/CHANGE_FORM_NAME'
export const CHANGE_FORM_EMAIL = 'settings/general/update_user_email/CHANGE_FORM_EMAIL'
// export const CHANGE_FORM_NUMBER = 'settings/general/update_user_email/CHANGE_FORM_NUMBER'
// export const CHANGE_FORM_NOTE = 'settings/general/update_user_email/CHANGE_FORM_NOTE'

export const OPEN_UPDATE_USER_EMAIL_DIALOG = 'settings/general/update_user_email/OPEN_UPDATE_USER_EMAIL_DIALOG'
export const CLOSE_UPDATE_USER_EMAIL_DIALOG = 'settings/general/update_user_email/CLOSE_UPDATE_USER_EMAIL_DIALOG'
