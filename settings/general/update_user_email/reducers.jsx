// Core
import reduceReducers from 'reduce-reducers'

// Action types
import * as types from './types'


const updateUserEmailReducer = (state, action) => {
  switch (action.type) {
    case types.UPDATE_USER_EMAIL:
      return {
        ...state,

        updateUserEmailForm: {
          ...state.updateUserEmailForm,

          error: {
            message: '',
            data: {},
            code: null,
          },

          updatingUserEmail: true,
          updateUserEmailFailed: false,
          updateUserEmailSucceeded: false,
        },
      }
    case types.UPDATE_USER_EMAIL_FAIL: {
      return {
        ...state,

        updateUserEmailForm: {
          ...state.updateUserEmailForm,

          error: {
            message: action.error.message,
            data: action.error.data,
            code: action.error.code,
          },

          updatingUserEmail: false,
          updateUserEmailFailed: true,
          updateUserEmailSucceeded: false,
        },
      }
    }
    case types.UPDATE_USER_EMAIL_SUCCESS: {
      return {
        ...state,

        userData: {
          ...state.userData,

          user: action.user,

          // user: {
          //   ...state.userData.user,
          //
          //   data: {
          //     ...state.userData.user.data,
          //
          //     [action.user.id]: action.user,
          //   },
          // },
        },

        updateUserEmailForm: {
          ...state.updateUserEmailForm,

          error: {
            message: '',
            data: {},
            code: null,
          },

          updatingUserEmail: false,
          updateUserEmailFailed: false,
          updateUserEmailSucceeded: true,
        },
      }
    }
    case types.UPDATE_USER_EMAIL_RESET:
      return {
        ...state,

        updateUserEmailForm: {
          isUpdateUserEmailDialogOpened: false,

          user: {},
          // name: '',
          email: '',

          error: {
            message: '',
            data: {},
            code: null,
          },

          updatingUserEmail: false,
          updateUserEmailFailed: false,
          updateUserEmailSucceeded: false,
        },
      }
    case types.SET_USER_EMAIL:
      return {
        ...state,

        updateUserEmailForm: {
          ...state.updateUserEmailForm,

          user: action.user,
        },
      }
    case types.CHANGE_FORM_EMAIL:
      return {
        ...state,

        updateUserEmailForm: {
          ...state.updateUserEmailForm,

          email: action.email,
        },
      }
    case types.OPEN_UPDATE_USER_EMAIL_DIALOG:
      return {
        ...state,

        updateUserEmailForm: {
          ...state.updateUserEmailForm,

          isUpdateUserEmailDialogOpened: true,
        },
      }
    case types.CLOSE_UPDATE_USER_EMAIL_DIALOG:
      return {
        ...state,

        updateUserEmailForm: {
          ...state.updateUserEmailForm,

          isUpdateUserEmailDialogOpened: false,
        },
      }
    default:
      return state
  }
}


export default reduceReducers(updateUserEmailReducer)
