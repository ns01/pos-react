export const UPDATE_BUSINESS_NAME = 'settings/general/update_business_name/UPDATE_BUSINESS_NAME'
export const UPDATE_BUSINESS_NAME_FAIL = 'settings/general/update_business_name/UPDATE_BUSINESS_NAME_FAIL'
export const UPDATE_BUSINESS_NAME_SUCCESS = 'settings/general/update_business_name/UPDATE_BUSINESS_NAME_SUCCESS'
export const UPDATE_BUSINESS_NAME_RESET = 'settings/general/update_business_name/UPDATE_BUSINESS_NAME_RESET'

export const SET_BUSINESS_NAME = 'settings/general/update_business_name/SET_BUSINESS_NAME'

// Inputs
export const CHANGE_FORM_BUSINESS_NAME = 'settings/general/update_business_name/CHANGE_FORM_BUSINESS_NAME'

export const OPEN_UPDATE_BUSINESS_NAME_DIALOG = 'settings/general/update_business_name/OPEN_UPDATE_BUSINESS_NAME_DIALOG'
export const CLOSE_UPDATE_BUSINESS_NAME_DIALOG = 'settings/general/update_business_name/CLOSE_UPDATE_BUSINESS_NAME_DIALOG'
