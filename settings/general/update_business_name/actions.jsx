import * as types from './types'


export const updateBusinessName = (
  userId,

  businessName,
) => ({
  type: types.UPDATE_BUSINESS_NAME,
  userId,

  businessName,
})

export const updateBusinessNameFail = (data = {}, message = '', code = null) => ({
  type: types.UPDATE_BUSINESS_NAME_FAIL,
  error: {
    data,
    message,
    code,
  },
})

export const updateBusinessNameSuccess = user => ({
  type: types.UPDATE_BUSINESS_NAME_SUCCESS,
  user,
})

export const updateBusinessNameReset = () => ({
  type: types.UPDATE_BUSINESS_NAME_RESET,
})

export const setBusinessName = user => ({
  type: types.SET_BUSINESS_NAME,
  user,
})

export const changeFormBusinessName = businessName => ({
  type: types.CHANGE_FORM_BUSINESS_NAME,
  businessName,
})

export const openUpdateBusinessNameDialog = () => ({
  type: types.OPEN_UPDATE_BUSINESS_NAME_DIALOG,
})

export const closeUpdateBusinessNameDialog = () => ({
  type: types.CLOSE_UPDATE_BUSINESS_NAME_DIALOG,
})
