// Core
import api from 'posModules/app/axios'
import { Observable } from 'rxjs'
import { mergeMap, debounceTime } from 'rxjs/operators'
import { ofType } from 'redux-observable'
import camelcaseKeys from 'camelcase-keys'

// Operations
import { updateBusinessNameSuccess, updateBusinessNameFail } from '../operations'

// Types
import { UPDATE_BUSINESS_NAME } from '../types';


const updateBusinessNameEpic = action$ => action$.pipe(
  ofType(UPDATE_BUSINESS_NAME),
  debounceTime(1000),
  mergeMap(action => (
    Observable.create((observer) => {
      const formErrors = {}

      if (Object.getOwnPropertyNames(formErrors).length > 0) {
        observer.next(updateBusinessNameFail(formErrors))
        observer.complete()
        return
      }

      api.put(`/v1/settings/general/company/${action.userId.company}`, {
        name: action.businessName,
      }).then((response) => {
        const user = camelcaseKeys(response.data.data, { deep: true })

        observer.next(updateBusinessNameSuccess(user))
        observer.complete()
      }).catch((error) => {
        const errorResponse = camelcaseKeys(error.response.data.errors, { deep: true })

        const errors = {
          data: errorResponse.data || {},
          message: errorResponse.message,
          code: errorResponse.code,
        }

        observer.next(updateBusinessNameFail(errors.data, errors.message, errors.code))
        observer.complete()
      })
    })
  )),
)

export default updateBusinessNameEpic
