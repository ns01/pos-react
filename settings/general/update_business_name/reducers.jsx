// Core
import reduceReducers from 'reduce-reducers'

// Action types
import * as types from './types'


const updateBusinessNameReducer = (state, action) => {
  switch (action.type) {
    case types.UPDATE_BUSINESS_NAME:
      return {
        ...state,

        updateBusinessNameForm: {
          ...state.updateBusinessNameForm,

          error: {
            message: '',
            data: {},
            code: null,
          },

          updatingBusinessName: true,
          updateBusinessNameFailed: false,
          updateBusinessNameSucceeded: false,
        },
      }
    case types.UPDATE_BUSINESS_NAME_FAIL: {
      return {
        ...state,

        updateBusinessNameForm: {
          ...state.updateBusinessNameForm,

          error: {
            message: action.error.message,
            data: action.error.data,
            code: action.error.code,
          },

          updatingBusinessName: false,
          updateBusinessNameFailed: true,
          updateBusinessNameSucceeded: false,
        },
      }
    }
    case types.UPDATE_BUSINESS_NAME_SUCCESS: {
      return {
        ...state,

        userData: {
          ...state.userData,

          user: action.user,
        },

        updateBusinessNameForm: {
          ...state.updateBusinessNameForm,

          error: {
            message: '',
            data: {},
            code: null,
          },

          updatingBusinessName: false,
          updateBusinessNameFailed: false,
          updateBusinessNameSucceeded: true,
        },
      }
    }
    case types.UPDATE_BUSINESS_NAME_RESET:
      return {
        ...state,

        updateBusinessNameForm: {
          isUpdateBusinessNameDialogOpened: false,

          user: {},
          businessName: '',

          error: {
            message: '',
            data: {},
            code: null,
          },

          updatingBusinessName: false,
          updateBusinessNameFailed: false,
          updateBusinessNameSucceeded: false,
        },
      }
    case types.SET_BUSINESS_NAME:
      return {
        ...state,

        updateBusinessNameForm: {
          ...state.updateBusinessNameForm,

          user: action.user,
        },
      }
    case types.CHANGE_FORM_BUSINESS_NAME:
      return {
        ...state,

        updateBusinessNameForm: {
          ...state.updateBusinessNameForm,

          businessName: action.businessName,
        },
      }
    case types.OPEN_UPDATE_BUSINESS_NAME_DIALOG:
      return {
        ...state,

        updateBusinessNameForm: {
          ...state.updateBusinessNameForm,

          isUpdateBusinessNameDialogOpened: true,
        },
      }
    case types.CLOSE_UPDATE_BUSINESS_NAME_DIALOG:
      return {
        ...state,

        updateBusinessNameForm: {
          ...state.updateBusinessNameForm,

          isUpdateBusinessNameDialogOpened: false,
        },
      }
    default:
      return state
  }
}


export default reduceReducers(updateBusinessNameReducer)
