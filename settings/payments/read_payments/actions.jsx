import * as types from './types'

export const readPayments = () => ({
  type: types.READ_PAYMENTS,
})

export const readPaymentsFail = (data = {}, message = '', code = null) => ({
  type: types.READ_PAYMENTS_FAIL,
  errors: {
    data,
    message,
    code,
  },
})

export const readPaymentsSuccess = paymentsData => ({
  type: types.READ_PAYMENTS_SUCCESS,
  paymentsData,
})
