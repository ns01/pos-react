export const READ_PAYMENTS = 'settings/payments/read_payments/READ_PAYMENTS'
export const READ_PAYMENTS_FAIL = 'settings/payments/read_payments/READ_PAYMENTS_FAIL'
export const READ_PAYMENTS_SUCCESS = 'settings/payments/read_payments/READ_PAYMENTS_SUCCESS'
