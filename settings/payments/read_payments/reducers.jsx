// Core
import reduceReducers from 'reduce-reducers'

// Action Types
import * as types from './types'

const readPaymentsReducer = (state, action) => {
  switch (action.type) {
    case types.READ_PAYMENTS:
      return {
        ...state,

        paymentsData: {
          ...state.paymentsData,

          error: {
            message: '',
            data: {},
            code: null,
          },

          readingPayments: true,
          readPaymentsFailed: false,
          readPaymentsSucceeded: false,
        },
      }

    case types.READ_PAYMENTS_FAIL:
      return {
        ...state,

        paymentsData: {
          ...state.paymentsData,

          error: {
            message: action.errors.message,
            data: action.errors.data,
            code: action.errors.code,
          },

          readingPayments: false,
          readPaymentsFailed: true,
          readPaymentsSucceeded: false,
        },
      }

    case types.READ_PAYMENTS_SUCCESS:
      return {
        ...state,

        paymentsData: {
          ...state.paymentsData,

          error: {
            message: '',
            data: {},
            code: null,
          },

          readingPayments: false,
          readPaymentsFailed: false,
          readPaymentsSucceeded: true,
        },
      }
    default:
      return state
  }
}

export default reduceReducers(readPaymentsReducer)
