// Core
import api from 'posModules/app/axios'
import { Observable } from 'rxjs'
import { mergeMap } from 'rxjs/operators'
import { ofType } from 'redux-observable'
import normalize from 'posModules/app/helper'
import camelcaseKeys from 'camelcase-keys'

// Types
import { READ_PAYMENTS } from '../types'

// Operations
import { readPaymentsFail, readPaymentsSuccess } from '../operations'

const readPaymentsEpic = action$ => action$.pipe(
  ofType(READ_PAYMENTS),
  mergeMap(() => (
    Observable.create((observer) => {
      api.get('/v1/items/').then((response) => {
        const paymentsData = camelcaseKeys(response.data.data, { deep: true })


        observer.next(readPaymentsSuccess(paymentsData))
        observer.complete()
      }).catch((error) => {
        const errorResponse = camelcaseKeys(error.response.data.errors, { deep: true })

        const errors = {
          data: errorResponse.data || {},
          message: errorResponse.message,
          code: errorResponse.code,
        }

        observer.next(readPaymentsFail(errors.data, errors.message, errors.code))
        observer.complete()
      })
    })
  )),
)


export default readPaymentsEpic
