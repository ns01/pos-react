// Core
import reduceReducers from 'reduce-reducers'

// Action types
import * as types from './types'

const createPaymentReducer = (state, action) => {
  switch (action.type) {
    case types.CREATE_PAYMENT:
      return {
        ...state,

        createPaymentForm: {
          ...state.createPaymentForm,

          error: {
            message: '',
            data: {},
            code: null,
          },

          creatingPayment: true,
          createPaymentFailed: false,
          createPaymentSucceeded: false,
        },
      }
    case types.CREATE_PAYMENT_FAIL:
      return {
        ...state,

        createPaymentForm: {
          ...state.createPaymentForm,

          error: {
            message: action.error.message,
            data: action.error.data,
            code: action.error.code,
          },

          creatingPayment: false,
          createPaymentFailed: true,
          createPaymentSucceeded: false,
        },
      }
    case types.CREATE_PAYMENT_SUCCESS: {
      const { result } = state.paymentsData.payments

      result.unshift(action.payment.id)
      return {
        ...state,

        paymentsData: {
          ...state.paymentsData,

          payments: {
            data: {
              ...state.paymentsData.payments.data,

              [action.payment.id]: action.payment,
            },

            result,
          },
        },
        createPaymentForm: {
          ...state.createPaymentForm,

          error: {
            message: '',
            data: {},
            code: null,
          },

          creatingPayment: false,
          createPaymentFailed: false,
          createPaymentSucceeded: true,
        },
      }
    }
    case types.CREATE_PAYMENT_RESET: {
      return {
        ...state,

        createPaymentForm: {
          isCreatePaymentDialogOpened: false,

          paymentType: '',
          name: '',

          error: {
            message: '',
            data: {},
            code: null,
          },

          creatingPayment: false,
          createPaymentFailed: false,
          createPaymentSucceeded: false,
        },
      }
    }
    case types.CHANGE_FORM_TYPE: {
      return {
        ...state,

        createPaymentForm: {
          ...state.createPaymentForm,

          paymentType: action.paymentType,
        },
      }
    }
    case types.CHANGE_FORM_NAME: {
      return {
        ...state,

        createPaymentForm: {
          ...state.createPaymentForm,

          name: action.name,
        },
      }
    }
    case types.OPEN_CREATE_PAYMENT_DIALOG:
      return {
        ...state,

        createPaymentForm: {
          ...state.createPaymentsForm,

          isCreatePaymentDialogOpened: true,
        },
      }
    case types.CLOSE_CREATE_PAYMENT_DIALOG:
      return {
        ...state,

        createPaymentForm: {
          ...state.createPaymentForm,

          isCreatePaymentDialogOpened: false,
        },
      }
    default:
      return state
  }
}

export default reduceReducers(createPaymentReducer)
