export const CREATE_PAYMENT = 'settings/payments/create_payment/CREATE_PAYMENT'
export const CREATE_PAYMENT_FAIL = 'settings/payments/create_payment/CREATE_PAYMENT_FAIL'
export const CREATE_PAYMENT_SUCCESS = 'settings/payments/create_payment/CREATE_PAYMENT_SUCCESS'
export const CREATE_PAYMENT_RESET = 'settings/payments/create_payment/CREATE_PAYMENT_RESET'

// Inputs
export const CHANGE_FORM_NAME = 'settings/payments/create_payment/CHANGE_FORM_NAME'
export const CHANGE_FORM_TYPE = 'settings/payments/create_payment/CHANGE_FORM_TYPE'

export const OPEN_CREATE_PAYMENT_DIALOG = 'settings/payments/create_payment/OPEN_CREATE_PAYMENT_DIALOG'
export const CLOSE_CREATE_PAYMENT_DIALOG = 'settings/payments/create_payment/CLOSE_CREATE_PAYMENT_DIALOG'
