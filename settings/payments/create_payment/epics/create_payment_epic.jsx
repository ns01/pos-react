// Core
import api from 'posModules/app/axios'
import { Observable } from 'rxjs'
import { mergeMap, debounceTime } from 'rxjs/operators'
import { ofType } from 'redux-observable'
import camelcaseKeys from 'camelcase-keys'

// Operations
import { createPaymentSuccess, createPaymentFail } from '../operations'

// Types
import { CREATE_PAYMENT } from '../types'

const createPaymentEpic = action$ => action$.pipe(
  ofType(CREATE_PAYMENT),
  debounceTime(1000),
  mergeMap(action => (
    Observable.create((observer) => {
      const formErrors = {}

      if (action.name.trim() === '') {
        formErrors.name = 'name is required!'
      }

      if (Object.getOwnPropertyNames(formErrors).length > 0) {
        observer.next(createPaymentFail(formErrors))
        observer.complete()
        return
      }

      api.post('/v1/settings/payments/', {
        paymentType: action.paymentType,
        name: action.name,
      }).then((response) => {
        const payment = camelcaseKeys(response.data.data, { deep: true })

        observer.next(createPaymentSuccess(payment))
        observer.complete()
      }).catch((error) => {
        const errorResponse = camelcaseKeys(error.response.data.errors, { deep: true })

        const errors = {
          data: errorResponse.data || {},
          message: errorResponse.message,
          code: errorResponse.code,
        }

        observer.next(createPaymentFail(errors.data, errors.message, errors.code))
        observer.complete()
      })
    })
  )),
)

export default createPaymentEpic
