import * as types from './types'

export const createPayment = (
  paymentType,
  name,
) => ({
  type: types.CREATE_PAYMENT,
  paymentType,
  name,
})

export const createPaymentFail = (data = {}, message = '', code = null) => ({
  type: types.CREATE_PAYMENT_FAIL,
  error: {
    data,
    message,
    code,
  },
})

export const createPaymentSuccess = payment => ({
  type: types.CREATE_PAYMENT_SUCCESS,
  payment,
})

export const createPaymentReset = () => ({
  type: types.CREATE_PAYMENT_RESET,
})

export const changeFormType = paymentType => ({
  type: types.CHANGE_FORM_TYPE,
  paymentType,
})

export const changeFormName = name => ({
  type: types.CHANGE_FORM_NAME,
  name,
})

export const openAddPaymentDialog = () => ({
  type: types.OPEN_CREATE_PAYMENT_DIALOG,
})

export const closeAddPaymentDialog = () => ({
  type: types.CLOSE_CREATE_PAYMENT_DIALOG,
})
