import reducer from './reducers'

import * as updatePaymentOperations from './operations'

export { updatePaymentOperations }

export default reducer
