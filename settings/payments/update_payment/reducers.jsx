// Core
import reduceReducers from 'reduce-reducers'

// Action Types
import * as types from './types'

const updatePaymentReducer = (state, action) => {
  switch (action.type) {
    case types.UPDATE_PAYMENT:
      return {
        ...state,

        updatePaymentForm: {
          ...state.updatePaymentForm,

          error: {
            message: '',
            data: {},
            code: null,
          },

          updatingPayment: true,
          updatePaymentFailed: false,
          updatePaymentSucceeded: false,
        },
      }
    case types.UPDATE_PAYMENT_FAIL: {
      return {
        ...state,

        updatePaymentForm: {
          ...state.updatePaymentForm,

          error: {
            message: action.error.message,
            data: action.error.data,
            code: action.error.code,
          },

          updatingPayment: false,
          updatePaymentFailed: true,
          updatePaymentSucceeded: false,
        },
      }
    }
    case types.UPDATE_PAYMENT_SUCCESS: {
      return {
        ...state,

        paymentsData: {
          ...state.paymentsData,

          payments: {
            ...state.paymentsData.payments,

            data: {
              ...state.paymentsData.payments.data,

              [action.payment.id]: action.payment,
            },
          },
        },

        updatePaymentForm: {
          ...state.updatePaymentForm,

          error: {
            message: '',
            data: {},
            code: null,
          },

          updatingPayment: false,
          updatingPaymentFailed: false,
          updatePaymentSucceeded: true,
        },
      }
    }
    case types.UPDATE_PAYMENT_RESET:
      return {
        ...state,

        updatingPaymentForm: {
          isUpdatePaymentDialogOpened: false,

          payment: {},
          paymentType: '',
          name: '',

          error: {
            message: '',
            data: {},
            code: null,
          },

          updatingPayment: false,
          updatePaymentFailed: false,
          updatePaymentSucceeded: false,
        },
      }
    case types.SET_PAYMENT:
      return {
        ...state,

        updatePaymentForm: {
          ...state.updatePaymentForm,

          payment: action.payment,
        },
      }
    case types.CHANGE_FORM_NAME:
      return {
        ...state,

        updatePaymentForm: {
          ...state.updatePaymentForm,

          name: action.name,
        },
      }
    case types.CHANGE_FORM_TYPE:
      return {
        ...state,

        updatePaymentForm: {
          ...state.updatePaymentForm,

          paymentType: action.paymentType,
        },
      }
    case types.OPEN_UPDATE_PAYMENT_DIALOG:
      return {
        ...state,

        updatePaymentForm: {
          ...state.updatePaymentForm,

          isUpdatePaymentDialogOpened: true,
        },
      }
    case types.CLOSE_UPDATE_PAYMENT_DIALOG:
      return {
        ...state,

        updatePaymentForm: {
          ...state.updatePaymentForm,

          isUpdatePaymentDialogOpened: false,
        },
      }

    default:
      return state
  }
}

export default reduceReducers(updatePaymentReducer)
