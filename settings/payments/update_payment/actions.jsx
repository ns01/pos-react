import * as types from './types'

export const updatePayment = (
  paymentId,
  name,
) => ({
  type: types.UPDATE_PAYMENT,
  paymentId,
  name,
})

export const updatePaymentFail = (data = {}, message = '', code = null) => ({
  type: types.UPDATE_PAYMENT_FAIL,
  error: {
    data,
    message,
    code,
  },
})

export const updatePaymentSuccess = payment => ({
  type: types.UPDATE_PAYMENT_SUCCESS,
  payment,
})

export const updatePaymentReset = () => ({
  type: types.UPDATE_PAYMENT_RESET,
})

export const setPayment = payment => ({
  type: types.SET_PAYMENT,
  payment,
})

export const changeFormName = name => ({
  type: types.CHANGE_FORM_NAME,
  name,
})

export const changeFormType = paymentType => ({
  type: types.CHANGE_FORM_TYPE,
  paymentType,
})

export const openUpdatePaymentDialog = () => ({
  type: types.OPEN_UPDATE_PAYMENT_DIALOG,
})

export const closeUpdatePaymentDialog = () => ({
  type: types.CLOSE_UPDATE_PAYMENT_DIALOG,
})
