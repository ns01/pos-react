export const UPDATE_PAYMENT = 'settings/payments/update_payment/UPDATE_PAYMENT'
export const UPDATE_PAYMENT_FAIL = 'settings/payments/update_payment/UPDATE_PAYMENT_FAIL'
export const UPDATE_PAYMENT_SUCCESS = 'settings/payments/update_payment/UPDATE_PAYMENT_SUCCESS'
export const UPDATE_PAYMENT_RESET = 'settings/payments/update_payment/UPDATE_PAYMENT_RESET'

export const SET_PAYMENT = 'settings/payments/update_payment/SET_PAYMENT'

// INPUTS
export const CHANGE_FORM_NAME = 'settings/payments/update_payment/CHANGE_FORM_NAME'
export const CHANGE_FORM_TYPE = 'settings/payments/update_payment/CHANGE_FORM_TYPE'

export const OPEN_UPDATE_PAYMENT_DIALOG = 'settings/payments/update_payment/OPEN_UPDATE_PAYMENT_DIALOG'
export const CLOSE_UPDATE_PAYMENT_DIALOG = 'settings/payments/update_payment/CLOSE_UPDATE_PAYMENT_DIALOG'
