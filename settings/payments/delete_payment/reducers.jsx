// Core
import reduceReducers from 'reduce-reducers'

// Action Types
import * as types from './types'


const deletePaymentReducer = (state, action) => {
  switch (action.type) {
    case types.DELETE_PAYMENT:
      return {
        ...state,

        deletePaymentForm: {
          ...state.deletePaymentForm,

          error: {
            message: '',
            data: {},
            code: null,
          },

          deletetingPayment: true,
          deletePaymentFailed: false,
          deletePaymentSucess: false,
        },
      }
    case types.DELETE_PAYMENT_FAIL:
      return {
        ...state,

        deletePaymentForm: {
          ...state.deletePaymentForm,

          error: {
            message: action.error.message,
            data: action.error.message,
            code: action.error.code,
          },

          deletetingPayment: false,
          deletePaymentFailed: true,
          deletePaymentSucceeded: false,
        },
      }
    case types.DELETE_PAYMENT_SUCCESS: {
      const { data, result: paymentResult } = state.paymentsData.payments
      delete data[action.paymentId]

      const result = paymentResult.filer(paymentId => paymentId !== action.paymentId)

      return {
        ...state,

        paymentData: {
          ...state.paymentData,

          payments: {
            data,
            result,
          },
        },

        deletePaymentForm: {
          ...state.deletePaymentForm,

          error: {
            message: '',
            data: {},
            code: null,
          },

          deletingPayment: false,
          deletePaymentFailed: false,
          deletePaymentSucceeded: true,
        },
      }
    }

    case types.DELETE_PAYMENT_RESET:
      return {
        ...state,

        deletePaymentForm: {
          isDeletePaymentDialogOpened: false,
          payment: {},

          error: {
            message: '',
            data: {},
            code: null,
          },

          deletingPayment: false,
          deletePaymentFailed: false,
          deletePaymentSucceeded: false,
        },
      }

    case types.SET_PAYMENT:
      return {
        ...state,

        deletePaymentForm: {
          ...state.deletePaymentForm,

          payment: action.payment,
        },
      }
    case types.CLOSE_DELETE_PAYMENT_DIALOG:
      return {
        ...state,

        deletePaymentForm: {
          ...state.deletePaymentForm,

          isDeletePaymentDialogOpened: false,
        },
      }

    default:
      return state
  }
}

export default reduceReducers(deletePaymentReducer)
