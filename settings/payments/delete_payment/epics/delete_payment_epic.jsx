// Core
import api from 'posModules/app/axios'
import { Observable } from 'rxjs'
import { mergeMap, debounceTime } from 'rxjs/operators'
import { ofType } from 'redux-observable'
import camelcaseKeys from 'camelcase-keys'

// Operations
import { deletePaymentSuccess, deletePaymentFail } from '../operations'

// Types
import { DELETE_PAYMENT } from '../types'

const deletePaymentEpic = action$ => action$.pipe(
  ofType(DELETE_PAYMENT),
  debounceTime(1000),
  mergeMap(action => (
    Observable.create((observer) => {
      api.delete(`/v1/settings/payments/${action.paymentId}`).then(() => {
        observer.next(deletePaymentSuccess(action.paymentId))
        observer.complete()
      }).catch((error) => {
        const errorResponse = camelcaseKeys(error.response.data.errors, { deep: true })

        const errors = {
          data: errorResponse.data || {},
          message: errorResponse.message,
          code: errorResponse.code,
        }

        observer.next(deletePaymentFail(errors.data, errors.message, errors.code))
        observer.complete()
      })
    })
  )),
)

export default deletePaymentEpic
