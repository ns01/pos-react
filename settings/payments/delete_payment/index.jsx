import reducer from './reducers'

import * as deletePaymentOperations from './operations'

export { deletePaymentOperations }

export default reducer
