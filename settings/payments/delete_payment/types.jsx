export const DELETE_PAYMENT = 'settings/payments/delete_payment/DELETE_PAYMENT'
export const DELETE_PAYMENT_FAIL = 'settings/payments/delete_payment/DELETE_PAYMENT_FAIL'
export const DELETE_PAYMENT_SUCCESS = 'settings/payments/delete_payment/DELETE_PAYMENT_SUCCESS'
export const DELETE_PAYMENT_RESET = 'settings/payments/delete_payment/DELETE_PAYMENT_RESET'

export const SET_PAYMENT = 'settings/payments/delete_payment/SET_PAYMENT'

export const OPEN_DELETE_PAYMENT_DIALOG = 'settings/payments/delete_payment/OPEN_DELETE_PAYMENT_DIALOG'
export const CLOSE_DELETE_PAYMENT_DIALOG = 'settings/payments/delete_payment/CLOSE_DELETE_PAYMENT_DIALOG'
