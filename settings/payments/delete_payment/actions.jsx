import * as types from './types'

export const deletePayment = paymentId => ({
  type: types.DELETE_PAYMENT,
  paymentId,
})

export const deletePaymentFail = (data = {}, message = '', code = null) => ({
  type: types.DELETE_PAYMENT_FAIL,
  error: {
    data,
    message,
    code,
  },
})

export const deletePaymentSuccess = paymentId => ({
  type: types.DELETE_PAYMENT_SUCCESS,
  paymentId,
})

export const deletePaymentReset = () => ({
  type: types.DELETE_PAYMENT_RESET,
})

export const setPayment = payment => ({
  type: types.SET_PAYMENT,
  payment,
})

export const openDeletePaymentDialog = () => ({
  type: types.OPEN_DELETE_PAYMENT_DIALOG,
})

export const closeDeletePaymentDialog = () => ({
  type: types.CLOSE_DELETE_PAYMENT_DIALOG,
})
