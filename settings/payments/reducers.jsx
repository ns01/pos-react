// Core
import reduceReducers from 'reduce-reducers'

// Reducers
import createPaymentReducer from './create_payment'
import readPaymentsReducer from './read_payments'
import updatePaymentReducer from './update_payment'
import deletePaymentReducer from './delete_payment'

// State Shape
const initialState = {
  paymentsData: {
    // Data
    payments: {
      result: [],
      data: {},
    },

    // Error
    error: {
      message: '',
      data: {},
      code: null,
    },

    // Status
    readingPayments: false,
    readPaymentsFailed: false,
    readPaymentsSucceeded: false,
  },

  createPaymentForm: {
    isCreatePaymentDialogOpened: false,

    // Input
    paymentType: '',
    name: '',

    // Error
    error: {
      message: '',
      data: {},
      code: null,
    },

    // Status
    creatingPayment: false,
    createPaymentFailed: false,
    createPaymentSucceeded: false,
  },

  updatePaymentForm: {
    isUpdatePaymentDialogOpened: false,

    // Input
    payment: {},

    // Error
    error: {
      message: '',
      data: {},
      code: null,
    },

    // Status
    updatingPayment: false,
    updatePaymentFailed: false,
    updatePaymentSucceeded: false,
  },

  deletePaymentForm: {
    isDeletePaymentDialogOpened: false,

    // Input
    payment: {},

    // Error
    error: {
      message: '',
      data: {},
      code: null,
    },

    // Status
    deletingPayment: false,
    deletePaymentFailed: false,
    deletePaymentSucceeded: false,
  },
}

// Initial reducer
const initialReducer = (state = initialState) => (
  state
)

export default reduceReducers(
  initialReducer,
  createPaymentReducer,
  readPaymentsReducer,
  updatePaymentReducer,
  deletePaymentReducer,
)
