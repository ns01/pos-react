import reducers from './reducers'

import * as readTaxesOperations from './operations'

export { readTaxesOperations }

export default reducers
