// Core
import api from 'posModules/app/axios'
import camelcaseKeys from 'camelcase-keys'
import { Observable } from 'rxjs'
import { mergeMap } from 'rxjs/operators'
import { ofType } from 'redux-observable'
import normalize from 'posModules/app/helper'

// Types
import { READ_TAXES } from '../types'

// Operations
import { readTaxesSuccess, readTaxesFail } from '../operations'

const readTaxesEpic = action$ => action$.pipe(
  ofType(READ_TAXES),
  mergeMap(() => (
    Observable.create((observer) => {
      api.get('/v1/settings/taxes').then((response) => {
        const taxes = camelcaseKeys(response.data.data, { deep: true })
        const taxesData = normalize(taxes, 'id')

        console.log(taxesData)

        const data = {}

        taxesData.result.forEach((taxId) => {
          const value = `${taxesData.data[taxId].value}`

          data[taxId] = {
            ...taxesData.data[taxId],
            value,
          }
        })

        taxesData.data = data

        observer.next(readTaxesSuccess(taxesData))
        observer.complete()
      }).catch((error) => {
        const errorResponse = camelcaseKeys(error.response.data.errors, { deep: true })

        const errors = {
          data: errorResponse.data || {},
          message: errorResponse.message,
          code: errorResponse.code,
        }

        observer.next(readTaxesFail(errors.data, errors.message, errors.code))
        observer.complete()
      })
    })
  )),
)

export default readTaxesEpic
