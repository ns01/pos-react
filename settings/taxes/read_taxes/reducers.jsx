import reduceReducers from 'reduce-reducers'

import * as types from './types'

const readTaxesReducer = (state, action) => {
  switch (action.type) {
    case types.READ_TAXES: {
      return {
        ...state,

        taxesData: {
          ...state.taxesData,

          error: {
            message: '',
            data: {},
            code: null,
          },

          readingTaxes: true,
          readTaxesFail: false,
          readTaxesSucceeded: false,
        },
      }
    }
    case types.READ_TAXES_FAIL: {
      return {
        ...state,

        taxesData: {
          ...state.taxesData,

          error: {
            message: action.errors.message,
            data: action.errors.data,
            code: action.errors.code,
          },

          readingTaxes: false,
          readTaxesFail: true,
          readTaxesSucceeded: false,
        },
      }
    }
    case types.READ_TAXES_SUCCESS: {
      return {
        ...state,

        taxesData: {
          ...state.taxesData,

          taxes: {
            result: action.taxesData.result,
            data: action.taxesData.data,
          },

          error: {
            message: '',
            data: {},
            code: null,
          },

          readingTaxes: false,
          readTaxesFail: false,
          readTaxesSucceeded: true,
        },
      }
    }
    default:
      return state
  }
}

export default reduceReducers(readTaxesReducer)
