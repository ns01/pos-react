import * as types from './types'

export const readTaxes = () => ({
  type: types.READ_TAXES,
})

export const readTaxesFail = (data = {}, message = '', code = null) => ({
  type: types.READ_TAXES_FAIL,
  error: {
    data,
    message,
    code,
  },
})

export const readTaxesSuccess = taxesData => ({
  type: types.READ_TAXES_SUCCESS,
  taxesData,
})
