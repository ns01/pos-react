export const READ_TAXES = './settings/taxes/read_taxes/READ_TAXES'
export const READ_TAXES_FAIL = './settings/taxes/read_taxes/READ_TAXES_FAIL'
export const READ_TAXES_SUCCESS = './settings/taxes/read_taxes/READ_TAXES_SUCCESS'
