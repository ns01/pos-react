import * as types from './types'

export const deleteTax = taxId => ({
  type: types.DELETE_TAX,
  taxId,
})

export const deleteTaxFail = (data = {}, message = '', code = null) => ({
  type: types.DELETE_TAX_FAIL,
  error: {
    data,
    message,
    code,
  },
})

export const deleteTaxSuccess = taxId => ({
  type: types.DELETE_TAX_SUCCESS,
  taxId,
})

export const deleteTaxReset = () => ({
  type: types.DELETE_TAX_RESET,
})

export const setTax = tax => ({
  type: types.SET_TAX,
  tax,
})

export const openDeleteTaxDialog = () => ({
  type: types.OPEN_DELETE_TAX_DIALOG,
})

export const closeDeleteTaxDialog = () => ({
  type: types.CLOSE_DELETE_TAX_DIALOG,
})
