// Core
import api from 'posModules/app/axios'
import { Observable } from 'rxjs'
import { mergeMap, debounceTime } from 'rxjs/operators'
import { ofType } from 'redux-observable'
import camelcaseKeys from 'camelcase-keys'

// Operations
import { deleteTaxSuccess, deleteTaxFail } from '../operations'

// Types
import { DELETE_TAX } from '../types';


const deleteTaxEpic = action$ => action$.pipe(
  ofType(DELETE_TAX),
  debounceTime(1000),
  mergeMap(action => (
    Observable.create((observer) => {
      api.delete(`/v1/settings/taxes/${action.taxId}`).then(() => {
        observer.next(deleteTaxSuccess(action.taxId))
        observer.complete()
      }).catch((error) => {
        const errorResponse = camelcaseKeys(error.response.data.errors, { deep: true })

        const errors = {
          data: errorResponse.data || {},
          message: errorResponse.message,
          code: errorResponse.code,
        }

        observer.next(deleteTaxFail(errors.data, errors.message, errors.code))
        observer.complete()
      })
    })
  )),
)

export default deleteTaxEpic
