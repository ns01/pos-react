export const DELETE_TAX = 'settings/taxes/delete_tax/DELETE_TAX'
export const DELETE_TAX_FAIL = 'settings/taxes/delete_tax/DELETE_TAX_FAIL'
export const DELETE_TAX_SUCCESS = 'settings/taxes/delete_tax/DELETE_TAX_SUCCESS'
export const DELETE_TAX_RESET = 'settings/taxes/delete_tax/DELETE_TAX_RESET'

export const SET_TAX = 'settings/taxes/delete_tax/SET_TAX'

export const OPEN_DELETE_TAX_DIALOG = 'settings/taxes/delete_tax/OPEN_DELETE_TAX_DIALOG'
export const CLOSE_DELETE_TAX_DIALOG = 'settings/taxes/delete_tax/CLOSE_DELETE_TAX_DIALOG'
