import {
  deleteTax,
  deleteTaxFail,
  deleteTaxSuccess,
  deleteTaxReset,

  setTax,

  openDeleteTaxDialog,
  closeDeleteTaxDialog,
} from './actions'

export {
  deleteTax,
  deleteTaxFail,
  deleteTaxSuccess,
  deleteTaxReset,

  setTax,

  openDeleteTaxDialog,
  closeDeleteTaxDialog,
}
