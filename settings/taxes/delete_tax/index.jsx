import reducers from './reducers'

import * as deleteTaxOperations from './operations'

export { deleteTaxOperations }

export default reducers
