import reduceReducers from 'reduce-reducers'

import * as types from './types'

export const deleteTaxReducer = (state, action) => {
  switch (action.type) {
    case types.DELETE_TAX: {
      return {
        ...state,

        deleteTaxForm: {
          ...state.deleteTaxForm,

          error: {
            message: '',
            data: {},
            code: null,
          },

          deletingTax: true,
          deleteTaxFailed: false,
          deleteTaxSucceeded: false,
        },
      }
    }
    case types.DELETE_TAX_FAIL: {
      return {
        ...state,

        deleteTaxForm: {
          ...state.deleteTaxForm,

          error: {
            message: action.error.message,
            data: action.error.data,
            code: action.error.code,
          },
        },

        deletingTax: false,
        deleteTaxFailed: true,
        deleteTaxSucceeded: false,
      }
    }
    case types.DELETE_TAX_SUCCESS: {
      const { data, result: taxResult } = state.taxesData.taxes
      delete data[action.taxId]

      const result = taxResult.filter(taxId => taxId !== action.taxId)
      return {
        ...state,

        taxesData: {
          ...state.taxesData,

          taxes: {
            data,
            result,
          },
        },

        deleteTaxForm: {
          ...state.deleteTaxForm,

          error: {
            message: '',
            data: {},
            code: null,
          },

          deletingTax: false,
          deleteTaxFailed: false,
          deleteTaxSucceeded: true,
        },
      }
    }
    case types.DELETE_TAX_RESET:
      return {
        ...state,

        deleteTaxForm: {
          isDeleteTaxDialogOpened: false,
          tax: {},

          error: {
            message: '',
            data: {},
            code: null,
          },

          deletingTax: false,
          deleteTaxFailed: false,
          deleteTaxSucceeded: false,
        },
      }
    case types.SET_TAX:
      return {
        ...state,

        deleteTaxForm: {
          ...state.deleteTaxForm,

          tax: action.tax,
        },
      }
    case types.OPEN_DELETE_TAX_DIALOG:
      return {
        ...state,

        deleteTaxForm: {
          ...state.deleteTaxForm,

          isDeleteTaxDialogOpened: true,
        },
      }
    case types.CLOSE_DELETE_TAX_DIALOG:
      return {
        ...state,

        deleteTaxForm: {
          ...state.deleteTaxForm,

          isDeleteTaxDialogOpened: false,
        },
      }
    default:
      return state
  }
}

export default reduceReducers(deleteTaxReducer)
