import {
  updateTax,
  updateTaxFail,
  updateTaxSuccess,
  updateTaxReset,

  changeFormName,
  changeFormRate,
  changeFormType,
  changeFormOption,
  setTax,

  openUpdateTaxDialog,
  closeUpdateTaxDialog,
} from './actions'

export {
  updateTax,
  updateTaxFail,
  updateTaxSuccess,
  updateTaxReset,

  changeFormName,
  changeFormRate,
  changeFormType,
  changeFormOption,
  setTax,

  openUpdateTaxDialog,
  closeUpdateTaxDialog,
}
