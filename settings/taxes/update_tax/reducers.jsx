// Core
import reduceReducers from 'reduce-reducers'

// Action Types
import * as types from './types'

const updateTaxReducer = (state, action) => {
  switch (action.type) {
    case types.UPDATE_TAX: {
      return {
        ...state,

        updateTaxForm: {
          ...state.updateTaxForm,

          error: {
            message: '',
            data: {},
            code: null,
          },

          updatingTax: true,
          updateTaxFailed: false,
          updateTaxSucceeded: false,
        },
      }
    }
    case types.UPDATE_TAX_FAIL: {
      return {
        ...state,

        updateTaxForm: {
          ...state.updateTaxForm,

          error: {
            message: action.error.message,
            data: action.error.data,
            code: action.error.code,
          },

          updatingTax: false,
          updateTaxFailed: true,
          updateTaxSucceeded: false,
        },
      }
    }
    case types.UPDATE_TAX_SUCCESS: {
      return {
        ...state,

        taxData: {
          ...state.taxData,

          tax: {
            ...state.taxData.tax,

            data: {
              ...state.taxData.data,

              [action.tax.id]: action.tax,
            },
          },
        },
        updateTaxForm: {
          ...state.updateTaxForm,

          error: {
            message: '',
            data: {},
            code: null,
          },

          updatingTax: false,
          updateTaxFailed: false,
          updateTaxSucceeded: true,
        },

      }
    }
    case types.UPDATE_TAX_RESET:
      return {
        ...state,

        updateTaxForm: {
          isUpdateTaxDialogOpened: false,

          tax: {},
          name: '',

          error: {
            message: '',
            data: {},
            code: null,
          },

          updatingTax: false,
          updateTaxFailed: false,
          updateTaxSucceeded: false,
        },
      }
    case types.SET_TAX:
      return {
        ...state,

        updateTaxForm: {
          ...state.updateTaxForm,

          tax: action.tax,
        },
      }
    case types.CHANGE_FORM_NAME:
      return {
        ...state,

        updateTaxForm: {
          ...state.updateTaxForm,

          name: action.name,
        },
      }

    case types.CHANGE_FORM_RATE:
      return {
        ...state,

        updateTaxForm: {
          ...state.updateTaxForm,

          rate: action.rate,
        },
      }

    case types.CHANGE_FORM_TYPE:
      return {
        ...state,

        updateTaxForm: {
          ...state.updateTaxForm,

          taxType: action.taxType,
        },
      }

    case types.CHANGE_FORM_OPTION:
      return {
        ...state,

        updateTaxForm: {
          ...state.updateTaxForm,

          option: action.option,
        },
      }

    case types.OPEN_UPDATE_TAX_DIALOG:
      return {
        ...state,

        updateTaxForm: {
          ...state.updateTaxForm,

          isUpdateTaxDialogOpened: true,
        },
      }
    case types.CLOSE_UPDATE_TAX_DIALOG:
      return {
        ...state,

        updateTaxForm: {
          ...state.updateTaxForm,

          isUpdateTaxDialogOpened: false,
        },
      }
    default:
      return state
  }
}

export default reduceReducers(updateTaxReducer)
