export const UPDATE_TAX = 'settings/taxes/update_tax/UPDATE_TAX'
export const UPDATE_TAX_FAIL = 'settings/taxes/update_tax/UPDATE_TAX_FAIL'
export const UPDATE_TAX_SUCCESS = 'settings/taxes/update_tax/UPDATE_TAX_SUCCESS'
export const UPDATE_TAX_RESET = 'settings/taxes/update_tax/UPDATE_TAX_RESET'

export const CHANGE_FORM_NAME = 'settings/taxes/update_tax/CHANGE_FORM_NAME'
export const CHANGE_FORM_RATE = 'settings/taxes/update_tax/CHANGE_FORM_RATE'
export const CHANGE_FORM_TYPE = 'settings/taxes/update_tax/CHANGE_FORM_TYPE'
export const CHANGE_FORM_OPTION = 'settings/taxes/update_tax/CHANGE_FORM_OPTION'

export const SET_TAX = 'settings/taxes/update_tax/SET_TAX'

export const OPEN_UPDATE_TAX_DIALOG = 'settings/taxes/update_tax/OPEN_UPDATE_TAX_DIALOG'
export const CLOSE_UPDATE_TAX_DIALOG = 'settings/taxes/update_tax/CLOSE_UPDATE_TAX_DIALOG'
