import reducers from './reducers'

import * as updateTaxOperations from './operations'

export { updateTaxOperations }

export default reducers
