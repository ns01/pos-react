import * as types from './types'

export const updateTax = (
  name,
  rate,
  taxType,
  option,
) => ({
  type: types.UPDATE_TAX,
  name,
  rate,
  taxType,
  option,
})

export const updateTaxFail = (data = {}, message = '', code = null) => ({
  type: types.UPDATE_TAX_FAIL,
  error: {
    message,
    data,
    code,
  },
})

export const updateTaxSuccess = tax => ({
  type: types.UPDATE_TAX_SUCCESS,
  tax,
})

export const updateTaxReset = () => ({
  type: types.UPDATE_TAX_RESET,
})

export const setTax = tax => ({
  type: types.SET_TAX,
  tax,
})

export const changeFormName = name => ({
  type: types.CHANGE_FORM_NAME,
  name,
})

export const changeFormRate = rate => ({
  type: types.CHANGE_FORM_NAME,
  rate,
})

export const changeFormType = taxType => ({
  type: types.CHANGE_FORM_TYPE,
  taxType,
})

export const changeFormOption = option => ({
  type: types.CHANGE_FORM_OPTION,
  option,
})

export const openUpdateTaxDialog = () => ({
  type: types.OPEN_UPDATE_TAX_DIALOG,

  isUpdateTaxDialogOpened: true,
})

export const closeUpdateTaxDialog = () => ({
  type: types.CLOSE_UPDATE_TAX_DIALOG,

  isUpdateTaxDialogOpened: false,
})
