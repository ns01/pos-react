// Core
import api from 'posModules/app/axios'
import { Observable } from 'rxjs'
import { mergeMap, debounceTime } from 'rxjs/operators'
import { ofType } from 'redux-observable'
import camelcaseKeys from 'camelcase-keys'

// Operations
import { updateTaxSuccess, updateTaxFail } from '../operations'

// Types
import { UPDATE_TAX } from '../types'

const updateTaxEpic = action$ => action$.pipe(
  ofType(UPDATE_TAX),
  debounceTime(1000),
  mergeMap(action => (
    Observable.create((observer) => {
      const formErrors = {}

      if (action.name.trim() === '') {
        formErrors.name = 'name is required'
      }

      const numberInput = parseInt(action.rate, 10)
      if (numberInput > 100) {
        formErrors.rate = 'rate must be less or equal to 100'
      }

      if (Object.getOwnPropertyNames(formErrors).length > 0) {
        observer.next(updateTaxFail(formErrors))
        observer.complete()
        return
      }

      api.put(`/v1/settings/taxes/${action.taxId}`, {
        name: action.name,
        rate: action.rate || 0,
        type: action.taxType,
        option: action.option,
      }).then((response) => {
        const tax = camelcaseKeys(response.data.data, { deep: true })

        observer.next(updateTaxSuccess(tax))
        observer.complete()
      }).catch((error) => {
        const errorResponse = camelcaseKeys(error.response.data.errors, { deep: true })

        const errors = {
          data: errorResponse.data || {},
          message: errorResponse.message,
          code: errorResponse.code,
        }

        observer.next(updateTaxFail(errors.data, errors.message, errors.code))
        observer.complete()
      })
    })
  )),
)

export default updateTaxEpic
