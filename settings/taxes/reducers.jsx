// Core
import reduceReducers from 'reduce-reducers'

// Reducers
import createTaxReducer from './create_tax'
import readTaxesReducer from './read_taxes'
import updateTaxReducer from './update_tax'
import deleteTaxReducer from './delete_tax'

// State Shape
const initialState = {
  taxesData: {
    // Data
    taxes: {
      result: [],
      data: {},
    },

    // Error
    error: {
      message: '',
      data: {},
      code: null,
    },

    // Status
    readingTaxes: false,
    readTaxesFailed: false,
    readTaxesSucceeded: false,
  },

  createTaxForm: {
    isCreateTaxDialogOpened: false,

    // Input
    name: '',
    rate: '',
    taxType: '',
    option: '',

    // Error
    error: {
      message: '',
      data: {},
      code: null,
    },

    // Status
    creatingTax: false,
    createTaxFailed: false,
    createTaxSucceeded: false,
  },
  updateTaxForm: {
    isCreateTaxDialogOpened: false,

    // Input
    name: '',
    rate: '',
    taxType: '',
    option: '',

    // Error
    error: {
      message: '',
      data: {},
      code: null,
    },

    // Status
    updatingTax: false,
    updateTaxFailed: false,
    updateTaxSucceeded: false,
  },
  deleteTaxForm: {
    isCreateTaxDialogOpened: false,

    // Input
    name: '',
    rate: '',
    taxType: '',
    option: '',

    // Error
    error: {
      message: '',
      data: {},
      code: null,
    },

    // Status
    deletingTax: false,
    deleteTaxFailed: false,
    deleteTaxSucceeded: false,
  },
}

// Initial Reducer
const initialReducer = (state = initialState) => (
  state
)

export default reduceReducers(
  initialReducer,
  createTaxReducer,
  readTaxesReducer,
  updateTaxReducer,
  deleteTaxReducer,
)
