import reducer from './reducers'

import * as createTaxOperations from './operations'

export { createTaxOperations }

export default reducer
