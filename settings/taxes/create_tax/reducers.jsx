// Core
import reduceReducers from 'reduce-reducers'

// Action Types
import * as types from './types'

const createTaxReducer = (state, action) => {
  switch (action.type) {
    case types.CREATE_TAX: {
      return {
        ...state,

        createTaxForm: {
          ...state.createTaxForm,

          error: {
            message: '',
            data: {},
            code: null,
          },

          creatingTax: true,
          createTaxFailed: false,
          createTaxSucceeded: false,
        },
      }
    }
    case types.CREATE_TAX_FAIL: {
      return {
        ...state,

        createTaxForm: {
          ...state.createTaxForm,

          error: {
            message: action.error.message,
            data: action.error.data,
            code: action.error.code,
          },

          creatingTax: false,
          createTaxFailed: true,
          createTaxSucceeded: false,
        },
      }
    }
    case types.CREATE_TAX_SUCCESS: {
      const { result } = state.taxesData.taxes

      result.unshift(action.tax.id)

      return {
        ...state,

        taxesData: {
          ...state.taxesData,

          taxes: {
            data: {
              ...state.taxesData.taxes.data,

              [action.tax.id]: action.tax,
            },

            result,
          },
        },

        createTaxForm: {
          ...state.createTaxForm,

          error: {
            message: '',
            data: {},
            code: null,
          },

          creatingTax: false,
          createTaxFailed: false,
          createTaxSucceeded: true,
        },
      }
    }
    case types.CREATE_TAX_RESET: {
      return {
        ...state,

        createTaxForm: {
          isCreateTaxDialogOpened: false,

          name: '',
          taxType: '',
          rate: '',
          option: '',

          error: {
            message: '',
            data: '',
            code: null,
          },

          creatingTax: false,
          createTaxFailed: false,
          createTaxSucceeded: false,
        },
      }
    }
    case types.CHANGE_FORM_NAME: {
      return {
        ...state,

        createTaxForm: {
          ...state.createTaxForm,

          name: action.name,
        },
      }
    }
    case types.CHANGE_FORM_RATE: {
      return {
        ...state,

        createTaxForm: {
          ...state.createTaxForm,

          rate: action.rate,
        },
      }
    }
    case types.CHANGE_FORM_TYPE: {
      return {
        ...state,

        createTaxForm: {
          ...state.createTaxForm,

          taxType: action.taxType,
        },
      }
    }
    case types.CHANGE_FORM_OPTION: {
      return {
        ...state,

        createTaxForm: {
          ...state.createTaxForm,

          option: action.option,
        },
      }
    }
    case types.OPEN_CREATE_TAX_DIALOG: {
      return {
        ...state,

        createTaxForm: {
          ...state.createTaxForm,

          isCreateTaxDialogOpened: true,
        },
      }
    }
    case types.CLOSE_CREATE_TAX_DIALOG: {
      return {
        ...state,

        createTaxForm: {
          ...state.createTaxForm,

          isCreateTaxDialogOpened: false,
        },
      }
    }
    default:
      return state
  }
}

export default reduceReducers(createTaxReducer)
