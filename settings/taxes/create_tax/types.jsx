export const CREATE_TAX = 'settings/taxes/create_tax/CREATE_TAX'
export const CREATE_TAX_FAIL = 'settings/taxes/create_tax/CREATE_TAX_FAIL'
export const CREATE_TAX_SUCCESS = 'settings/taxes/create_tax/CREATE_TAX_SUCCESS'
export const CREATE_TAX_RESET = 'settings/taxes/create_tax/CREATE_TAX_RESET'

// Inputs
export const CHANGE_FORM_NAME = 'settings/taxes/create_tax/CHANGE_FORM_NAME'
export const CHANGE_FORM_RATE = 'settings/taxes/create_tax/CHANGE_FORM_RATE'
export const CHANGE_FORM_TYPE = 'settings/taxes/create_tax/CHANGE_FORM_TYPE'

// Optional
export const CHANGE_FORM_OPTION = 'settings/taxes/create_tax/CHANGE_FORM_OPTION'

export const OPEN_CREATE_TAX_DIALOG = 'settings/taxes/create_tax/OPEN_CREATE_TAX_DIALOG'
export const CLOSE_CREATE_TAX_DIALOG = 'settings/taxes/create_tax/CLOSE_CREATE_TAX_DIALOG'
