import {
  createTax,
  createTaxFail,
  createTaxSuccess,
  createTaxReset,

  changeFormName,
  changeFormRate,
  changeFormType,
  changeFormOption,

  openCreateTaxDialog,
  closeCreateTaxDialog,
} from './actions'

export {
  createTax,
  createTaxFail,
  createTaxSuccess,
  createTaxReset,

  changeFormName,
  changeFormRate,
  changeFormType,
  changeFormOption,

  openCreateTaxDialog,
  closeCreateTaxDialog,
}
