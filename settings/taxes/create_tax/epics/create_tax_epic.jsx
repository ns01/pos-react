// Core
import api from 'posModules/app/axios'
import { Observable } from 'rxjs'
import { mergeMap, debounceTime } from 'rxjs/operators'
import { ofType } from 'redux-observable'
import camelcaseKeys from 'camelcase-keys'

// Operations
import { createTaxSuccess, createTaxFail } from '../operations'

// Types
import { CREATE_TAX } from '../types'

const createTaxEpic = action$ => action$.pipe(
  ofType(CREATE_TAX),
  debounceTime(1000),
  mergeMap(action => (
    Observable.create((observer) => {
      const formErrors = {}

      if (action.name.trim() === '') {
        formErrors.name = 'tax name is required'
      }

      const numberInput = parseInt(action.rate, 10)
      if (numberInput > 100) {
        formErrors.rate = 'rate must be less or equal to 100'
      }


      if (Object.getOwnPropertyNames(formErrors).length > 0) {
        observer.next(createTaxFail(formErrors))
        observer.complete()
        return
      }

      api.post('/v1/settings/taxes/', {
        name: action.name,
        rate: action.rate || 0,
        type: action.taxType,
        option: action.option,
      }).then((response) => {
        const tax = camelcaseKeys(response.data.data, { deep: true })

        observer.next(createTaxSuccess(tax))
        observer.complete()
      }).catch((error) => {
        const errorResponse = camelcaseKeys(error.response.data.errors, { deep: true })

        const errors = {
          data: errorResponse.data || {},
          message: errorResponse.message,
          code: errorResponse.code,
        }

        observer.next(createTaxFail(errors.data, errors.message, errors.code))
        observer.complete()
      })
    })
  )),
)

export default createTaxEpic
