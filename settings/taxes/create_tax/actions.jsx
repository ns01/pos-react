import * as types from './types'

export const createTax = (
  name,
  rate,
  taxType,
  option,

  taxId,
) => ({
  type: types.CREATE_TAX,
  name,
  rate,
  taxType,
  option,

  taxId,
})

export const createTaxFail = (data = {}, message = '', code = null) => ({
  type: types.CREATE_TAX_FAIL,
  error: {
    data,
    message,
    code,
  },
})

export const createTaxSuccess = tax => ({
  type: types.CREATE_TAX_SUCCESS,
  tax,
})

export const createTaxReset = () => ({
  type: types.CREATE_TAX_RESET,
})

export const changeFormName = name => ({
  type: types.CHANGE_FORM_NAME,
  name,
})

export const changeFormRate = rate => ({
  type: types.CHANGE_FORM_RATE,
  rate,
})

export const changeFormType = taxType => ({
  type: types.CHANGE_FORM_TYPE,
  taxType,
})

export const changeFormOption = option => ({
  type: types.CHANGE_FORM_OPTION,
  option,
})

export const openCreateTaxDialog = () => ({
  type: types.OPEN_CREATE_TAX_DIALOG,
})

export const closeCreateTaxDialog = () => ({
  type: types.CLOSE_CREATE_TAX_DIALOG,
})
