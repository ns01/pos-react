export const REGISTER = 'auth/register/REGISTER'
export const REGISTER_FAIL = 'auth/register/REGISTER_FAIL'
export const REGISTER_SUCCESS = 'auth/register/REGISTER_SUCCESS'
export const REGISTER_RESET = 'auth/register/REGISTER_RESET'

// Inputs
export const CHANGE_FORM_EMAIL = 'auth/register/CHANGE_FORM_EMAIL'
export const CHANGE_FORM_EMAIL_FAIL = 'auth/register/CHANGE_FORM_EMAIL_FAIL'
export const CHANGE_FORM_EMAIL_SUCCESS = 'auth/register/CHANGE_FORM_EMAIL_SUCCESS'

export const CHANGE_FORM_PASSWORD = 'auth/register/CHANGE_FORM_PASSWORD'
export const CHANGE_FORM_PASSWORD_FAIL = 'auth/register/CHANGE_FORM_PASSWORD_FAIL'
export const CHANGE_FORM_PASSWORD_SUCCESS = 'auth/register/CHANGE_FORM_PASSWORD_SUCCESS'

export const CHANGE_FORM_CONFIRM_PASSWORD = 'auth/register/CHANGE_FORM_CONFIRM_PASSWORD'
export const CHANGE_FORM_CONFIRM_PASSWORD_FAIL = 'auth/register/CHANGE_FORM_CONFIRM_PASSWORD_FAIL'
export const CHANGE_FORM_CONFIRM_PASSWORD_SUCCESS = 'auth/register/CHANGE_FORM_CONFIRM_PASSWORD_SUCCESS'

export const CHANGE_FORM_BUSINESS_NAME = 'auth/register/CHANGE_FORM_BUSINESS_NAME'
export const CHANGE_FORM_BUSINESS_NAME_FAIL = 'auth/register/CHANGE_FORM_BUSINESS_NAME_FAIL'
export const CHANGE_FORM_BUSINESS_NAME_SUCCESS = 'auth/register/CHANGE_FORM_BUSINESS_NAME_SUCCESS'

export const CHANGE_FORM_COUNTRY_CODE = 'auth/register/CHANGE_FORM_COUNTRY_CODE'
export const CHANGE_FORM_COUNTRY_CODE_FAIL = 'auth/register/CHANGE_FORM_COUNTRY_CODE_FAIL'
export const CHANGE_FORM_COUNTRY_CODE_SUCCESS = 'auth/register/CHANGE_FORM_COUNTRY_CODE_SUCCESS'

export const CHECK_FORM_TERMS_OF_SERVICE = 'auth/register/CHECK_FORM_TERMS_OF_SERVICE'
export const CHECK_FORM_TERMS_OF_SERVICE_FAIL = 'auth/register/CHECK_FORM_TERMS_OF_SERVICE_FAIL'
export const CHECK_FORM_TERMS_OF_SERVICE_SUCCESS = 'auth/register/CHECK_FORM_TERMS_OF_SERVICE_SUCCESS'
