// Core
import reduceReducers from 'reduce-reducers'

// Action types
import * as types from './types'


const registerReducer = (state, action) => {
  switch (action.type) {
    case types.REGISTER:
      return {
        ...state,

        registerForm: {
          ...state.registerForm,

          error: {
            message: '',
            data: {},
            code: null,
          },

          registering: true,
          registerFailed: false,
          registerSucceeded: false,
        },
      }
    case types.REGISTER_FAIL:
      return {
        ...state,

        registerForm: {
          ...state.registerForm,

          error: {
            message: action.error.message,
            data: action.error.data,
            code: action.error.code,
          },

          registering: false,
          registerFailed: true,
          registerSucceeded: false,
        },
      }
    case types.REGISTER_SUCCESS:
      return {
        ...state,

        registerForm: {
          ...state.registerForm,

          error: {
            message: '',
            data: {},
            code: null,
          },

          registering: false,
          registerFailed: false,
          registerSucceeded: true,
        },
      }
    case types.REGISTER_RESET:
      return {
        ...state,

        registerForm: {
          ...state.registerForm,

          email: '',
          password: '',
          confirmPassword: '',
          businessName: '',
          countryCode: '',
          termsOfServiceChecked: false,

          error: {
            message: '',
            data: {},
            code: null,
          },

          readingUser: false,
          readUserFailed: false,
          readUserSucceeded: false,
        },
      }
    case types.CHANGE_FORM_EMAIL:
      return {
        ...state,

        registerForm: {
          ...state.registerForm,

          email: action.email,
        },
      }
    case types.CHANGE_FORM_EMAIL_FAIL:
      return {
        ...state,

        registerForm: {
          ...state.registerForm,

          error: {
            message: action.error.message,
            data: action.error.data,
            code: action.error.code,
          },
        },
      }
    case types.CHANGE_FORM_EMAIL_SUCCESS: {
      let data = {}
      if (state.registerForm.error) {
        data = {
          ...state.registerForm.error.data,
          email: '',
        }
      }

      return {
        ...state,

        registerForm: {
          ...state.registerForm,

          error: {
            ...state.registerForm.error,

            data,
          },
        },
      }
    }
    case types.CHANGE_FORM_PASSWORD:
      return {
        ...state,

        registerForm: {
          ...state.registerForm,

          password: action.password,
        },
      }
    case types.CHANGE_FORM_PASSWORD_FAIL:
      return {
        ...state,

        registerForm: {
          ...state.registerForm,

          error: {
            ...state.registerForm.error,

            data: {
              ...state.registerForm.error.data,

              password: action.error.data.pasword,
            },
          },
        },
      }
    case types.CHANGE_FORM_PASSWORD_SUCCESS:
      return {
        ...state,

        registerForm: {
          ...state.registerForm,

          error: {
            ...state.registerForm.error,

            data: {
              ...state.registerForm.error.data,

              password: '',
            },
          },
        },
      }
    case types.CHANGE_FORM_CONFIRM_PASSWORD:
      return {
        ...state,

        registerForm: {
          ...state.registerForm,

          confirmPassword: action.confirmPassword,
        },
      }
    case types.CHANGE_FORM_CONFIRM_PASSWORD_FAIL:
      return {
        ...state,

        registerForm: {
          ...state.registerForm,

          error: {
            ...state.registerForm.error,

            data: {
              ...state.registerForm.error.data,

              confirmPassword: action.error.data.confirmPassword,
            },
          },
        },
      }
    case types.CHANGE_FORM_CONFIRM_PASSWORD_SUCCESS:
      return {
        ...state,

        registerForm: {
          ...state.registerForm,

          error: {
            ...state.registerForm.error,

            data: {
              ...state.registerForm.error.data,

              confirmPassword: '',
            },
          },
        },
      }
    case types.CHANGE_FORM_BUSINESS_NAME:
      return {
        ...state,

        registerForm: {
          ...state.registerForm,

          businessName: action.businessName,
        },
      }
    case types.CHANGE_FORM_BUSINESS_NAME_FAIL:
      return {
        ...state,

        registerForm: {
          ...state.registerForm,

          error: {
            ...state.registerForm.error,

            data: {
              ...state.registerForm.error.data,

              businessName: action.error.data.businessName,
            },
          },
        },
      }
    case types.CHANGE_FORM_BUSINESS_NAME_SUCCESS:
      return {
        ...state,

        registerForm: {
          ...state.registerForm,

          error: {
            ...state.registerForm.error,

            data: {
              ...state.registerForm.error.data,

              businessName: '',
            },
          },
        },
      }
    case types.CHANGE_FORM_COUNTRY_CODE:
      return {
        ...state,

        registerForm: {
          ...state.registerForm,

          countryCode: action.countryCode,
        },
      }
    case types.CHANGE_FORM_COUNTRY_CODE_FAIL:
      return {
        ...state,

        registerForm: {
          ...state.registerForm,

          error: {
            ...state.registerForm.error,

            data: {
              ...state.registerForm.error.data,

              countryCode: action.error.data.countryCode,
            },
          },
        },
      }
    case types.CHANGE_FORM_COUNTRY_CODE_SUCCESS:
      return {
        ...state,

        registerForm: {
          ...state.registerForm,

          error: {
            ...state.registerForm.error,

            data: {
              ...state.registerForm.error.data,

              countryCode: '',
            },
          },
        },
      }

    case types.CHECK_FORM_TERMS_OF_SERVICE:
      return {
        ...state,

        registerForm: {
          ...state.registerForm,

          termsOfServiceChecked: action.termsOfServiceChecked,
        },
      }
    case types.CHECK_FORM_TERMS_OF_SERVICE_FAIL:
      return {
        ...state,

        registerForm: {
          ...state.registerForm,

          error: {
            ...state.registerForm.error,

            data: {
              ...state.registerForm.error.data,

              termsOfServiceChecked: action.error.data.termsOfServiceChecked,
            },
          },
        },
      }
    case types.CHECK_FORM_TERMS_OF_SERVICE_SUCCESS:
      return {
        ...state,

        registerForm: {
          ...state.registerForm,

          error: {
            ...state.registerForm.error,

            data: {
              ...state.registerForm.error.data,

              termsOfServiceChecked: null,
            },
          },
        },
      }
    default:
      return state
  }
}


export default reduceReducers(registerReducer)
