import reducer from './reducers'

import * as registerOperations from './operations'


export { registerOperations }

export default reducer
