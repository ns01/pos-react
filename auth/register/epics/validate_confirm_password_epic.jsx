// Core
import { Observable } from 'rxjs/Observable'

// Types
import { CHANGE_CONFIRM_PASSWORD } from '../types'

// Operations
import { changeConfirmPasswordSuccess, changeConfirmPasswordFail } from '../operations'


const validateConfirmPasswordEpic = (action$, store) =>
  action$.ofType(CHANGE_CONFIRM_PASSWORD)
    .debounceTime(1000)
    .mergeMap(action => (
      Observable.create((observer) => {
        const errors = {}

        if (action.confirmPassword.trim() === '') {
          errors.confirm_password = 'Confirm password is required'
        }

        if (store.getState().walletAuthReducer.signUpForm.password !== action.confirmPassword) {
          errors.confirm_password = 'Passwords don\'t match'
        }

        if (Object.getOwnPropertyNames(errors).length > 0) {
          observer.next(changeConfirmPasswordFail({ data: errors }))
          observer.complete()
          return
        }

        observer.next(changeConfirmPasswordSuccess())
        observer.complete()
      })
    ))

export default validateConfirmPasswordEpic
