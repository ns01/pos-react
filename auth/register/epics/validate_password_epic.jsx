// Core
import { Observable } from 'rxjs/Observable'

// Type
import { CHANGE_PASSWORD } from '../types'

// Operations
import { changePasswordSuccess, changePasswordFail } from '../operations'


const validatePasswordEpic = action$ =>
  action$.ofType(CHANGE_PASSWORD)
    .debounceTime(1000)
    .mergeMap(action => (
      Observable.create((observer) => {
        const errors = {}

        if (action.password.trim().length > 128) {
          errors.password = 'Password must be less than 128 characters'
        }

        if (action.password.trim().length < 6) {
          errors.password = 'Password must be at least 6 characters'
        }

        if (action.password.trim() === '') {
          errors.password = 'Password is required'
        }

        if (Object.getOwnPropertyNames(errors).length > 0) {
          observer.next(changePasswordFail({ data: errors }))
          observer.complete()
          return
        }

        observer.next(changePasswordSuccess())
        observer.complete()
      })
    ))

export default validatePasswordEpic
