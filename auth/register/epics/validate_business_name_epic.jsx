// Core
import api from 'bitpalModules/app/axios'
import { Observable } from 'rxjs/Observable'

// Type
import { CHANGE_USERNAME } from '../types'

// Operations
import { changeUsernameSuccess, changeUsernameFail } from '../operations'


const validateUsernameEpic = action$ =>
  action$.ofType(CHANGE_USERNAME)
    .debounceTime(1000)
    .mergeMap(action => (
      Observable.create((observer) => {
        const errors = {}

        if (action.username.trim().length > 63) {
          errors.username = 'Username must be less than 128 characters'
        }

        if (action.username.trim().length < 6) {
          errors.username = 'Username must be at least 6 characters'
        }

        if (action.username.trim() === '') {
          errors.username = 'Username is required'
        }

        if (Object.getOwnPropertyNames(errors).length > 0) {
          observer.next(changeUsernameFail({ data: errors }))
          observer.complete()
          return
        }

        api.post('/v1/accounts/signup/validate/username/', {
          username: action.username,
        }).then(() => {
          observer.next(changeUsernameSuccess())
          observer.complete()
        }).catch((error) => {
          observer.next(changeUsernameFail(error.response.data.errors))
          observer.complete()
        })
      })
    ))

export default validateUsernameEpic
