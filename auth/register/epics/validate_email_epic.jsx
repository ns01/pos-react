// Core
import api from 'posModules/app/axios'
import { Observable } from 'rxjs'
import { mergeMap, debounceTime } from 'rxjs/operators'
import { ofType } from 'redux-observable'

// Types
import { CHANGE_FORM_EMAIL } from '../types'

// Operations
import { changeFormEmailSuccess, changeFormEmailFail } from '../operations'


const validateEmailEpic = action$ => action$.pipe(
  ofType(CHANGE_FORM_EMAIL),
  debounceTime(1000),
  mergeMap(action => (
    Observable.create((observer) => {
      const formErrors = {}

      const emailRegEx = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

      if (!emailRegEx.test(action.email)) {
        formErrors.email = 'Email address is invalid'
      }

      if (action.email.trim() === '') {
        formErrors.email = 'Email address is required'
      }

      if (Object.getOwnPropertyNames(formErrors).length > 0) {
        observer.next(changeFormEmailFail({ data: formErrors }))
        observer.complete()
        return
      }

      api.post('/v1/users/signup_check/email/', {
        email: action.email,
      }).then(() => {
        observer.next(changeFormEmailSuccess())
        observer.complete()
      }).catch((error) => {
        const errorResponse = error.response.data.errors

        const errors = {
          data: errorResponse.data || {},
          message: errorResponse.message,
          code: errorResponse.code,
        }

        observer.next(changeFormEmailFail(errors.data, errors.message, errors.code))
        observer.complete()
      })
    })
  )),
)


export default validateEmailEpic
