// Core
import api from 'posModules/app/axios'
import { Observable } from 'rxjs'
import { mergeMap, debounceTime } from 'rxjs/operators'
import { ofType } from 'redux-observable'

// Operations
import { resendVerificationCodeOperations } from 'posModules/auth/resend_verification_code'
import { registerSuccess, registerFail } from '../operations'

// Types
import { REGISTER } from '../types';


const registerEpic = action$ => action$.pipe(
  ofType(REGISTER),
  debounceTime(1000),
  mergeMap(action => (
    Observable.create((observer) => {
      const formErrors = {}

      const emailRegEx = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

      if (!emailRegEx.test(action.email)) {
        formErrors.email = 'email address is invalid'
      }

      if (action.email.trim() === '') {
        formErrors.email = 'email address is required'
      }

      if (action.password.trim().length > 128) {
        formErrors.password = 'password must be less than 128 characters'
      }

      if (action.password.trim().length < 6) {
        formErrors.password = 'password must be at least 6 characters'
      }

      if (action.password.trim() === '') {
        formErrors.password = 'password is required'
      }

      if (action.confirmPassword.trim() === '') {
        formErrors.confirmPassword = 'confirm password is required'
      }

      if (action.password !== action.confirmPassword) {
        formErrors.confirmPassword = 'passwords don\'t match'
      }

      if (action.businessName.trim() === '') {
        formErrors.businessName = 'business name is required'
      }

      if (!action.termsOfServiceChecked) {
        formErrors.termsOfService = 'agreeing to the terms of service is required'
      }

      if (Object.getOwnPropertyNames(formErrors).length > 0) {
        observer.next(registerFail(formErrors))
        observer.complete()
        return
      }

      api.post('/v1/users/signup/', {
        email: action.email,
        password: action.password,
        business_name: action.businessName,
        country_code: action.countryCode,
      }).then((response) => {
        const verificationId = response.data.data.verification_id

        observer.next(resendVerificationCodeOperations.changeVerificationId(verificationId))

        observer.next(registerSuccess())
        observer.complete()
      }).catch((error) => {
        const errorResponse = error.response.data.errors

        const errors = {
          data: errorResponse.data || {},
          message: errorResponse.message,
          code: errorResponse.code,
        }

        observer.next(registerFail(errors.data, errors.message, errors.code))
        observer.complete()
      })
    })
  )),
)

export default registerEpic
