// Core
import api, { setAuthorizationHeader } from 'bitpalModules/app/axios'
import { Observable } from 'rxjs/Observable'

// Types
import { CONFIRM_SIGN_UP } from '../types'

// Operations
import {
  confirmSignUpSuccess,
  confirmSignUpFail,
  signUpReset,
  signUpConfirmReset,
  resendVerificationCodeReset,
} from '../operations'

import { loginConfirmSuccess } from 'bitpalModules/auth/actions/loginActions'


const confirmSignUpEpic = (action$, store) =>
  action$.ofType(CONFIRM_SIGN_UP)
    .debounceTime(500)
    .mergeMap(action => (
      Observable.create((observer) => {
        api.post('/v1/accounts/signup/code/', {
          email_or_username: store.getState().walletAuthReducer.signUpForm.email,
          code: action.code,
        }).then((response) => {
          const jwtToken = response.data.data.token
          const date = new Date()
          date.setTime(date.getTime() + (2.5 * 60 * 60 * 1000))
          const jwtExpiration = date.getTime()

          // Set jwt local store
          localStorage.setItem('jwtToken', jwtToken)
          localStorage.setItem('jwtExpiration', jwtExpiration)

          // Set authorization header
          setAuthorizationHeader(jwtToken)

          observer.next(confirmSignUpSuccess())
          observer.next(loginConfirmSuccess(jwtToken, jwtExpiration))
          observer.next(signUpReset())
          observer.next(resendVerificationCodeReset())
          observer.next(signUpConfirmReset())
          observer.complete()
        }).catch((error) => {
          observer.next(confirmSignUpFail(error.response.data.errors))
          observer.complete()
        })
      })
    ))

export default confirmSignUpEpic
