// Core
import { Observable } from 'rxjs/Observable'

// Type
import { CHANGE_TERMS_OF_SERVICE } from '../types'

// Operations
import { changeTermsOfServiceSuccess, changeTermsOfServiceFail } from '../operations'


const validateTermsOfServiceEpic = action$ =>
  action$.ofType(CHANGE_TERMS_OF_SERVICE)
    .mergeMap(action => (
      Observable.create((observer) => {
        const errors = {}

        if (!action.termsOfService) {
          errors.terms_of_service = 'Check the box to agree'
        }

        if (Object.getOwnPropertyNames(errors).length > 0) {
          observer.next(changeTermsOfServiceFail({ data: errors }))
          observer.complete()
          return
        }

        observer.next(changeTermsOfServiceSuccess())
        observer.complete()
      })
    ))

export default validateTermsOfServiceEpic
