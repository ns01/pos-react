// Core
import api from 'bitpalModules/app/axios'
import { toast } from 'react-toastify'
import { Observable } from 'rxjs/Observable'

// Types
import { RESEND_VERIFICATION_CODE } from '../types'

// Operations
import { resendVerificationCodeFail, resendVerificationCodeSuccess, changeVerificationId } from '../operations'


const resendVerificationCode = action$ =>
  action$.ofType(RESEND_VERIFICATION_CODE)
    .debounceTime(500)
    .mergeMap(action => (
      Observable.create((observer) => {
        api.post('/v1/accounts/signup/resend/', {
          verification_id: action.verificationId,
        }).then((response) => {
          const verificationId = response.data.data.verification_id
          observer.next(changeVerificationId(verificationId))

          toast.success('Successfully sent email')

          observer.next(resendVerificationCodeSuccess(verificationId))
          observer.complete()
        }).catch((error) => {
          toast.error('Fail to send email')
          observer.next(resendVerificationCodeFail(error.response.data.errors))
          observer.complete()
        })
      })
    ))

export default resendVerificationCode
