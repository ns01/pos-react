import * as types from './types'


export const register = (
  email,
  password,
  confirmPassword,
  businessName,
  countryCode,
  termsOfServiceChecked,
) => ({
  type: types.REGISTER,
  email,
  password,
  confirmPassword,
  businessName,
  countryCode,
  termsOfServiceChecked,
})


export const registerFail = (data = {}, message = '', code = null) => ({
  type: types.REGISTER_FAIL,
  error: {
    data,
    message,
    code,
  },
})


export const registerSuccess = () => ({
  type: types.REGISTER_SUCCESS,
})


export const registerReset = () => ({
  type: types.REGISTER_RESET,
})


export const changeFormEmail = email => ({
  type: types.CHANGE_FORM_EMAIL,
  email,
})


export const changeFormEmailFail = (data = {}, message = '', code = null) => ({
  type: types.CHANGE_FORM_EMAIL_FAIL,
  error: {
    data,
    message,
    code,
  },
})


export const changeFormEmailSuccess = () => ({
  type: types.CHANGE_FORM_EMAIL_SUCCESS,
})


export const changeFormPassword = password => ({
  type: types.CHANGE_FORM_PASSWORD,
  password,
})


export const changeFormPasswordFail = (data = {}, message = '', code = null) => ({
  type: types.CHANGE_FORM_PASSWORD_FAIL,
  error: {
    data,
    message,
    code,
  },
})


export const changeFormPasswordSuccess = () => ({
  type: types.CHANGE_FORM_PASSWORD_SUCCESS,
})


export const changeFormConfirmPassword = confirmPassword => ({
  type: types.CHANGE_FORM_CONFIRM_PASSWORD,
  confirmPassword,
})


export const changeFormConfirmPasswordFail = (data = {}, message = '', code = null) => ({
  type: types.CHANGE_FORM_CONFIRM_PASSWORD_FAIL,
  error: {
    data,
    message,
    code,
  },
})


export const changeFormConfirmPasswordSuccess = () => ({
  type: types.CHANGE_FORM_CONFIRM_PASSWORD_SUCCESS,
})

export const changeFormBusinessName = businessName => ({
  type: types.CHANGE_FORM_BUSINESS_NAME,
  businessName,
})


export const changeFormBusinessNameFail = (data = {}, message = '', code = null) => ({
  type: types.CHANGE_FORM_BUSINESS_NAME_FAIL,
  error: {
    data,
    message,
    code,
  },
})


export const changeFormBusinessNameSuccess = () => ({
  type: types.CHANGE_FORM_BUSINESS_NAME_SUCCESS,
})


export const changeFormCountryCode = countryCode => ({
  type: types.CHANGE_FORM_COUNTRY_CODE,
  countryCode,
})


export const changeFormCountryCodeFail = (data = {}, message = '', code = null) => ({
  type: types.CHANGE_FORM_COUNTRY_CODE_FAIL,
  error: {
    data,
    message,
    code,
  },
})


export const changeFormCountryCodeSuccess = () => ({
  type: types.CHANGE_FORM_COUNTRY_CODE_SUCCESS,
})


export const checkFormTermsOfService = termsOfServiceChecked => ({
  type: types.CHECK_FORM_TERMS_OF_SERVICE,
  termsOfServiceChecked,
})


export const checkFormTermsOfServiceFail = (data = {}, message = '', code = null) => ({
  type: types.CHECK_FORM_TERMS_OF_SERVICE_FAIL,
  error: {
    data,
    message,
    code,
  },
})


export const checkFormTermsOfServiceSuccess = () => ({
  type: types.CHECK_FORM_TERMS_OF_SERVICE_SUCCESS,
})
