// Core
import api, { setAuthorizationHeader } from 'posModules/app/axios'
import { Observable } from 'rxjs'
import { ofType } from 'redux-observable'
import { mergeMap, debounceTime } from 'rxjs/operators'
import camelcaseKeys from 'camelcase-keys'

// Action types
import { LOGIN } from '../types';

// Actions
import { loginSuccess, loginFail } from '../operations'


const loginEpic = action$ => action$.pipe(
  ofType(LOGIN),
  debounceTime(1000),
  mergeMap(action => (
    Observable.create((observer) => {
      const formErrors = {}

      const emailRegEx = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

      if (!emailRegEx.test(action.email)) {
        formErrors.email = 'email address is invalid'
      }

      if (action.email.trim() === '') {
        formErrors.email = 'email address is required'
      }

      if (action.password.trim() === '') {
        formErrors.password = 'password is required'
      }

      if (Object.getOwnPropertyNames(formErrors).length > 0) {
        observer.next(loginFail(formErrors, undefined, 422))
        observer.complete()
        return
      }

      api.post('/v1/users/login/', {
        email: action.email,
        password: action.password,
      }).then((response) => {
        const jwtToken = response.data.data.token
        const date = new Date()
        date.setTime(date.getTime() + (2.5 * 60 * 60 * 1000))
        const jwtExpiration = date.getTime()

        // Set jwt local store
        localStorage.setItem('jwtToken', jwtToken)
        localStorage.setItem('jwtExpiration', jwtExpiration)

        // Set authorization header
        setAuthorizationHeader(jwtToken)

        observer.next(loginSuccess(jwtToken, jwtExpiration))
        observer.complete()
      }).catch((error) => {
        const errorResponse = camelcaseKeys(error.response.data.errors, { deep: true })

        const data = errorResponse.data || {}

        if (errorResponse.code === 401) {
          data.email = 'email does not match'
          data.password = 'password does not match'
        }

        const serverError = {
          data,
          message: errorResponse.message,
          code: errorResponse.code,
        }

        observer.next(loginFail(serverError.data, serverError.message, serverError.code))
        observer.complete()
      })
    })
  )),
)

export default loginEpic
