// Core
import reduceReducers from 'reduce-reducers'

// Action types
import * as types from './types'


const loginReducer = (state, action) => {
  switch (action.type) {
    case types.LOGIN:
      return {
        ...state,

        loginForm: {
          ...state.loginForm,

          email: action.email,
          password: action.password,

          error: {
            message: '',
            data: {},
            code: null,
          },

          loggingIn: true,
          loginFailed: false,
          loginSucceeded: false,
        },
      }
    case types.LOGIN_FAIL:
      return {
        ...state,

        loginForm: {
          ...state.loginForm,

          error: {
            message: action.errors.message,
            data: action.errors.data,
            code: action.errors.code,
          },

          loggingIn: false,
          loginFailed: true,
          loginSucceeded: false,
        },
      }
    case types.LOGIN_SUCCESS:
      return {
        ...state,

        isAuthenticated: true,
        jwtToken: action.jwtToken,
        jwtExpiration: action.jwtExpiration,

        loginForm: {
          ...state.loginForm,

          error: {
            message: '',
            data: {},
            code: null,
          },

          loggingIn: false,
          loginFailed: false,
          loginSucceeded: true,
        },
      }
    case types.LOGIN_RESET:
      return {
        ...state,

        loginForm: {
          ...state.loginForm,

          email: '',
          password: '',

          error: {
            message: '',
            data: {},
            code: null,
          },

          loggingIn: false,
          loginFailed: false,
          loginSucceeded: false,
        },
      }
    case types.CHANGE_FORM_EMAIL:
      return {
        ...state,

        loginForm: {
          ...state.loginForm,

          email: action.email,
        },
      }
    case types.CHANGE_FORM_PASSWORD:
      return {
        ...state,

        loginForm: {
          ...state.loginForm,

          password: action.password,
        },
      }
    default:
      return state
  }
}


export default reduceReducers(loginReducer)
