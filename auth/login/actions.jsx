import * as types from './types';


export const login = (email, password) => ({
  type: types.LOGIN,
  email,
  password,
})

export const loginFail = (data = {}, message = '', code = null) => ({
  type: types.LOGIN_FAIL,
  errors: {
    data,
    message,
    code,
  },
})

export const loginSuccess = (jwtToken, jwtExpiration) => ({
  type: types.LOGIN_SUCCESS,
  jwtToken,
  jwtExpiration,
})

export const loginReset = () => ({
  type: types.LOGIN_RESET,
})

export const changeFormEmail = email => ({
  type: types.CHANGE_FORM_EMAIL,
  email,
})

export const changeFormPassword = password => ({
  type: types.CHANGE_FORM_PASSWORD,
  password,
})
