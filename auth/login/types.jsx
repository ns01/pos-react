export const LOGIN = 'auth/login/LOGIN'
export const LOGIN_FAIL = 'auth/login/LOGIN_FAIL'
export const LOGIN_SUCCESS = 'auth/login/LOGIN_SUCCESS'
export const LOGIN_RESET = 'auth/login/LOGIN_RESET'

export const CHANGE_FORM_EMAIL = 'auth/login/CHANGE_FORM_EMAIL'
export const CHANGE_FORM_PASSWORD = 'auth/login/CHANGE_FORM_PASSWORD'
