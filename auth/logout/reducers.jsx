// Core
import reduceReducers from 'reduce-reducers'

// Action types
import * as types from './types'


const logoutReducer = (state, action) => {
  switch (action.type) {
    case types.LOGOUT_SUCCESS: {
      return {
        ...state,

        isAuthenticated: false,
        jwtToken: '',
        jwtExpiration: null,
        logoutSucceeded: true,
      }
    }
    default:
      return state
  }
}


export default reduceReducers(logoutReducer)
