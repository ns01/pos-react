import * as types from './types'


export const logout = () => ({
  type: types.LOGOUT,
})

export const logoutSuccess = () => ({
  type: types.LOGOUT_SUCCESS,
})
