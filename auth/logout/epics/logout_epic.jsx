// Core
import { setAuthorizationHeader } from 'posModules/app/axios'
import { Observable } from 'rxjs'
import { ofType } from 'redux-observable'
import { mergeMap, debounceTime } from 'rxjs/operators'

// Types
import { LOGOUT } from '../types';

// Operations
import { logoutSuccess } from '../operations'


const logoutEpic = action$ => action$.pipe(
  ofType(LOGOUT),
  debounceTime(500),
  mergeMap(() => (
    Observable.create((observer) => {
      // Remove jwt local storage
      localStorage.removeItem('jwtToken')
      localStorage.removeItem('jwtExpiration')

      // Set authorization header
      setAuthorizationHeader(null)

      observer.next(logoutSuccess())
      observer.complete()
    })
  )),
)

export default logoutEpic
