// Core
import api from 'posModules/app/axios'
import { Observable } from 'rxjs'
import { mergeMap } from 'rxjs/operators'
import { ofType } from 'redux-observable'
import camelcaseKeys from 'camelcase-keys'

// Action types
import { RESEND_VERIFICATION_CODE } from '../types';

// Actions
import { resendVerificationCodeSuccess, resendVerificationCodeFail, changeVerificationId } from '../operations'


const resendVerificationCodeEpic = action$ => action$.pipe(
  ofType(RESEND_VERIFICATION_CODE),
  mergeMap(action => (
    Observable.create((observer) => {
      api.post('/v1/accounts/signup/resend/', {
        verification_id: action.verificationId,
      }).then((response) => {
        const verificationId = response.data.data.verification_id
        observer.next(changeVerificationId(verificationId))

        observer.next(resendVerificationCodeSuccess())
        observer.complete()
      }).catch((error) => {
        const errorResponse = camelcaseKeys(error.response.data.errors, { deep: true })

        const errors = {
          data: errorResponse.data || {},
          message: errorResponse.message,
          code: errorResponse.code,
        }

        observer.next(resendVerificationCodeFail(errors.data, errors.message, errors.code))
        observer.complete()
      })
    })
  )),
)


export default resendVerificationCodeEpic
