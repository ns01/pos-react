import * as types from './types';


export const changeVerificationId = verificationId => ({
  type: types.CHANGE_VERIFICATION_Id,
  verificationId,
})

export const resendVerificationCode = () => ({
  type: types.RESEND_VERIFICATION_CODE,
})

export const resendVerificationCodeFail = (data = {}, message = '', code = null) => ({
  type: types.RESEND_VERIFICATION_CODE_FAIL,
  error: {
    data,
    message,
    code,
  },
})

export const resendVerificationCodeSuccess = () => ({
  type: types.RESEND_VERIFICATION_CODE_SUCCESS,
})

export const resendVerificationCodeReset = () => ({
  type: types.RESEND_VERIFICATION_CODE_RESET,
})
