// Core
import reduceReducers from 'reduce-reducers'

// Action types
import * as types from './types'


const resendVerificationCodeReducer = (state, action) => {
  switch (action.type) {
    case types.CHANGE_VERIFICATION_Id:
      return {
        ...state,

        resendVerificationCodeForm: {
          ...state.resendVerificationCodeForm,

          verificationId: action.verificationId,
        },
      }
    case types.RESEND_VERIFICATION_CODE:
      return {
        ...state,

        resendVerificationCodeForm: {
          ...state.resendVerificationCodeForm,

          verificationId: action.verificationId,

          error: {
            message: '',
            data: {},
            code: null,
          },

          resendingCode: true,
          resendCodeFailed: false,
          resendCodeSucceeded: false,
        },
      }
    case types.RESEND_VERIFICATION_CODE_FAIL:
      return {
        ...state,

        resendVerificationCodeForm: {
          ...state.resendVerificationCodeForm,

          error: {
            message: action.error.message,
            data: action.error.data,
            code: action.error.code,
          },

          resendingCode: false,
          resendCodeFailed: true,
          resendCodeSucceeded: false,
        },
      }
    case types.RESEND_VERIFICATION_CODE_SUCCESS:
      return {
        ...state,

        resendVerificationCodeForm: {
          ...state.resendVerificationCodeForm,

          error: {
            message: '',
            data: {},
            code: null,
          },

          resendingCode: false,
          resendCodeFailed: false,
          resendCodeSucceeded: true,
        },
      }
    case types.RESEND_VERIFICATION_CODE_RESET:
      return {
        ...state,

        resendVerificationCodeForm: {
          verificationId: null,

          error: {
            message: '',
            data: {},
            code: null,
          },

          resendingCode: false,
          resendCodeFailed: false,
          resendCodeSucceeded: false,
        },
      }
    default:
      return state
  }
}


export default reduceReducers(resendVerificationCodeReducer)
