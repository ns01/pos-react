// Core
import reduceReducers from 'reduce-reducers'

// Reducers
import registerReducer from './register'
import verifyReducer from './verify'
import resendVerificationCodeReducer from './resend_verification_code'
import loginReducer from './login'
import logoutReducer from './logout'

// State shape
const initialState = {
  isAuthenticated: false,
  jwtToken: '',
  jwtExpiration: null,

  logoutSucceeded: false,

  registerForm: {

    // Inputs
    email: '',
    password: '',
    confirmPassword: '',
    businessName: '',
    countryCode: '',
    termsOfServiceChecked: false,

    // Error
    error: {
      message: '',
      data: {},
      code: null,
    },

    // Form status
    registering: false,
    registerFailed: false,
    registerSucceeded: false,
  },

  verifyForm: {
    verificationCode: '',

    error: {
      message: '',
      data: {},
      code: null,
    },

    verifying: false,
    verifyFailed: false,
    verifySucceeded: false,
  },

  resendVerificationForm: {
    verificationId: null,

    error: {
      message: '',
      data: {},
      code: null,
    },

    resendingCode: false,
    resendCodeFailed: false,
    resendCodeSucceeded: false,
  },

  loginForm: {

    email: '',
    password: '',

    error: {
      message: '',
      data: {},
      code: null,
    },

    loggingIn: true,
    loginFailed: false,
    loginSucceeded: false,
  },
}

// Initial reducer
const initialReducer = (state = initialState) => (
  state
)


export default reduceReducers(
  initialReducer,
  registerReducer,
  verifyReducer,
  resendVerificationCodeReducer,
  loginReducer,
  logoutReducer,
)
