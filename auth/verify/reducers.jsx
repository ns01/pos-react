// Core
import reduceReducers from 'reduce-reducers'

// Action types
import * as types from './types'


const verifyReducer = (state, action) => {
  switch (action.type) {
    case types.CHANGE_FORM_VERIFICATION_CODE:
      return {
        ...state,

        verifyForm: {
          ...state.verifyForm,

          verificationCode: action.verificationCode,
        },
      }
    case types.VERIFY:
      return {
        ...state,

        verifyForm: {
          ...state.verifyForm,

          error: {
            message: '',
            data: {},
            code: null,
          },

          verifying: true,
          verifyFailed: false,
          verifySucceeded: false,
        },
      }
    case types.VERIFY_FAIL:
      return {
        ...state,

        verifyForm: {
          ...state.verifyForm,

          error: {
            message: action.error.message,
            data: action.error.data,
            code: action.error.code,
          },

          verifying: false,
          verifyFailed: true,
          verifySucceeded: false,
        },
      }
    case types.VERIFY_SUCCESS:
      return {
        ...state,

        isAuthenticated: true,
        jwtToken: action.jwtToken,
        jwtExpiration: action.jwtExpiration,

        verifyForm: {
          ...state.verifyForm,

          error: {
            message: '',
            data: {},
            code: null,
          },

          verifying: false,
          verifyFailed: false,
          verifySucceeded: true,
        },
      }
    case types.VERIFY_RESET:
      return {
        ...state,

        verifyForm: {
          verificationCode: '',

          error: {
            message: '',
            data: {},
            code: null,
          },

          verifying: false,
          verifyFailed: false,
          verifySucceeded: false,
        },
      }
    default:
      return state
  }
}


export default reduceReducers(verifyReducer)
