export const VERIFY = 'auth/verify/VERIFY'
export const VERIFY_FAIL = 'auth/verify/VERIFY_FAIL'
export const VERIFY_SUCCESS = 'auth/verify/VERIFY_SUCCESS'
export const VERIFY_RESET = 'auth/verify/VERIFY_RESET'

export const CHANGE_FORM_VERIFICATION_CODE = 'auth/verify/CHANGE_FORM_VERIFICATION_CODE'
