// Core
import api, { setAuthorizationHeader } from 'posModules/app/axios'
import { Observable } from 'rxjs'
import { mergeMap } from 'rxjs/operators'
import { ofType } from 'redux-observable'
import camelcaseKeys from 'camelcase-keys'

// Action types
import { VERIFY } from '../types';

// Operations
import { verifySuccess, verifyFail } from '../operations'

const verifyEpic = action$ => action$.pipe(
  ofType(VERIFY),
  mergeMap(action => (
    Observable.create((observer) => {
      const formErrors = {}

      if (action.verificationCode.trim() === '') {
        formErrors.verificationCode = 'verification code is required'
      }

      if (Object.getOwnPropertyNames(formErrors).length > 0) {
        observer.next(verifyFail(formErrors))
        observer.complete()
        return
      }

      api.post('/v1/users/signup/code/', {
        email: action.email,
        code: action.verificationCode,
      }).then((response) => {
        const jwtToken = response.data.data.token
        const date = new Date()
        date.setTime(date.getTime() + (2.5 * 60 * 60 * 1000))
        const jwtExpiration = date.getTime()

        // Set jwt local store
        localStorage.setItem('jwtToken', jwtToken)
        localStorage.setItem('jwtExpiration', jwtExpiration)

        // Set authorization header
        setAuthorizationHeader(jwtToken)

        observer.next(verifySuccess(jwtToken, jwtExpiration))
        observer.complete()
      }).catch((error) => {
        console.log(error)

        const errorResponse = camelcaseKeys(error.response.data.errors, { deep: true })

        const data = errorResponse.data || {}

        if (errorResponse.code === 404) {
          data.verificationCode = errorResponse.message
        }

        const errors = {
          data,
          message: errorResponse.message,
          code: errorResponse.code,
        }

        observer.next(verifyFail(errors.data, errors.message, errors.code))
        observer.complete()
      })
    })
  )),
)


export default verifyEpic
