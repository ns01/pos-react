import * as types from './types';


export const changeFormVerificationCode = verificationCode => ({
  type: types.CHANGE_FORM_VERIFICATION_CODE,
  verificationCode,
})

export const verify = (verificationCode, email) => ({
  type: types.VERIFY,
  verificationCode,
  email,
})

export const verifyFail = (data = {}, message = '', code = null) => ({
  type: types.VERIFY_FAIL,
  error: {
    data,
    message,
    code,
  },
})

export const verifySuccess = (jwtToken, jwtExpiration) => ({
  type: types.VERIFY_SUCCESS,
  jwtToken,
  jwtExpiration,
})

export const verifyReset = () => ({
  type: types.VERIFY_RESET,
})
