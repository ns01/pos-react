import * as types from './types'


export const updateCustomer = (
  customerId,

  name,
  email,
  number,
  note,
) => ({
  type: types.UPDATE_CUSTOMER,
  customerId,

  name,
  email,
  number,
  note,
})

export const updateCustomerFail = (data = {}, message = '', code = null) => ({
  type: types.UPDATE_CUSTOMER_FAIL,
  error: {
    data,
    message,
    code,
  },
})

export const updateCustomerSuccess = customer => ({
  type: types.UPDATE_CUSTOMER_SUCCESS,
  customer,
})

export const updateCustomerReset = () => ({
  type: types.UPDATE_CUSTOMER_RESET,
})

export const setCustomer = customer => ({
  type: types.SET_CUSTOMER,
  customer,
})

export const changeFormName = name => ({
  type: types.CHANGE_FORM_NAME,
  name,
})

export const changeFormEmail = email => ({
  type: types.CHANGE_FORM_EMAIL,
  email,
})

export const changeFormNumber = number => ({
  type: types.CHANGE_FORM_NUMBER,
  number,
})

export const changeFormNote = note => ({
  type: types.CHANGE_FORM_NOTE,
  note,
})

export const openUpdateCustomerDialog = () => ({
  type: types.OPEN_UPDATE_CUSTOMER_DIALOG,
})

export const closeUpdateCustomerDialog = () => ({
  type: types.CLOSE_UPDATE_CUSTOMER_DIALOG,
})
