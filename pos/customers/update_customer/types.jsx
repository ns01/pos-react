export const UPDATE_CUSTOMER = 'pos/customers/update_customer/UPDATE_CUSTOMER'
export const UPDATE_CUSTOMER_FAIL = 'pos/customers/update_customer/UPDATE_CUSTOMER_FAIL'
export const UPDATE_CUSTOMER_SUCCESS = 'pos/customers/update_customer/UPDATE_CUSTOMER_SUCCESS'
export const UPDATE_CUSTOMER_RESET = 'pos/customers/update_customer/UPDATE_CUSTOMER_RESET'

export const SET_CUSTOMER = 'pos/customers/update_customer/SET_CUSTOMER'

// Inputs
export const CHANGE_FORM_NAME = 'pos/customers/update_customer/CHANGE_FORM_NAME'
export const CHANGE_FORM_EMAIL = 'pos/customers/update_customer/CHANGE_FORM_EMAIL'
export const CHANGE_FORM_NUMBER = 'pos/customers/update_customer/CHANGE_FORM_NUMBER'
export const CHANGE_FORM_NOTE = 'pos/customers/update_customer/CHANGE_FORM_NOTE'

export const OPEN_UPDATE_CUSTOMER_DIALOG = 'pos/customers/update_customer/OPEN_UPDATE_CUSTOMER_DIALOG'
export const CLOSE_UPDATE_CUSTOMER_DIALOG = 'pos/customers/update_customer/CLOSE_UPDATE_CUSTOMER_DIALOG'
