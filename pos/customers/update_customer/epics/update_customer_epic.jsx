// Core
import api from 'posModules/app/axios'
import { Observable } from 'rxjs'
import { mergeMap, debounceTime } from 'rxjs/operators'
import { ofType } from 'redux-observable'
import camelcaseKeys from 'camelcase-keys'

// Operations
import { updateCustomerSuccess, updateCustomerFail } from '../operations'

// Types
import { UPDATE_CUSTOMER } from '../types';


const updateCustomerEpic = action$ => action$.pipe(
  ofType(UPDATE_CUSTOMER),
  debounceTime(1000),
  mergeMap(action => (
    Observable.create((observer) => {
      const formErrors = {}

      if (action.name.trim() === '') {
        formErrors.name = 'name is required'
      }

      if (Object.getOwnPropertyNames(formErrors).length > 0) {
        observer.next(updateCustomerFail(formErrors))
        observer.complete()
        return
      }

      api.put(`/v1/users/customers/${action.customerId}`, {
        name: action.name,
        email: action.email,
        number: action.number,
        note: action.note,
      }).then((response) => {
        const customer = camelcaseKeys(response.data.data, { deep: true })

        observer.next(updateCustomerSuccess(customer))
        observer.complete()
      }).catch((error) => {
        const errorResponse = camelcaseKeys(error.response.data.errors, { deep: true })

        const errors = {
          data: errorResponse.data || {},
          message: errorResponse.message,
          code: errorResponse.code,
        }

        observer.next(updateCustomerFail(errors.data, errors.message, errors.code))
        observer.complete()
      })
    })
  )),
)

export default updateCustomerEpic
