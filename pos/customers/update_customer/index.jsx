import reducer from './reducers'

import * as updateCustomerOperations from './operations'


export { updateCustomerOperations }

export default reducer
