import {
  updateCustomer,
  updateCustomerFail,
  updateCustomerSuccess,
  updateCustomerReset,

  setCustomer,

  changeFormName,
  changeFormEmail,
  changeFormNumber,
  changeFormNote,

  openUpdateCustomerDialog,
  closeUpdateCustomerDialog,
} from './actions'


export {
  updateCustomer,
  updateCustomerFail,
  updateCustomerSuccess,
  updateCustomerReset,

  setCustomer,

  changeFormName,
  changeFormEmail,
  changeFormNumber,
  changeFormNote,

  openUpdateCustomerDialog,
  closeUpdateCustomerDialog,
}
