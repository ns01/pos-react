// Core
import reduceReducers from 'reduce-reducers'

// Action types
import * as types from './types'


const updateCustomerReducer = (state, action) => {
  switch (action.type) {
    case types.UPDATE_CUSTOMER:
      return {
        ...state,

        updateCustomerForm: {
          ...state.updateCustomerForm,

          error: {
            message: '',
            data: {},
            code: null,
          },

          updatingCustomer: true,
          updateCustomerFailed: false,
          updateCustomerSucceeded: false,
        },
      }
    case types.UPDATE_CUSTOMER_FAIL: {
      return {
        ...state,

        updateCustomerForm: {
          ...state.updateCustomerForm,

          error: {
            message: action.error.message,
            data: action.error.data,
            code: action.error.code,
          },

          updatingCustomer: false,
          updateCustomerFailed: true,
          updateCustomerSucceeded: false,
        },
      }
    }
    case types.UPDATE_CUSTOMER_SUCCESS: {
      return {
        ...state,

        customersData: {
          ...state.customersData,

          customers: {
            ...state.customersData.customers,

            data: {
              ...state.customersData.customers.data,

              [action.customer.id]: action.customer,
            },
          },
        },

        updateCustomerForm: {
          ...state.updateCustomerForm,

          error: {
            message: '',
            data: {},
            code: null,
          },

          updatingCustomer: false,
          updateCustomerFailed: false,
          updateCustomerSucceeded: true,
        },
      }
    }
    case types.UPDATE_CUSTOMER_RESET:
      return {
        ...state,

        updateCustomerForm: {
          isUpdateCustomerDialogOpened: false,

          customer: {},
          name: '',
          email: '',
          number: '',
          note: '',

          error: {
            message: '',
            data: {},
            code: null,
          },

          updatingCustomer: false,
          updateCustomerFailed: false,
          updateCustomerSucceeded: false,
        },
      }
    case types.SET_CUSTOMER:
      return {
        ...state,

        updateCustomerForm: {
          ...state.updateCustomerForm,

          customer: action.customer,
        },
      }
    case types.CHANGE_FORM_NAME:
      return {
        ...state,

        updateCustomerForm: {
          ...state.updateCustomerForm,

          name: action.name,
        },
      }
    case types.CHANGE_FORM_EMAIL:
      return {
        ...state,

        updateCustomerForm: {
          ...state.updateCustomerForm,

          email: action.email,
        },
      }
    case types.CHANGE_FORM_NUMBER:
      return {
        ...state,

        updateCustomerForm: {
          ...state.updateCustomerForm,

          number: action.number,
        },
      }
    case types.CHANGE_FORM_NOTE:
      return {
        ...state,

        updateCustomerForm: {
          ...state.updateCustomerForm,

          note: action.note,
        },
      }
    case types.OPEN_UPDATE_CUSTOMER_DIALOG:
      return {
        ...state,

        updateCustomerForm: {
          ...state.updateCustomerForm,

          isUpdateCustomerDialogOpened: true,
        },
      }
    case types.CLOSE_UPDATE_CUSTOMER_DIALOG:
      return {
        ...state,

        updateCustomerForm: {
          ...state.updateCustomerForm,

          isUpdateCustomerDialogOpened: false,
        },
      }
    default:
      return state
  }
}


export default reduceReducers(updateCustomerReducer)
