// Core
import reduceReducers from 'reduce-reducers'

// Reducers
import createCustomerReducer from './create_customer'
import readCustomersReducer from './read_customers'
import updateCustomerReducer from './update_customer'
import deleteCustomerReducer from './delete_customer'


// State shape
const initialState = {
  customersData: {
    // Data
    customers: {
      result: [],
      data: {},
    },

    // Error
    error: {
      message: '',
      data: {},
      code: null,
    },

    // Status
    readingCustomers: false,
    readCustomersFailed: false,
    readCustomersSucceeded: false,
  },

  createCustomerForm: {
    isAddCustomerDialogOpened: false,

    // Input
    name: '',
    email: '',
    number: '',
    note: '',

    // Error
    error: {
      message: '',
      data: {},
      code: null,
    },

    // Status
    creatingCustomer: false,
    createCustomerFailed: false,
    createCustomerSucceeded: false,
  },

  updateCustomerForm: {
    isUpdateCustomerDialogOpened: false,

    // Input
    customer: {},

    name: '',
    email: '',
    number: '',
    note: '',

    // Error
    error: {
      message: '',
      data: {},
      code: null,
    },

    // Status
    updatingCustomer: false,
    updateCustomerFailed: false,
    updateCustomerSucceeded: false,
  },

  deleteCustomerForm: {
    isRemoveCustomerDialogOpened: false,

    customer: {},

    // Error
    error: {
      message: '',
      data: {},
      code: null,
    },

    // Status
    deletingCustomer: false,
    deleteCustomerFailed: false,
    deleteCustomerSucceeded: false,
  },
}

// Initial reducer
const initialReducer = (state = initialState) => (
  state
)

export default reduceReducers(
  initialReducer,
  createCustomerReducer,
  readCustomersReducer,
  updateCustomerReducer,
  deleteCustomerReducer,
)
