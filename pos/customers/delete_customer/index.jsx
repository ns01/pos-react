import reducer from './reducers'

import * as deleteCustomerOperations from './operations'


export { deleteCustomerOperations }

export default reducer
