export const DELETE_CUSTOMER = 'pos/customers/delete_customer/DELETE_CUSTOMER'
export const DELETE_CUSTOMER_FAIL = 'pos/customers/delete_customer/DELETE_CUSTOMER_FAIL'
export const DELETE_CUSTOMER_SUCCESS = 'pos/customers/delete_customer/DELETE_CUSTOMER_SUCCESS'
export const DELETE_CUSTOMER_RESET = 'pos/customers/delete_customer/DELETE_CUSTOMER_RESET'

export const SET_CUSTOMER = 'pos/customers/delete_customer/SET_CUSTOMER'

export const OPEN_DELETE_CUSTOMER_DIALOG = 'pos/customers/delete_customer/OPEN_DELETE_CUSTOMER_DIALOG'
export const CLOSE_DELETE_CUSTOMER_DIALOG = 'pos/customers/delete_customer/CLOSE_DELETE_CUSTOMER_DIALOG'
