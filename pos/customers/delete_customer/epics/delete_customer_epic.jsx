// Core
import api from 'posModules/app/axios'
import { Observable } from 'rxjs'
import { mergeMap, debounceTime } from 'rxjs/operators'
import { ofType } from 'redux-observable'
import camelcaseKeys from 'camelcase-keys'

// Operations
import { deleteCustomerSuccess, deleteCustomerFail } from '../operations'

// Types
import { DELETE_CUSTOMER } from '../types';


const deleteCustomerEpic = action$ => action$.pipe(
  ofType(DELETE_CUSTOMER),
  debounceTime(1000),
  mergeMap(action => (
    Observable.create((observer) => {
      api.delete(`/v1/users/customers/${action.customerId}`).then(() => {
        observer.next(deleteCustomerSuccess(action.customerId))
        observer.complete()
      }).catch((error) => {
        const errorResponse = camelcaseKeys(error.response.data.errors, { deep: true })

        const errors = {
          data: errorResponse.data || {},
          message: errorResponse.message,
          code: errorResponse.code,
        }

        observer.next(deleteCustomerFail(errors.data, errors.message, errors.code))
        observer.complete()
      })
    })
  )),
)

export default deleteCustomerEpic
