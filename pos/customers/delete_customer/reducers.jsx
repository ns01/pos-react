// Core
import reduceReducers from 'reduce-reducers'

// Action types
import * as types from './types'


const deleteCustomerReducer = (state, action) => {
  switch (action.type) {
    case types.DELETE_CUSTOMER:
      return {
        ...state,

        deleteCustomerForm: {
          ...state.deleteCustomerForm,

          error: {
            message: '',
            data: {},
            code: null,
          },

          deletingCustomer: true,
          deleteCustomerFailed: false,
          deleteCustomerSucceeded: false,
        },
      }
    case types.DELETE_CUSTOMER_FAIL: {
      return {
        ...state,

        deleteCustomerForm: {
          ...state.deleteCustomerForm,

          error: {
            message: action.error.message,
            data: action.error.data,
            code: action.error.code,
          },

          deletingCustomer: false,
          deleteCustomerFailed: true,
          deleteCustomerSucceeded: false,
        },
      }
    }
    case types.DELETE_CUSTOMER_SUCCESS: {
      const { data, result: customerResult } = state.customersData.customers
      delete data[action.customerId]

      const result = customerResult.filter(customerId => customerId !== action.customerId)

      return {
        ...state,

        customersData: {
          ...state.customersData,

          customers: {
            data,
            result,
          },
        },

        deleteCustomerForm: {
          ...state.deleteCustomerForm,

          error: {
            message: '',
            data: {},
            code: null,
          },

          deletingCustomer: false,
          deleteCustomerFailed: false,
          deleteCustomerSucceeded: true,
        },
      }
    }
    case types.DELETE_CUSTOMER_RESET:
      return {
        ...state,

        deleteCustomerForm: {
          isRemoveCustomerDialogOpened: false,
          customer: {},

          error: {
            message: '',
            data: {},
            code: null,
          },

          deletingCustomer: false,
          deleteCustomerFailed: false,
          deleteCustomerSucceeded: false,
        },
      }
    case types.SET_CUSTOMER:
      return {
        ...state,

        deleteCustomerForm: {
          ...state.deleteCustomerForm,

          customer: action.customer,
        },
      }
    case types.OPEN_DELETE_CUSTOMER_DIALOG:
      return {
        ...state,

        deleteCustomerForm: {
          ...state.deleteCustomerForm,

          isRemoveCustomerDialogOpened: true,
        },
      }
    case types.CLOSE_DELETE_CUSTOMER_DIALOG:
      return {
        ...state,

        deleteCustomerForm: {
          ...state.deleteCustomerForm,

          isRemoveCustomerDialogOpened: false,
        },
      }
    default:
      return state
  }
}


export default reduceReducers(deleteCustomerReducer)
