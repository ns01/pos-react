// Core
import api from 'posModules/app/axios'
import { Observable } from 'rxjs'
import { mergeMap } from 'rxjs/operators'
import { ofType } from 'redux-observable'
import normalize from 'posModules/app/helper'
import camelcaseKeys from 'camelcase-keys'

// Types
import { READ_CUSTOMERS } from '../types';

// Operations
import { readCustomersSuccess, readCustomersFail } from '../operations'


const readCustomersEpic = action$ => action$.pipe(
  ofType(READ_CUSTOMERS),
  mergeMap(() => (
    Observable.create((observer) => {
      api.get('/v1/users/customers/').then((response) => {
        const customers = camelcaseKeys(response.data.data, { deep: true })
        const customersData = normalize(customers, 'id')

        observer.next(readCustomersSuccess(customersData))
        observer.complete()
      }).catch((error) => {
        const errorResponse = camelcaseKeys(error.response.data.errors, { deep: true })

        const errors = {
          data: errorResponse.data || {},
          message: errorResponse.message,
          code: errorResponse.code,
        }

        observer.next(readCustomersFail(errors.data, errors.message, errors.code))
        observer.complete()
      })
    })
  )),
)


export default readCustomersEpic
