// Core
import reduceReducers from 'reduce-reducers'

// Action types
import * as types from './types'


const readCustomersReducer = (state, action) => {
  switch (action.type) {
    case types.READ_CUSTOMERS:
      return {
        ...state,

        customersData: {
          ...state.customersData,

          error: {
            message: '',
            data: {},
            code: null,
          },

          readingCustomers: true,
          readCustomersFailed: false,
          readCustomersSucceeded: false,
        },
      }
    case types.READ_CUSTOMERS_FAIL:
      return {
        ...state,

        customersData: {
          ...state.customersData,

          error: {
            message: action.errors.message,
            data: action.errors.data,
            code: action.errors.code,
          },

          readingCustomers: false,
          readCustomersFailed: true,
          readCustomersSucceeded: false,
        },
      }
    case types.READ_CUSTOMERS_SUCCESS:
      return {
        ...state,

        customersData: {
          ...state.customersData,

          customers: {
            result: action.customersData.result,
            data: action.customersData.data,
          },

          error: {
            message: '',
            data: {},
            code: null,
          },

          readingCustomers: false,
          readCustomersFailed: false,
          readCustomersSucceeded: true,
        },
      }
    default:
      return state
  }
}


export default reduceReducers(readCustomersReducer)
