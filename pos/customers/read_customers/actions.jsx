import * as types from './types'


export const readCustomers = () => ({
  type: types.READ_CUSTOMERS,
})


export const readCustomersFail = (data = {}, message = '', code = null) => ({
  type: types.READ_CUSTOMERS_FAIL,
  errors: {
    data,
    message,
    code,
  },
})


export const readCustomersSuccess = customersData => ({
  type: types.READ_CUSTOMERS_SUCCESS,
  customersData,
})
