export const READ_CUSTOMERS = 'pos/customers/read_customers/READ_CUSTOMERS'
export const READ_CUSTOMERS_FAIL = 'pos/customers/read_customers/READ_CUSTOMERS_FAIL'
export const READ_CUSTOMERS_SUCCESS = 'pos/customers/read_customers/READ_CUSTOMERS_SUCCESS'
