import reducer from './reducers'

import * as readCustomersOperations from './operations'


export { readCustomersOperations }

export default reducer
