// Core
import reduceReducers from 'reduce-reducers'

// Reducers
import readCompaniesReducer from './read_companies'


// State shape
const initialState = {
  companiesData: {
    // Data
    companies: {
      result: [],
      data: {},
    },

    // Error
    error: {
      message: '',
      data: {},
      code: null,
    },

    // Status
    readingCompanies: false,
    readCompaniesFailed: false,
    readCompaniesSucceeded: false,
  },
}

// Initial reducer
const initialReducer = (state = initialState) => (
  state
)

export default reduceReducers(
  initialReducer,
  readCompaniesReducer,
)
