import * as types from './types'


export const readCompanies = () => ({
  type: types.READ_COMPANIES,
})


export const readCompaniesFail = (data = {}, message = '', code = null) => ({
  type: types.READ_COMPANIES_FAIL,
  errors: {
    data,
    message,
    code,
  },
})


export const readCompaniesSuccess = companiesData => ({
  type: types.READ_COMPANIES_SUCCESS,
  companiesData,
})
