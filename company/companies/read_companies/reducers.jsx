// Core
import reduceReducers from 'reduce-reducers'

// Action types
import * as types from './types'


const readCompaniesReducer = (state, action) => {
  switch (action.type) {
    case types.READ_COMPANIES:
      return {
        ...state,

        companiesData: {
          ...state.companiesData,

          error: {
            message: '',
            data: {},
            code: null,
          },

          readingCompanies: true,
          readCompaniesFailed: false,
          readCompaniesSucceeded: false,
        },
      }
    case types.READ_COMPANIES_FAIL:
      return {
        ...state,

        companiesData: {
          ...state.companiesData,

          error: {
            message: action.errors.message,
            data: action.errors.data,
            code: action.errors.code,
          },

          readingCompanies: false,
          readCompaniesFailed: true,
          readCompaniesSucceeded: false,
        },
      }
    case types.READ_COMPANIES_SUCCESS:
      return {
        ...state,

        companiesData: {
          ...state.companiesData,

          companies: {
            result: action.companiesData.result,
            data: action.companiesData.data,
          },

          error: {
            message: '',
            data: {},
            code: null,
          },

          readingCompanies: false,
          readCompaniesFailed: false,
          readCompaniesSucceeded: true,
        },
      }
    default:
      return state
  }
}


export default reduceReducers(readCompaniesReducer)
