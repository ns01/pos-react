// Core
import api from 'posModules/app/axios'
import { Observable } from 'rxjs'
import { mergeMap } from 'rxjs/operators'
import { ofType } from 'redux-observable'
import normalize from 'posModules/app/helper'
import camelcaseKeys from 'camelcase-keys'

// Types
import { READ_COMPANIES } from '../types';

// Operations
import { readCompaniesSuccess, readCompaniesFail } from '../operations'


const readCompaniesEpic = action$ => action$.pipe(
  ofType(READ_COMPANIES),
  mergeMap(() => (
    Observable.create((observer) => {
      api.get('/v1/users/companies/').then((response) => {
        const companies = camelcaseKeys(response.data.data, { deep: true })
        const companiesData = normalize(companies, 'id')

        observer.next(readCompaniesSuccess(companiesData))
        observer.complete()
      }).catch((error) => {
        const errorResponse = camelcaseKeys(error.response.data.errors, { deep: true })

        const errors = {
          data: errorResponse.data || {},
          message: errorResponse.message,
          code: errorResponse.code,
        }

        observer.next(readCompaniesFail(errors.data, errors.message, errors.code))
        observer.complete()
      })
    })
  )),
)


export default readCompaniesEpic
