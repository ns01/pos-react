export const READ_COMPANIES = 'company/companies/read_companies/READ_COMPANIES'
export const READ_COMPANIES_FAIL = 'company/companies/read_companies/READ_COMPANIES_FAIL'
export const READ_COMPANIES_SUCCESS = 'company/companies/read_companies/READ_COMPANIES_SUCCESS'
