export const readReceipts = () => ({
    type: types.readReceipts,
})

export const readReceiptFail = (data = {}, message = '', code = null) => ({
    type: types.readReceiptFail,
    erorr: {
        data,
        message,
        code,
    }
})

export const readReceiptSuccess = ReceiptsData => ({
    type: types.readReceiptSuccess,
    ReceiptsData,
    })