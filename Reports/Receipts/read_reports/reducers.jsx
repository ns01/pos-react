// Core
import reduceReducers from 'reduce-reducers'

//Reducers
import readReceiptReducer from '.read_categories'

//state 
const initialState = {
    receiptData: {
        reciept: {
            result: [],
            data: {},
        },
        error: {
            massege: '',
            data:{},
            code: null,
        },
        readingreceipt: false,
        readReceipt: false,
        reacReceipt: false,

    }
}

const initialReducer = (state = initialState) = {
    state
}

export default reduceReducers(
    initialReducer,
    readReceiptReducer,
)
