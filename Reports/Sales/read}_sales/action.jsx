export const readSale = () => ({
    type: types.readSale,
})

export const readSaleFail = (data = {}, message = '', code = null ) => ({
    type: types.readSaleFail,
    error: {
        data,
        message,
        code,
    }
})
export const readSalesSuccess = SalesData => ({
    type: types.readSalesSuccess,
    SalesData,
    
})