// Core
import api from 'posModules/app/axios'
import { Observable } from 'rxjs'
import { mergeMap } from 'rxjs/operators'
import { ofType } from 'redux-observable'
import camelcaseKeys from 'camelcase-keys'

// Operations
import { readSalesSuccess, readSaleFail  } from '../operations'

// Types
import { FETCH_SALES} from '../types';


const readSalesEpic = action$ => action$.pipe(
    ofType(FETCH_SALES),
    mergeMap(() => (
      Observable.create((observer) => {
        api.get('/v1/items/sales/').then((response) => {
          const sales = camelcaseKeys(response.data.data, { deep: true })
  
            sales.unshift({name: 'no sales record', id: 0})
          
            const salesData = normalize (sales, 'id')
  
          observer.next(readSalesSuccess(salesData))
          observer.complete()
        }).catch((error) => {
          const errorResponse = camelcaseKeys(error.response.data.errors, { deep: true })
  
          const errors = {
            data: errorResponse.data || {},
            message: errorResponse.message,
            code: errorResponse.code,
          }
  
          observer.next(readCategoriesFail(errors.data, errors.message, errors.code))
          observer.complete()
        })
      })
    )),
  )
  
  export default readSalesEpic
  