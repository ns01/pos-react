export const READ_SALE = 'inventory/reports/read_sales/READ_SALES'
export const READ_SALE_FAIL = 'inventory/reports/read_sales/READ_SALES_FAIL'
export const READ_SALE_SUCCESS = 'inventory/reports/read_sales/READ_SALES_SUCCESS'