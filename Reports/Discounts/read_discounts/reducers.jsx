// Core
import reduceReducers from 'reduce-reducers'

// Reducers
import readDiscountReducer from './read_sale'



//state 
const initialState ={
    DiscountData: {
        Discount: {
            result: [],
            data: {},
        },
        error: {
            message: '',
            data: {},
            code: null,
        },

        readingDiscount: false,
        readDiscount : false,
        reacDiscount : false,
    }
}

const initialReducer = (state = initialState) => {
    state
}

export default reduceRedures(
    initialReducer,
    readSaleReducer,
)
   

   