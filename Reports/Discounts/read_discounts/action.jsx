export const readDiscount = () => ({
    type: types.readDiscount,
})

export const readDiscountFail = (data = {}, message ='', code = null) => ({
    type: types.readDiscountFail,
    error: {
        data,
        message,
        code,
    }
})
export const readDiscountSuccess = SalesData = ({
    type: types.readDiscountSuccess,
    DiscountData,
})