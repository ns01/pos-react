//Core
import reduceReducers from 'reduce-reducers'

import readTaxReducer from './read_taxes'


//state
const initialState = {
    TaxesDate: {
        taxes: {
            result: [],
            data: {},
        },
        error : {
            message: '',
            data: {},
            code: null,
        },
        readingTaxes: false,
        readTaxes: false,
        reacTaxes: false,
    }
}

const initialReducer = (state = initialState) => {state}

export default reduceReducers(
    initialReducer,
    readTaxesReducer,
)