export const readTax = () => ({
    type: types.readTaxes,
})

export const readTaxFail = (data = {}, message = '', code  = null ) => ({
    type: types.readTaxesFail,
    error: {
        data,
        message,
        code,
    }
})

export const readTaxSuccess = TaxesData => ({
    type:type.readTaxSuccess,
    TaxesData,
})

