export const READ_TAX = 'inventory/reports/read_taxes/READ_TAXES'
export const READ_TAX_FAIL = 'inventory/reports/read_taxes/READ_TAXES_FAIL'
export const READ_TAX_SUCCESS = 'inventory/reports/read_taxes/READ_TAXES_SUCCESS'