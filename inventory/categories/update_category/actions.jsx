import * as types from './types'


export const updateCategory = (categoryId, name) => ({
  type: types.UPDATE_CATEGORY,
  categoryId,
  name,
})

export const updateCategoryFail = (data = {}, message = '', code = null) => ({
  type: types.UPDATE_CATEGORY_FAIL,
  error: {
    data,
    message,
    code,
  },
})

export const updateCategorySuccess = category => ({
  type: types.UPDATE_CATEGORY_SUCCESS,
  category,
})

export const updateCategoryReset = () => ({
  type: types.UPDATE_CATEGORY_RESET,
})

export const setCategory = category => ({
  type: types.SET_CATEGORY,
  category,
})

export const changeFormName = name => ({
  type: types.CHANGE_FORM_NAME,
  name,
})

export const openUpdateCategoryDialog = () => ({
  type: types.OPEN_UPDATE_CATEGORY_DIALOG,
})

export const closeUpdateCategoryDialog = () => ({
  type: types.CLOSE_UPDATE_CATEGORY_DIALOG,
})
