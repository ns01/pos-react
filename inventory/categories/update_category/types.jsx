export const UPDATE_CATEGORY = 'inventory/categories/update_category/UPDATE_CATEGORY'
export const UPDATE_CATEGORY_FAIL = 'inventory/categories/update_category/UPDATE_CATEGORY_FAIL'
export const UPDATE_CATEGORY_SUCCESS = 'inventory/categories/update_category/UPDATE_CATEGORY_SUCCESS'
export const UPDATE_CATEGORY_RESET = 'inventory/categories/update_category/UPDATE_CATEGORY_RESET'

// Input
export const CHANGE_FORM_NAME = 'inventory/categories/update_category/CHANGE_FORM_NAME'
export const SET_CATEGORY = 'inventory/categories/update_category/SET_CATEGORY'

// Dialog
export const OPEN_UPDATE_CATEGORY_DIALOG = 'inventory/categories/update_category/OPEN_UPDATE_CATEGORY_DIALOG'
export const CLOSE_UPDATE_CATEGORY_DIALOG = 'inventory/categories/update_category/CLOSE_UPDATE_CATEGORY_DIALOG'
