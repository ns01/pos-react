// Core
import api from 'posModules/app/axios'
import { Observable } from 'rxjs'
import { mergeMap, debounceTime } from 'rxjs/operators'
import { ofType } from 'redux-observable'
import camelcaseKeys from 'camelcase-keys'

// Operations
import { updateCategorySuccess, updateCategoryFail } from '../operations'

// Types
import { UPDATE_CATEGORY } from '../types';


const updateCategoryEpic = action$ => action$.pipe(
  ofType(UPDATE_CATEGORY),
  debounceTime(1000),
  mergeMap(action => (
    Observable.create((observer) => {
      const formErrors = {}

      if (action.name.trim() === '') {
        formErrors.name = 'name is required'
      }

      if (Object.getOwnPropertyNames(formErrors).length > 0) {
        observer.next(updateCategoryFail(formErrors))
        observer.complete()
        return
      }

      api.put(`/v1/items/categories/${action.categoryId}`, { name: action.name }).then((response) => {
        const category = camelcaseKeys(response.data.data, { deep: true })

        observer.next(updateCategorySuccess(category))
        observer.complete()
      }).catch((error) => {
        const errorResponse = camelcaseKeys(error.response.data.errors, { deep: true })

        const errors = {
          data: errorResponse.data || {},
          message: errorResponse.message,
          code: errorResponse.code,
        }

        if (errorResponse.code === 422) {
          errors.data = errorResponse.data.category_form
        }


        observer.next(updateCategoryFail(errors.data, errors.message, errors.code))
        observer.complete()
      })
    })
  )),
)

export default updateCategoryEpic
