// Core
import reduceReducers from 'reduce-reducers'

// Action types
import * as types from './types'


const updateCategoryReducer = (state, action) => {
  switch (action.type) {
    case types.UPDATE_CATEGORY:
      return {
        ...state,

        updateCategoryForm: {
          ...state.updateCategoryForm,

          error: {
            message: '',
            data: {},
            code: null,
          },

          updatingCategory: true,
          updateCategoryFailed: false,
          updateCategorySucceeded: false,
        },
      }
    case types.UPDATE_CATEGORY_FAIL: {
      return {
        ...state,

        updateCategoryForm: {
          ...state.updateCategoryForm,

          error: {
            message: action.error.message,
            data: action.error.data,
            code: action.error.code,
          },

          updatingCategory: false,
          updateCategoryFailed: true,
          updateCategorySucceeded: false,
        },
      }
    }
    case types.UPDATE_CATEGORY_SUCCESS: {
      return {
        ...state,

        categoriesData: {
          ...state.categoriesData,

          categories: {
            ...state.categoriesData.categories,

            data: {
              ...state.categoriesData.categories.data,

              [action.category.id]: action.category,
            },
          },
        },

        updateCategoryForm: {
          ...state.updateCategoryForm,

          error: {
            message: '',
            data: {},
            code: null,
          },

          updatingCategory: false,
          updateCategoryFailed: false,
          updateCategorySucceeded: true,
        },
      }
    }
    case types.UPDATE_CATEGORY_RESET:
      return {
        ...state,

        updateCategoryForm: {
          isUpdateCategoryDialogOpened: false,

          category: {},
          name: '',

          error: {
            message: '',
            data: {},
            code: null,
          },

          updatingCategory: false,
          updateCategoryFailed: false,
          updateCategorySucceeded: false,
        },
      }
    case types.SET_CATEGORY:
      return {
        ...state,

        updateCategoryForm: {
          ...state.updateCategoryForm,

          category: action.category,
        },
      }
    case types.CHANGE_FORM_NAME:
      return {
        ...state,

        updateCategoryForm: {
          ...state.updateCategoryForm,

          name: action.name,
        },
      }
    case types.OPEN_UPDATE_CATEGORY_DIALOG:
      return {
        ...state,

        updateCategoryForm: {
          ...state.updateCategoryForm,

          isUpdateCategoryDialogOpened: true,
        },
      }
    case types.CLOSE_UPDATE_CATEGORY_DIALOG:
      return {
        ...state,

        updateCategoryForm: {
          ...state.updateCategoryForm,

          isUpdateCategoryDialogOpened: false,
        },
      }
    default:
      return state
  }
}


export default reduceReducers(updateCategoryReducer)
