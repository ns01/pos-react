import reducer from './reducers'

import * as updateCategoryOperations from './operations'


export { updateCategoryOperations }

export default reducer
