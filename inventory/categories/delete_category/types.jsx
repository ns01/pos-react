export const DELETE_CATEGORY = 'inventory/categories/delete_category/DELETE_CATEGORY'
export const DELETE_CATEGORY_FAIL = 'inventory/categories/delete_category/DELETE_CATEGORY_FAIL'
export const DELETE_CATEGORY_SUCCESS = 'inventory/categories/delete_category/DELETE_CATEGORY_SUCCESS'
export const DELETE_CATEGORY_RESET = 'inventory/categories/delete_category/DELETE_CATEGORY_RESET'

export const SET_CATEGORY = 'inventory/categories/delete_category/SET_CATEGORY'

export const OPEN_DELETE_CATEGORY_DIALOG = 'inventory/categories/delete_category/OPEN_DELETE_CATEGORY_DIALOG'
export const CLOSE_DELETE_CATEGORY_DIALOG = 'inventory/categories/delete_category/CLOSE_DELETE_CATEGORY_DIALOG'
