import reducer from './reducers'

import * as deleteCategoryOperations from './operations'


export { deleteCategoryOperations }

export default reducer
