// Core
import api from 'posModules/app/axios'
import { Observable } from 'rxjs'
import { mergeMap, debounceTime } from 'rxjs/operators'
import { ofType } from 'redux-observable'
import camelcaseKeys from 'camelcase-keys'

// Operations
import { deleteCategorySuccess, deleteCategoryFail } from '../operations'

// Types
import { DELETE_CATEGORY } from '../types';


const deleteCategoryEpic = action$ => action$.pipe(
  ofType(DELETE_CATEGORY),
  debounceTime(1000),
  mergeMap(action => (
    Observable.create((observer) => {
      api.delete(`/v1/items/categories/${action.categoryId}`).then(() => {
        observer.next(deleteCategorySuccess(action.categoryId))
        observer.complete()
      }).catch((error) => {
        const errorResponse = camelcaseKeys(error.response.data.errors, { deep: true })

        const errors = {
          data: errorResponse.data || {},
          message: errorResponse.message,
          code: errorResponse.code,
        }

        observer.next(deleteCategoryFail(errors.data, errors.message, errors.code))
        observer.complete()
      })
    })
  )),
)

export default deleteCategoryEpic
