// Core
import reduceReducers from 'reduce-reducers'

// Action types
import * as types from './types'


const deleteCategoryReducer = (state, action) => {
  switch (action.type) {
    case types.DELETE_CATEGORY:
      return {
        ...state,

        deleteCategoryForm: {
          ...state.deleteCategoryForm,

          error: {
            message: '',
            data: {},
            code: null,
          },

          deletingCategory: true,
          deleteCategoryFailed: false,
          deleteCategorySucceeded: false,
        },
      }
    case types.DELETE_CATEGORY_FAIL: {
      return {
        ...state,

        deleteCategoryForm: {
          ...state.deleteCategoryForm,

          error: {
            message: action.error.message,
            data: action.error.data,
            code: action.error.code,
          },

          deletingCategory: false,
          deleteCategoryFailed: true,
          deleteCategorySucceeded: false,
        },
      }
    }
    case types.DELETE_CATEGORY_SUCCESS: {
      const { data, result: categoryResult } = state.categoriesData.categories
      delete data[action.categoryId]

      const result = categoryResult.filter(categoryId => categoryId !== action.categoryId)

      return {
        ...state,

        categoriesData: {
          ...state.categoriesData,

          categories: {
            data,
            result,
          },
        },

        deleteCategoryForm: {
          ...state.deleteCategoryForm,

          error: {
            message: '',
            data: {},
            code: null,
          },

          deletingCategory: false,
          deleteCategoryFailed: false,
          deleteCategorySucceeded: true,
        },
      }
    }
    case types.DELETE_CATEGORY_RESET:
      return {
        ...state,

        deleteCategoryForm: {
          isRemoveCategoryDialogOpened: false,
          category: {},

          error: {
            message: '',
            data: {},
            code: null,
          },

          deletingCategory: false,
          deleteCategoryFailed: false,
          deleteCategorySucceeded: false,
        },
      }
    case types.SET_CATEGORY:
      return {
        ...state,

        deleteCategoryForm: {
          ...state.deleteCategoryForm,

          category: action.category,
        },
      }
    case types.OPEN_DELETE_CATEGORY_DIALOG:
      return {
        ...state,

        deleteCategoryForm: {
          ...state.deleteCategoryForm,

          isRemoveCategoryDialogOpened: true,
        },
      }
    case types.CLOSE_DELETE_CATEGORY_DIALOG:
      return {
        ...state,

        deleteCategoryForm: {
          ...state.deleteCategoryForm,

          isRemoveCategoryDialogOpened: false,
        },
      }
    default:
      return state
  }
}


export default reduceReducers(deleteCategoryReducer)
