import * as types from './types'


export const deleteCategory = categoryId => ({
  type: types.DELETE_CATEGORY,
  categoryId,
})

export const deleteCategoryFail = (data = {}, message = '', code = null) => ({
  type: types.DELETE_CATEGORY_FAIL,
  error: {
    data,
    message,
    code,
  },
})

export const deleteCategorySuccess = categoryId => ({
  type: types.DELETE_CATEGORY_SUCCESS,
  categoryId,
})

export const deleteCategoryReset = () => ({
  type: types.DELETE_CATEGORY_RESET,
})

export const setCategory = category => ({
  type: types.SET_CATEGORY,
  category,
})

export const openDeleteCategoryDialog = () => ({
  type: types.OPEN_DELETE_CATEGORY_DIALOG,
})

export const closeDeleteCategoryDialog = () => ({
  type: types.CLOSE_DELETE_CATEGORY_DIALOG,
})
