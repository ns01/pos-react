// Core
import reduceReducers from 'reduce-reducers'

// Reducers
import createCategoryReducer from './create_category'
import readCategoriesReducer from './read_categories'
import updateCategoryReducer from './update_category'
import deleteCategoryReducer from './delete_category'

// State shape
const initialState = {
  categoriesData: {
    // Data
    categories: {
      result: [],
      data: {},
    },

    // Error
    error: {
      message: '',
      data: {},
      code: null,
    },

    // Status
    readingCategories: false,
    readCategoriesFailed: false,
    readCategoriesSucceeded: false,
  },

  createCategoryForm: {
    isAddCategoryDialogOpened: false,

    // Input
    name: '',

    error: {
      message: '',
      data: {},
      code: null,
    },

    creatingCategory: false,
    createCategoryFailed: false,
    createCategorySucceeded: false,
  },

  updateCategoryForm: {
    isUpdateCategoryDialogOpened: false,

    category: {},
    name: '',

    error: {
      message: '',
      data: {},
      code: null,
    },

    updatingCategory: false,
    updateCategoryFailed: false,
    updateCategorySucceeded: false,
  },

  deleteCategoryForm: {
    isRemoveCategoryDialogOpened: false,
    category: {},

    error: {
      message: '',
      data: {},
      code: null,
    },

    deletingCategory: false,
    deleteCategoryFailed: false,
    deleteCategorySucceeded: false,
  },
}

// Initial reducer
const initialReducer = (state = initialState) => (
  state
)

export default reduceReducers(
  initialReducer,
  createCategoryReducer,
  readCategoriesReducer,
  updateCategoryReducer,
  deleteCategoryReducer,
)
