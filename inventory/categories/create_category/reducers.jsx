// Core
import reduceReducers from 'reduce-reducers'

// Action types
import * as types from './types'


const createCategoryReducer = (state, action) => {
  switch (action.type) {
    case types.CREATE_CATEGORY:
      return {
        ...state,

        createCategoryForm: {
          ...state.createCategoryForm,

          error: {
            message: '',
            data: {},
            code: null,
          },

          creatingCategory: true,
          createCategoryFailed: false,
          createCategorySucceeded: false,
        },
      }
    case types.CREATE_CATEGORY_FAIL:
      return {
        ...state,

        createCategoryForm: {
          ...state.createCategoryForm,

          error: {
            message: action.error.message,
            data: action.error.data,
            code: action.error.code,
          },

          creatingCategory: false,
          createCategoryFailed: true,
          createCategorySucceeded: false,
        },
      }
    case types.CREATE_CATEGORY_SUCCESS: {
      const { result } = state.categoriesData.categories

      result.unshift(action.category.id)

      return {
        ...state,

        categoriesData: {
          ...state.categoriesData,

          categories: {
            data: {
              ...state.categoriesData.categories.data,

              [action.category.id]: action.category,
            },

            result,
          },
        },


        createCategoryForm: {
          ...state.createCategoryForm,

          error: {
            message: '',
            data: {},
            code: null,
          },

          creatingCategory: false,
          createCategoryFailed: false,
          createCategorySucceeded: true,
        },
      }
    }
    case types.CREATE_CATEGORY_RESET:
      return {
        ...state,

        createCategoryForm: {
          name: '',

          error: {
            message: '',
            data: {},
            code: null,
          },

          creatingCategory: false,
          createCategoryFailed: false,
          createCategorySucceeded: false,
        },
      }
    case types.CHANGE_FORM_NAME:
      return {
        ...state,

        createCategoryForm: {
          ...state.createCategoryForm,

          name: action.name,
        },
      }
    case types.OPEN_CREATE_CATEGORY_DIALOG:
      return {
        ...state,

        createCategoryForm: {
          ...state.createCategoryForm,

          isAddCategoryDialogOpened: true,
        },
      }
    case types.CLOSE_CREATE_CATEGORY_DIALOG:
      return {
        ...state,

        createCategoryForm: {
          ...state.createCategoryForm,

          isAddCategoryDialogOpened: false,
        },
      }
    default:
      return state
  }
}


export default reduceReducers(createCategoryReducer)
