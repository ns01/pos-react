import * as types from './types'


export const createCategory = name => ({
  type: types.CREATE_CATEGORY,
  name,
})

export const createCategoryFail = (data = {}, message = '', code = null) => ({
  type: types.CREATE_CATEGORY_FAIL,
  error: {
    data,
    message,
    code,
  },
})

export const createCategorySuccess = category => ({
  type: types.CREATE_CATEGORY_SUCCESS,
  category,
})

export const createCategoryReset = () => ({
  type: types.CREATE_CATEGORY_RESET,
})

export const changeFormName = name => ({
  type: types.CHANGE_FORM_NAME,
  name,
})

export const openAddCategoryDialog = () => ({
  type: types.OPEN_CREATE_CATEGORY_DIALOG,
})

export const closeAddCategoryDialog = () => ({
  type: types.CLOSE_CREATE_CATEGORY_DIALOG,
})
