export const CREATE_CATEGORY = 'inventory/categories/create_category/CREATE_CATEGORY'
export const CREATE_CATEGORY_FAIL = 'inventory/categories/create_category/CREATE_CATEGORY_FAIL'
export const CREATE_CATEGORY_SUCCESS = 'inventory/categories/create_category/CREATE_CATEGORY_SUCCESS'
export const CREATE_CATEGORY_RESET = 'inventory/categories/create_category/CREATE_CATEGORY_RESET'

// Inputs
export const CHANGE_FORM_NAME = 'inventory/categories/create_category/CHANGE_FORM_NAME'

export const OPEN_CREATE_CATEGORY_DIALOG = 'inventory/categories/create_item/OPEN_CREATE_CATEGORY_DIALOG'
export const CLOSE_CREATE_CATEGORY_DIALOG = 'inventory/categories/create_item/CLOSE_CREATE_CATEGORY_DIALOG'
