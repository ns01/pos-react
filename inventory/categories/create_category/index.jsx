import reducer from './reducers'

import * as createCategoryOperations from './operations'


export { createCategoryOperations }

export default reducer
