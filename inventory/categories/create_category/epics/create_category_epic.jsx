// Core
import api from 'posModules/app/axios'
import { Observable } from 'rxjs'
import { mergeMap, debounceTime } from 'rxjs/operators'
import { ofType } from 'redux-observable'
import camelcaseKeys from 'camelcase-keys'

// Operations
import { createCategorySuccess, createCategoryFail } from '../operations'

// Types
import { CREATE_CATEGORY } from '../types';


const createCategoryEpic = action$ => action$.pipe(
  ofType(CREATE_CATEGORY),
  debounceTime(1000),
  mergeMap(action => (
    Observable.create((observer) => {
      const formErrors = {}

      if (action.name.trim() === '') {
        formErrors.name = 'name is required'
      }

      if (Object.getOwnPropertyNames(formErrors).length > 0) {
        observer.next(createCategoryFail(formErrors))
        observer.complete()
        return
      }

      api.post('/v1/items/categories', {
        name: action.name,
      }).then((response) => {
        const category = camelcaseKeys(response.data.data, { deep: true })

        observer.next(createCategorySuccess(category))
        observer.complete()
      }).catch((error) => {
        const errorResponse = camelcaseKeys(error.response.data.errors, { deep: true })

        const errors = {
          data: errorResponse.data || {},
          message: errorResponse.message,
          code: errorResponse.code,
        }

        observer.next(createCategoryFail(errors.data, errors.message, errors.code))
        observer.complete()
      })
    })
  )),
)

export default createCategoryEpic
