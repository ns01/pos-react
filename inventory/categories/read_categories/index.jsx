import reducer from './reducers'

import * as readCategoriesOperations from './operations'


export { readCategoriesOperations }

export default reducer
