import * as types from './types'


export const readCategories = () => ({
  type: types.READ_CATEGORIES,
})


export const readCategoriesFail = (data = {}, message = '', code = null) => ({
  type: types.READ_CATEGORIES_FAIL,
  errors: {
    data,
    message,
    code,
  },
})


export const readCategoriesSuccess = categoriesData => ({
  type: types.READ_CATEGORIES_SUCCESS,
  categoriesData,
})
