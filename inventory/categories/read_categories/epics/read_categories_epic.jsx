// Core
import api from 'posModules/app/axios'
import { Observable } from 'rxjs'
import { mergeMap } from 'rxjs/operators'
import { ofType } from 'redux-observable'
import normalize from 'posModules/app/helper'
import camelcaseKeys from 'camelcase-keys'

// Types
import { READ_CATEGORIES } from '../types';

// Operations
import { readCategoriesSuccess, readCategoriesFail } from '../operations'


const readCategoriesEpic = action$ => action$.pipe(
  ofType(READ_CATEGORIES),
  mergeMap(() => (
    Observable.create((observer) => {
      api.get('/v1/items/categories/').then((response) => {
        const categories = camelcaseKeys(response.data.data, { deep: true })

        const categoriesData = normalize(categories, 'id')


        observer.next(readCategoriesSuccess(categoriesData))
        observer.complete()
      }).catch((error) => {
        const errorResponse = camelcaseKeys(error.response.data.errors, { deep: true })

        const errors = {
          data: errorResponse.data || {},
          message: errorResponse.message,
          code: errorResponse.code,
        }

        observer.next(readCategoriesFail(errors.data, errors.message, errors.code))
        observer.complete()
      })
    })
  )),
)


export default readCategoriesEpic
