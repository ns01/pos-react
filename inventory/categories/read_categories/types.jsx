export const READ_CATEGORIES = 'inventory/categories/read_categories/READ_CATEGORIES'
export const READ_CATEGORIES_FAIL = 'inventory/categories/read_categories/READ_CATEGORIES_FAIL'
export const READ_CATEGORIES_SUCCESS = 'inventory/categories/read_categories/READ_CATEGORIES_SUCCESS'
