// Core
import reduceReducers from 'reduce-reducers'

// Action types
import * as types from './types'


const readCategoriesReducer = (state, action) => {
  switch (action.type) {
    case types.READ_CATEGORIES:
      return {
        ...state,

        categoriesData: {
          ...state.categoriesData,

          error: {
            message: '',
            data: {},
            code: null,
          },

          readingCategories: true,
          readCategoriesFailed: false,
          readCategoriesSucceeded: false,
        },
      }
    case types.READ_CATEGORIES_FAIL:
      return {
        ...state,

        categoriesData: {
          ...state.categoriesData,

          error: {
            message: action.errors.message,
            data: action.errors.data,
            code: action.errors.code,
          },

          readingCategories: false,
          readCategoriesFailed: true,
          readCategoriesSucceeded: false,
        },
      }
    case types.READ_CATEGORIES_SUCCESS:
      return {
        ...state,

        categoriesData: {
          ...state.categoriesData,

          categories: {
            result: action.categoriesData.result,
            data: action.categoriesData.data,
          },

          error: {
            message: '',
            data: {},
            code: null,
          },

          readingCategories: false,
          readCategoriesFailed: false,
          readCategoriesSucceeded: true,
        },
      }
    default:
      return state
  }
}


export default reduceReducers(readCategoriesReducer)
