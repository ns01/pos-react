// Core
import reduceReducers from 'reduce-reducers'

// Action types
import * as types from './types'


const createItemReducer = (state, action) => {
  switch (action.type) {
    case types.CREATE_ITEM:
      return {
        ...state,

        createItemForm: {
          ...state.createItemForm,

          error: {
            message: '',
            data: {},
            code: null,
          },

          creatingItem: true,
          createItemFailed: false,
          createItemSucceeded: false,
        },
      }
    case types.CREATE_ITEM_FAIL:
      return {
        ...state,

        createItemForm: {
          ...state.createItemForm,

          error: {
            message: action.error.message,
            data: action.error.data,
            code: action.error.code,
          },

          creatingItem: false,
          createItemFailed: true,
          createItemSucceeded: false,
        },
      }
    case types.CREATE_ITEM_SUCCESS: {
      const { result } = state.itemsData.items

      result.unshift(action.item.id)

      return {
        ...state,

        itemsData: {
          ...state.itemsData,

          items: {
            data: {
              ...state.itemsData.items.data,

              [action.item.id]: action.item,
            },

            result,
          },
        },

        createItemForm: {
          ...state.createItemForm,

          error: {
            message: '',
            data: {},
            code: null,
          },

          creatingItem: false,
          createItemFailed: false,
          createItemSucceeded: true,
        },
      }
    }
    case types.CREATE_ITEM_RESET:
      return {
        ...state,

        createItemForm: {
          isAddItemDialogOpened: false,

          name: '',
          forSaleChecked: false,
          quantityInStock: '',
          unit: '',
          price: '',
          categoryId: '',
          cost: '',
          sku: '',
          barCode: '',
          description: '',

          error: {
            message: '',
            data: {},
            code: null,
          },

          creatingItem: false,
          createItemFailed: false,
          createItemSucceeded: false,
        },
      }
    case types.CHANGE_FORM_NAME:
      return {
        ...state,

        createItemForm: {
          ...state.createItemForm,

          name: action.name,
        },
      }
    case types.CHANGE_FORM_FOR_SALE:
      return {
        ...state,

        createItemForm: {
          ...state.createItemForm,

          forSaleChecked: action.forSaleChecked,
        },
      }
    case types.CHANGE_FORM_UNIT:
      return {
        ...state,

        createItemForm: {
          ...state.createItemForm,

          unit: action.unit,
        },
      }
    case types.CHANGE_FORM_QUANTITY_IN_STOCK:
      return {
        ...state,

        createItemForm: {
          ...state.createItemForm,

          quantityInStock: action.quantityInStock,
        },
      }
    case types.CHANGE_FORM_PRICE:
      return {
        ...state,

        createItemForm: {
          ...state.createItemForm,

          price: action.price,
        },
      }
    case types.CHANGE_FORM_CATEGORY:
      return {
        ...state,

        createItemForm: {
          ...state.createItemForm,

          categoryId: action.categoryId,
        },
      }
    case types.CHANGE_FORM_COST:
      return {
        ...state,

        createItemForm: {
          ...state.createItemForm,

          cost: action.cost,
        },
      }
    case types.CHANGE_FORM_SKU:
      return {
        ...state,

        createItemForm: {
          ...state.createItemForm,

          sku: action.sku,
        },
      }
    case types.CHANGE_FORM_BAR_CODE:
      return {
        ...state,

        createItemForm: {
          ...state.createItemForm,

          barCode: action.barCode,
        },
      }
    case types.CHANGE_FORM_DESCRIPTION:
      return {
        ...state,

        createItemForm: {
          ...state.createItemForm,

          description: action.description,
        },
      }
    case types.OPEN_CREATE_ITEM_DIALOG:
      return {
        ...state,

        createItemForm: {
          ...state.createItemForm,

          isAddItemDialogOpened: true,
        },
      }
    case types.CLOSE_CREATE_ITEM_DIALOG:
      return {
        ...state,

        createItemForm: {
          ...state.createItemForm,

          isAddItemDialogOpened: false,
        },
      }
    default:
      return state
  }
}


export default reduceReducers(createItemReducer)
