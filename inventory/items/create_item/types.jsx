export const CREATE_ITEM = 'inventory/items/create_item/CREATE_ITEM'
export const CREATE_ITEM_FAIL = 'inventory/items/create_item/CREATE_ITEM_FAIL'
export const CREATE_ITEM_SUCCESS = 'inventory/items/create_item/CREATE_ITEM_SUCCESS'
export const CREATE_ITEM_RESET = 'inventory/items/create_item/CREATE_ITEM_RESET'

// Inputs
export const CHANGE_FORM_NAME = 'inventory/items/create_item/CHANGE_FORM_NAME'

// Optional
export const CHANGE_FORM_FOR_SALE = 'inventory/items/create_item/CHANGE_FORM_FOR_SALE'
export const CHANGE_FORM_QUANTITY_IN_STOCK = 'inventory/items/create_item/CHANGE_FORM_QUANTITY_IN_STOCK'
export const CHANGE_FORM_UNIT = 'inventory/items/create_item/CHANGE_FORM_UNIT'
export const CHANGE_FORM_PRICE = 'inventory/items/create_item/CHANGE_FORM_PRICE'
export const CHANGE_FORM_CATEGORY = 'inventory/items/create_item/CHANGE_FORM_CATEGORY'
export const CHANGE_FORM_COST = 'inventory/items/create_item/CHANGE_FORM_COST'
export const CHANGE_FORM_SKU = 'inventory/items/create_item/CHANGE_FORM_SKU'
export const CHANGE_FORM_BAR_CODE = 'inventory/items/create_item/CHANGE_FORM_BAR_CODE'
export const CHANGE_FORM_DESCRIPTION = 'inventory/items/create_item/CHANGE_FORM_DESCRIPTION'

export const OPEN_CREATE_ITEM_DIALOG = 'inventory/items/create_item/OPEN_CREATE_ITEM_DIALOG'
export const CLOSE_CREATE_ITEM_DIALOG = 'inventory/items/create_item/CLOSE_CREATE_ITEM_DIALOG'
