// Core
import api from 'posModules/app/axios'
import { Observable } from 'rxjs'
import { mergeMap, debounceTime } from 'rxjs/operators'
import { ofType } from 'redux-observable'
import camelcaseKeys from 'camelcase-keys'

// Operations
import { setPOSRepresentationOperations } from 'posModules/inventory/items/set_pos_representation'
import { createItemSuccess, createItemFail } from '../operations'

// Types
import { CREATE_ITEM } from '../types'


const createItemEpic = (action$, state$) => action$.pipe(
  ofType(CREATE_ITEM),
  debounceTime(1000),
  mergeMap(action => (
    Observable.create((observer) => {
      const formErrors = {}

      const { categories } = state$.value.categoriesData

      if (action.name.trim() === '') {
        formErrors.name = 'name is required'
      }

      if (Object.getOwnPropertyNames(formErrors).length > 0) {
        observer.next(createItemFail(formErrors))
        observer.complete()
        return
      }

      console.log(action.forSaleChecked)

      api.post('/v1/items/', {
        name: action.name,
        category: categories.data[action.categoryId].id || '',
        is_available: action.forSaleChecked,
        unit: action.unit,
        quantity_in_stock: action.quantityInStock || 0,
        price: action.price || 0,
        cost: action.cost || 0,
        sku: action.sku,
        bar_code: action.barCode,
        description: action.description,
      }).then((response) => {
        const item = camelcaseKeys(response.data.data, { deep: true })

        observer.next(createItemSuccess(item))
        observer.next(setPOSRepresentationOperations.setItem(item))
        observer.complete()
      }).catch((error) => {
        const errorResponse = camelcaseKeys(error.response.data.errors, { deep: true })

        const errors = {
          data: errorResponse.data || {},
          message: errorResponse.message,
          code: errorResponse.code,
        }

        observer.next(createItemFail(errors.data, errors.message, errors.code))
        observer.complete()
      })
    })
  )),
)

export default createItemEpic
