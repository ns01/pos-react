import * as types from './types'


export const createItem = (
  name,
  categoryId,
  sku,
  barCode,
  forSaleChecked,
  quantityInStock,
  unit,
  price,
  cost,
  description,
) => ({
  type: types.CREATE_ITEM,
  name,
  categoryId,
  sku,
  barCode,
  forSaleChecked,
  quantityInStock,
  unit,
  price,
  cost,
  description,
})


export const createItemFail = (data = {}, message = '', code = null) => ({
  type: types.CREATE_ITEM_FAIL,
  error: {
    data,
    message,
    code,
  },
})

export const createItemSuccess = item => ({
  type: types.CREATE_ITEM_SUCCESS,
  item,
})

export const createItemReset = () => ({
  type: types.CREATE_ITEM_RESET,
})

export const changeFormName = name => ({
  type: types.CHANGE_FORM_NAME,
  name,
})

export const changeFormForSale = forSaleChecked => ({
  type: types.CHANGE_FORM_FOR_SALE,
  forSaleChecked,
})

export const changeFormQuantityInStock = quantityInStock => ({
  type: types.CHANGE_FORM_QUANTITY_IN_STOCK,
  quantityInStock,
})

export const changeFormUnit = unit => ({
  type: types.CHANGE_FORM_UNIT,
  unit,
})

export const changeFormPrice = price => ({
  type: types.CHANGE_FORM_PRICE,
  price,
})

export const changeFormCategory = categoryId => ({
  type: types.CHANGE_FORM_CATEGORY,
  categoryId,
})

export const changeFormCost = cost => ({
  type: types.CHANGE_FORM_COST,
  cost,
})

export const changeFormSKU = sku => ({
  type: types.CHANGE_FORM_SKU,
  sku,
})

export const changeFormBarCode = barCode => ({
  type: types.CHANGE_FORM_BAR_CODE,
  barCode,
})

export const changeFormDescription = description => ({
  type: types.CHANGE_FORM_DESCRIPTION,
  description,
})

export const openAddItemDialog = () => ({
  type: types.OPEN_CREATE_ITEM_DIALOG,
})

export const closeAddItemDialog = () => ({
  type: types.CLOSE_CREATE_ITEM_DIALOG,
})
