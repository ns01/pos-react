import reducer from './reducers'

import * as deleteItemOperations from './operations'


export { deleteItemOperations }

export default reducer
