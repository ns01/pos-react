export const DELETE_ITEM = 'inventory/items/delete_item/DELETE_ITEM'
export const DELETE_ITEM_FAIL = 'inventory/items/delete_item/DELETE_ITEM_FAIL'
export const DELETE_ITEM_SUCCESS = 'inventory/items/delete_item/DELETE_ITEM_SUCCESS'
export const DELETE_ITEM_RESET = 'inventory/items/delete_item/DELETE_ITEM_RESET'

export const SET_ITEM = 'inventory/items/delete_item/SET_ITEM'

export const OPEN_DELETE_ITEM_DIALOG = 'inventory/items/delete_item/OPEN_DELETE_ITEM_DIALOG'
export const CLOSE_DELETE_ITEM_DIALOG = 'inventory/items/delete_item/CLOSE_DELETE_ITEM_DIALOG'
