import * as types from './types'


export const deleteItem = itemId => ({
  type: types.DELETE_ITEM,
  itemId,
})

export const deleteItemFail = (data = {}, message = '', code = null) => ({
  type: types.DELETE_ITEM_FAIL,
  error: {
    data,
    message,
    code,
  },
})

export const deleteItemSuccess = itemId => ({
  type: types.DELETE_ITEM_SUCCESS,
  itemId,
})

export const deleteItemReset = () => ({
  type: types.DELETE_ITEM_RESET,
})

export const setItem = item => ({
  type: types.SET_ITEM,
  item,
})

export const openDeleteItemDialog = () => ({
  type: types.OPEN_DELETE_ITEM_DIALOG,
})

export const closeDeleteItemDialog = () => ({
  type: types.CLOSE_DELETE_ITEM_DIALOG,
})
