// Core
import reduceReducers from 'reduce-reducers'

// Action types
import * as types from './types'


const deleteItemReducer = (state, action) => {
  switch (action.type) {
    case types.DELETE_ITEM:
      return {
        ...state,

        deleteItemForm: {
          ...state.deleteItemForm,

          error: {
            message: '',
            data: {},
            code: null,
          },

          deletingItem: true,
          deleteItemFailed: false,
          deleteItemSucceeded: false,
        },
      }
    case types.DELETE_ITEM_FAIL: {
      return {
        ...state,

        deleteItemForm: {
          ...state.deleteItemForm,

          error: {
            message: action.error.message,
            data: action.error.data,
            code: action.error.code,
          },

          deletingItem: false,
          deleteItemFailed: true,
          deleteItemSucceeded: false,
        },
      }
    }
    case types.DELETE_ITEM_SUCCESS: {
      const { data, result: itemResult } = state.itemsData.items
      delete data[action.itemId]

      const result = itemResult.filter(itemId => itemId !== action.itemId)

      return {
        ...state,

        itemsData: {
          ...state.itemsData,

          items: {
            data,
            result,
          },
        },

        deleteItemForm: {
          ...state.deleteItemForm,

          error: {
            message: '',
            data: {},
            code: null,
          },

          deletingItem: false,
          deleteItemFailed: false,
          deleteItemSucceeded: true,
        },
      }
    }
    case types.DELETE_ITEM_RESET:
      return {
        ...state,

        deleteItemForm: {
          isRemoveItemDialogOpened: false,
          item: {},

          error: {
            message: '',
            data: {},
            code: null,
          },

          deletingItem: false,
          deleteItemFailed: false,
          deleteItemSucceeded: false,
        },
      }
    case types.SET_ITEM:
      return {
        ...state,

        deleteItemForm: {
          ...state.deleteItemForm,

          item: action.item,
        },
      }
    case types.OPEN_DELETE_ITEM_DIALOG:
      return {
        ...state,

        deleteItemForm: {
          ...state.deleteItemForm,

          isRemoveItemDialogOpened: true,
        },
      }
    case types.CLOSE_DELETE_ITEM_DIALOG:
      return {
        ...state,

        deleteItemForm: {
          ...state.deleteItemForm,

          isRemoveItemDialogOpened: false,
        },
      }
    default:
      return state
  }
}


export default reduceReducers(deleteItemReducer)
