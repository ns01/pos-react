// Core
import api from 'posModules/app/axios'
import { Observable } from 'rxjs'
import { mergeMap, debounceTime } from 'rxjs/operators'
import { ofType } from 'redux-observable'
import camelcaseKeys from 'camelcase-keys'

// Operations
import { deleteItemSuccess, deleteItemFail } from '../operations'

// Types
import { DELETE_ITEM } from '../types';


const deleteItemEpic = action$ => action$.pipe(
  ofType(DELETE_ITEM),
  debounceTime(1000),
  mergeMap(action => (
    Observable.create((observer) => {
      api.delete(`/v1/items/${action.itemId}`).then(() => {
        observer.next(deleteItemSuccess(action.itemId))
        observer.complete()
      }).catch((error) => {
        const errorResponse = camelcaseKeys(error.response.data.errors, { deep: true })

        const errors = {
          data: errorResponse.data || {},
          message: errorResponse.message,
          code: errorResponse.code,
        }

        observer.next(deleteItemFail(errors.data, errors.message, errors.code))
        observer.complete()
      })
    })
  )),
)

export default deleteItemEpic
