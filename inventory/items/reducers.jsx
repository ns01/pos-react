// Core
import reduceReducers from 'reduce-reducers'

// Reducers
import createItemReducer from './create_item'
import readItemsReducer from './read_items'
import updateItemReducer from './update_item'
import deleteItemReducer from './delete_item'
import posRepresentationReducer from './set_pos_representation'
import updatePOSRepresentationSymbolColorReducer from './set_pos_representation/update_symbol_color'
import createPOSRepresentationImageReducer from './set_pos_representation/create_image'
import updatePOSRepresentationImageReducer from './set_pos_representation/update_image'
import deletePOSRepresentationImageReducer from './set_pos_representation/delete_image'

// State shape
const initialState = {
  itemsData: {
    // Data
    items: {
      result: [],
      data: {},
    },

    // Error
    error: {
      message: '',
      data: {},
      code: null,
    },

    // Status
    readingItems: false,
    readItemsFailed: false,
    readItemsSucceeded: false,
  },

  createItemForm: {
    isAddItemDialogOpened: false,

    // Input
    name: '',
    forSaleChecked: false,
    quantityInStock: '',
    unit: '',
    price: '',
    categoryId: '',
    cost: '',
    sku: '',
    barCode: '',
    description: '',

    // Error
    error: {
      message: '',
      data: {},
      code: null,
    },

    // Status
    creatingItem: false,
    createItemFailed: false,
    createItemSucceeded: false,
  },

  updateItemForm: {
    isUpdateItemDialogOpened: false,

    // Input
    item: {},

    name: '',
    forSaleChecked: false,
    soldBy: '',
    quantityInStock: '',
    price: '',
    categoryId: '',
    cost: '',
    sku: '',
    barCode: '',

    // Error
    error: {
      message: '',
      data: {},
      code: null,
    },

    // Status
    updatingItem: false,
    updateItemFailed: false,
    updateItemSucceeded: false,
  },

  deleteItemForm: {
    isRemoveItemDialogOpened: false,

    item: {},

    // Error
    error: {
      message: '',
      data: {},
      code: null,
    },

    // Status
    deletingItem: false,
    deleteItemFailed: false,
    deleteItemSucceeded: false,
  },

  setPOSRepresentationForm: {
    isSetPOSRepresentationDialogOpened: false,

    item: {},
    posImage: {},
    posShape: '',
    posColor: '',
  },

  updatePOSRepresentationSymbolColorForm: {
    error: {
      message: '',
      data: {},
      code: null,
    },

    updatingPOSRepresentationSymbolColor: false,
    updatePOSRepresentationSymbolColorFailed: false,
    updatePOSRepresentationSymbolColorSucceeded: false,
  },

  createPOSRepresentationImageForm: {
    error: {
      message: '',
      data: {},
      code: null,
    },

    creatingPOSRepresentationImage: false,
    createPOSRepresentationImageFailed: false,
    createPOSRepresentationImageSucceeded: false,
  },

  updatePOSRepresentationImageForm: {
    error: {
      message: '',
      data: {},
      code: null,
    },

    updatingPOSRepresentationImage: false,
    updatePOSRepresentationImageFailed: false,
    updatePOSRepresentationImageSucceeded: false,
  },

  deletePOSRepresentationImageForm: {
    error: {
      message: '',
      data: {},
      code: null,
    },

    deletingPOSRepresentationImage: false,
    deletePOSRepresentationImageFailed: false,
    deletePOSRepresentationImageSucceeded: false,
  },
}

// Initial reducer
const initialReducer = (state = initialState) => (
  state
)

export default reduceReducers(
  initialReducer,
  createItemReducer,
  readItemsReducer,
  updateItemReducer,
  deleteItemReducer,
  posRepresentationReducer,
  updatePOSRepresentationSymbolColorReducer,
  createPOSRepresentationImageReducer,
  updatePOSRepresentationImageReducer,
  deletePOSRepresentationImageReducer,
)
