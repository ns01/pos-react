export const READ_ITEMS = 'inventory/items/read_items/READ_ITEMS'
export const READ_ITEMS_FAIL = 'inventory/items/read_items/READ_ITEMS_FAIL'
export const READ_ITEMS_SUCCESS = 'inventory/items/read_items/READ_ITEMS_SUCCESS'
