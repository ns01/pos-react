import * as types from './types'


export const readItems = () => ({
  type: types.READ_ITEMS,
})


export const readItemsFail = (data = {}, message = '', code = null) => ({
  type: types.READ_ITEMS_FAIL,
  errors: {
    data,
    message,
    code,
  },
})


export const readItemsSuccess = itemsData => ({
  type: types.READ_ITEMS_SUCCESS,
  itemsData,
})
