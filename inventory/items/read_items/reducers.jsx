// Core
import reduceReducers from 'reduce-reducers'

// Action types
import * as types from './types'


const readItemsReducer = (state, action) => {
  switch (action.type) {
    case types.READ_ITEMS:
      return {
        ...state,

        itemsData: {
          ...state.itemsData,

          error: {
            message: '',
            data: {},
            code: null,
          },

          readingItems: true,
          readItemsFailed: false,
          readItemsSucceeded: false,
        },
      }
    case types.READ_ITEMS_FAIL:
      return {
        ...state,

        itemsData: {
          ...state.itemsData,

          error: {
            message: action.errors.message,
            data: action.errors.data,
            code: action.errors.code,
          },

          readingItems: false,
          readItemsFailed: true,
          readItemsSucceeded: false,
        },
      }
    case types.READ_ITEMS_SUCCESS:
      return {
        ...state,

        itemsData: {
          ...state.itemsData,

          items: {
            result: action.itemsData.result,
            data: action.itemsData.data,
          },

          error: {
            message: '',
            data: {},
            code: null,
          },

          readingItems: false,
          readItemsFailed: false,
          readItemsSucceeded: true,
        },
      }
    default:
      return state
  }
}


export default reduceReducers(readItemsReducer)
