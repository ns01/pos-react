// Core
import api from 'posModules/app/axios'
import { Observable } from 'rxjs'
import { mergeMap } from 'rxjs/operators'
import { ofType } from 'redux-observable'
import normalize from 'posModules/app/helper'
import camelcaseKeys from 'camelcase-keys'

// Types
import { READ_ITEMS } from '../types';

// Operations
import { readItemsSuccess, readItemsFail } from '../operations'


const readItemsEpic = action$ => action$.pipe(
  ofType(READ_ITEMS),
  mergeMap(() => (
    Observable.create((observer) => {
      api.get('/v1/items/').then((response) => {
        const items = camelcaseKeys(response.data.data, { deep: true })
        const itemsData = normalize(items, 'id')

        // stringify number values
        const data = {}

        itemsData.result.forEach((itemId) => {
          const sku = `${itemsData.data[itemId].sku}`
          const price = `${itemsData.data[itemId].price}`
          const cost = `${itemsData.data[itemId].cost}`
          const quantityInStock = `${itemsData.data[itemId].quantityInStock}`

          data[itemId] = {
            ...itemsData.data[itemId],
            sku,
            price,
            cost,
            quantityInStock,
          }
        })

        itemsData.data = data

        observer.next(readItemsSuccess(itemsData))
        observer.complete()
      }).catch((error) => {
        const errorResponse = camelcaseKeys(error.response.data.errors, { deep: true })

        const errors = {
          data: errorResponse.data || {},
          message: errorResponse.message,
          code: errorResponse.code,
        }

        observer.next(readItemsFail(errors.data, errors.message, errors.code))
        observer.complete()
      })
    })
  )),
)


export default readItemsEpic
