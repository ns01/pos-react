// Core
import reduceReducers from 'reduce-reducers'

// Action types
import * as types from './types'


const setPOSRepresentationReducer = (state, action) => {
  switch (action.type) {
    case types.SET_ITEM:
      return {
        ...state,

        setPOSRepresentationForm: {
          ...state.setPOSRepresentationData,

          item: action.item,
        },
      }
    case types.CHANGE_FORM_POS_COLOR:
      return {
        ...state,

        setPOSRepresentationForm: {
          ...state.setPOSRepresentationForm,

          posColor: action.posColor,
        },
      }
    case types.CHANGE_FORM_POS_SHAPE:
      return {
        ...state,

        setPOSRepresentationForm: {
          ...state.setPOSRepresentationForm,

          posShape: action.posShape,
        },
      }
    case types.CHANGE_FORM_POS_IMAGE:
      return {
        ...state,

        setPOSRepresentationForm: {
          ...state.setPOSRepresentationForm,

          posImage: action.posImage,
        },
      }
    case types.SET_POS_REPRESENTATION_RESET:
      return {
        ...state,

        setPOSRepresentationForm: {
          ...state.setPOSRepresentationForm,

          item: {},
          posImage: {},
          posShape: '',
          posColor: '',
        },
      }
    case types.OPEN_SET_POS_REPRESENTATION_DIALOG:
      return {
        ...state,

        setPOSRepresentationForm: {
          ...state.setPOSRepresentationForm,

          isSetPOSRepresentationDialogOpened: true,
        },
      }
    case types.CLOSE_SET_POS_REPRESENTATION_DIALOG:
      return {
        ...state,

        setPOSRepresentationForm: {
          ...state.setPOSRepresentationForm,

          isSetPOSRepresentationDialogOpened: false,
        },
      }
    default:
      return state
  }
}


export default reduceReducers(setPOSRepresentationReducer)
