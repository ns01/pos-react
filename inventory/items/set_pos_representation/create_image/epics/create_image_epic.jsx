// Core
import api from 'posModules/app/axios'
import { Observable } from 'rxjs'
import { mergeMap, debounceTime } from 'rxjs/operators'
import { ofType } from 'redux-observable'
import camelcaseKeys from 'camelcase-keys'

// Operations
import { createPOSRepresentationImageSuccess, createPOSRepresentationImageFail } from '../operations'

// Types
import { CREATE_POS_REPRESENTATION_IMAGE } from '../types';


const createPOSRepresentationImageEpic = action$ => action$.pipe(
  ofType(CREATE_POS_REPRESENTATION_IMAGE),
  debounceTime(1000),
  mergeMap(action => (
    Observable.create((observer) => {
      const formData = new FormData()

      formData.append('image', action.posImage)

      // set content type header to accept file
      api.defaults.headers.common.ContentType = 'multipart/form-data'

      api.post(`v1/items/images/${action.itemId}`, formData).then((response) => {
        const item = camelcaseKeys(response.data.data, { deep: true })

        observer.next(createPOSRepresentationImageSuccess(item))
        observer.complete()
      }).catch((error) => {
        console.log(error)

        const errorResponse = camelcaseKeys(error.response.data.errors, { deep: true })

        const errors = {
          data: errorResponse.data || {},
          message: errorResponse.message,
          code: errorResponse.code,
        }

        observer.next(createPOSRepresentationImageFail(errors.data, errors.message, errors.code))
        observer.complete()
      })

      // remove content type to return to default header config
      delete api.defaults.headers.common.ContentType
    })
  )),
)

export default createPOSRepresentationImageEpic
