import reducer from './reducers'

import * as createPOSRepresentationImageOperations from './operations'


export { createPOSRepresentationImageOperations }

export default reducer
