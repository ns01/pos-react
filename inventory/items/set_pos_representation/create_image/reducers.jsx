// Core
import reduceReducers from 'reduce-reducers'

// Action types
import * as types from './types'


const createPOSRepresentationImageReducer = (state, action) => {
  switch (action.type) {
    case types.CREATE_POS_REPRESENTATION_IMAGE:
      return {
        ...state,

        createPOSRepresentationImageForm: {
          ...state.createPOSRepresentationImageForm,

          error: {
            message: '',
            data: {},
            code: null,
          },

          creatingPOSRepresentationImage: true,
          createPOSRepresentationImageFailed: false,
          createPOSRepresentationImageSucceeded: false,
        },
      }
    case types.CREATE_POS_REPRESENTATION_IMAGE_FAIL:
      return {
        ...state,

        createPOSRepresentationImageForm: {
          ...state.createPOSRepresentationImageForm,

          error: {
            message: action.error.message,
            data: action.error.data,
            code: action.error.code,
          },

          creatingPOSRepresentationImage: false,
          createPOSRepresentationImageFailed: true,
          createPOSRepresentationImageSucceeded: false,
        },
      }
    case types.CREATE_POS_REPRESENTATION_IMAGE_SUCCESS:
      return {
        ...state,

        itemsData: {
          ...state.itemsData,

          items: {
            ...state.itemsData.items,

            data: {
              ...state.itemsData.items.data,

              [action.item.id]: action.item,
            },
          },
        },

        createPOSRepresentationImageForm: {
          ...state.createPOSRepresentationImageForm,

          error: {
            message: '',
            data: {},
            code: null,
          },

          creatingPOSRepresentationImage: false,
          createPOSRepresentationImageFailed: false,
          createPOSRepresentationImageSucceeded: true,
        },
      }
    case types.CREATE_POS_REPRESENTATION_IMAGE_RESET:
      return {
        ...state,

        createPOSRepresentationImageForm: {
          ...state.createPOSRepresentationImageForm,

          posImage: {},

          error: {
            message: '',
            data: {},
            code: null,
          },

          creatingPOSRepresentationImage: false,
          createPOSRepresentationImageFailed: false,
          createPOSRepresentationImageSucceeded: false,
        },
      }
    default:
      return state
  }
}


export default reduceReducers(createPOSRepresentationImageReducer)
