import * as types from './types'


export const createPOSRepresentationImage = (
  itemId,
  posImage,
) => ({
  type: types.CREATE_POS_REPRESENTATION_IMAGE,
  itemId,
  posImage,
})

export const createPOSRepresentationImageFail = (data = {}, message = '', code = null) => ({
  type: types.CREATE_POS_REPRESENTATION_IMAGE_FAIL,
  error: {
    data,
    message,
    code,
  },
})

export const createPOSRepresentationImageSuccess = item => ({
  type: types.CREATE_POS_REPRESENTATION_IMAGE_SUCCESS,
  item,
})

export const createPOSRepresentationImageReset = () => ({
  type: types.CREATE_POS_REPRESENTATION_IMAGE_RESET,
})
