import {
  setItem,
  changeFormPOSColor,
  changeFormPOSShape,
  changeFormPOSImage,

  setPOSRepresentationReset,

  openSetPOSRepresentationDialog,
  closeSetPOSRepresentationDialog,
} from './actions'


export {
  setItem,
  changeFormPOSColor,
  changeFormPOSShape,
  changeFormPOSImage,

  setPOSRepresentationReset,

  openSetPOSRepresentationDialog,
  closeSetPOSRepresentationDialog,
}
