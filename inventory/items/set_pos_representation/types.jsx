// Inputs
export const SET_ITEM = 'inventory/items/set_pos_representation/SET_ITEM'
export const CHANGE_FORM_POS_COLOR = 'inventory/items/set_pos_representation/CHANGE_FORM_POS_COLOR'
export const CHANGE_FORM_POS_SHAPE = 'inventory/items/set_pos_representation/CHANGE_FORM_POS_SHAPE'
export const CHANGE_FORM_POS_IMAGE = 'inventory/items/set_pos_representation/CHANGE_FORM_POS_IMAGE'

export const SET_POS_REPRESENTATION_RESET = 'inventory/items/set_pos_representation/SET_POS_REPRESENTATION_RESET'

// Dialog
export const OPEN_SET_POS_REPRESENTATION_DIALOG = 'inventory/items/set_pos_representation/OPEN_SET_POS_REPRESENTATION_DIALOG'
export const CLOSE_SET_POS_REPRESENTATION_DIALOG = 'inventory/items/set_pos_representation/CLOSE_SET_POS_REPRESENTATION_DIALOG'
