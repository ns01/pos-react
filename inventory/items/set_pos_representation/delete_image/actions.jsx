import * as types from './types'

export const deletePOSRepresentationImage = itemId => ({
  type: types.DELETE_POS_REPRESENTATION_IMAGE,
  itemId,
})

export const deletePOSRepresentationImageFail = (data = {}, message = '', code = null) => ({
  type: types.DELETE_POS_REPRESENTATION_IMAGE_FAIL,
  error: {
    data,
    message,
    code,
  },
})

export const deletePOSRepresentationImageSuccess = item => ({
  type: types.DELETE_POS_REPRESENTATION_IMAGE_SUCCESS,
  item,
})

export const deletePOSRepresentationImageReset = () => ({
  type: types.DELETE_POS_REPRESENTATION_IMAGE_RESET,
})
