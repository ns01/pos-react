// Core
import reduceReducers from 'reduce-reducers'

// Action types
import * as types from './types'


const deletePOSRepresentationImageReducer = (state, action) => {
  switch (action.type) {
    case types.DELETE_POS_REPRESENTATION_IMAGE:
      return {
        ...state,

        deletePOSRepresentationImageForm: {
          error: {
            message: '',
            data: {},
            code: null,
          },

          deletingPOSRepresentationImage: true,
          deletePOSRepresentationImageFailed: false,
          deletePOSRepresentationImageSucceeded: false,
        },
      }
    case types.DELETE_POS_REPRESENTATION_IMAGE_FAIL:
      return {
        ...state,

        deletePOSRepresentationImageForm: {
          error: {
            message: action.error.message,
            data: action.error.data,
            code: action.error.code,
          },

          deletingPOSRepresentationImage: false,
          deletePOSRepresentationImageFailed: true,
          deletePOSRepresentationImageSucceeded: false,
        },
      }
    case types.DELETE_POS_REPRESENTATION_IMAGE_SUCCESS:
      return {
        ...state,

        itemsData: {
          ...state.itemsData,

          items: {
            ...state.itemsData.items,

            data: {
              ...state.itemsData.items.data,

              [action.item.id]: action.item,
            },
          },
        },

        deletePOSRepresentationImageForm: {
          error: {
            message: '',
            data: {},
            code: null,
          },

          deletingPOSRepresentationImage: false,
          deletePOSRepresentationImageFailed: false,
          deletePOSRepresentationImageSucceeded: true,
        },
      }
    case types.DELETE_POS_REPRESENTATION_IMAGE_RESET:
      return {
        ...state,

        deletePOSRepresentationImageForm: {

          error: {
            message: '',
            data: {},
            code: null,
          },

          deletingPOSRepresentationImage: false,
          deletePOSRepresentationImageFailed: false,
          deletePOSRepresentationImageSucceeded: false,
        },
      }
    default:
      return state
  }
}


export default reduceReducers(deletePOSRepresentationImageReducer)
