import {
  deletePOSRepresentationImage,
  deletePOSRepresentationImageFail,
  deletePOSRepresentationImageSuccess,
  deletePOSRepresentationImageReset,
} from './actions'


export {
  deletePOSRepresentationImage,
  deletePOSRepresentationImageFail,
  deletePOSRepresentationImageSuccess,
  deletePOSRepresentationImageReset,
}
