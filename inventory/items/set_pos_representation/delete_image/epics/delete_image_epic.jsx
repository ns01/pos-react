// Core
import api from 'posModules/app/axios'
import { Observable } from 'rxjs'
import { mergeMap, debounceTime } from 'rxjs/operators'
import { ofType } from 'redux-observable'
import camelcaseKeys from 'camelcase-keys'

// Operations
import { deletePOSRepresentationImageSuccess, deletePOSRepresentationImageFail } from '../operations'

// Types
import { DELETE_POS_REPRESENTATION_IMAGE } from '../types';


const deletePOSRepresentationImageEpic = action$ => action$.pipe(
  ofType(DELETE_POS_REPRESENTATION_IMAGE),
  debounceTime(1000),
  mergeMap(action => (
    Observable.create((observer) => {
      api.delete(`v1/items/images/${action.itemId}`).then((response) => {
        console.log(response)
        const item = camelcaseKeys(response.data.data)

        observer.next(deletePOSRepresentationImageSuccess(item))
        observer.complete()
      }).catch((error) => {
        console.log(error)

        const errorResponse = camelcaseKeys(error.response.data.errors, { deep: true })

        const errors = {
          data: errorResponse.data || {},
          message: errorResponse.message,
          code: errorResponse.code,
        }

        observer.next(deletePOSRepresentationImageFail(errors.data, errors.message, errors.code))
        observer.complete()
      })

      // remove content type to return to default header config
      delete api.defaults.headers.common.ContentType
    })
  )),
)

export default deletePOSRepresentationImageEpic
