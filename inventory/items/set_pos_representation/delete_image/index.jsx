import reducer from './reducers'

import * as deletePOSRepresentationImageOperations from './operations'


export { deletePOSRepresentationImageOperations }

export default reducer
