// Core
import reduceReducers from 'reduce-reducers'

// Action types
import * as types from './types'


const updatePOSRepresentationImageReducer = (state, action) => {
  switch (action.type) {
    case types.UPDATE_POS_REPRESENTATION_IMAGE:
      return {
        ...state,

        updatePOSRepresentationImageForm: {
          ...state.updatePOSRepresentationImageForm,

          error: {
            message: '',
            data: {},
            code: null,
          },

          updatingPOSRepresentationImage: true,
          updatePOSRepresentationImageFailed: false,
          updatePOSRepresentationImageSucceeded: false,
        },
      }
    case types.UPDATE_POS_REPRESENTATION_IMAGE_FAIL:
      return {
        ...state,

        updatePOSRepresentationImageForm: {
          ...state.updatePOSRepresentationImageForm,

          error: {
            message: action.error.message,
            data: action.error.data,
            code: action.error.code,
          },

          updatingPOSRepresentationImage: false,
          updatePOSRepresentationImageFailed: true,
          updatePOSRepresentationImageSucceeded: false,
        },
      }
    case types.UPDATE_POS_REPRESENTATION_IMAGE_SUCCESS:
      return {
        ...state,

        itemsData: {
          ...state.itemsData,

          items: {
            ...state.itemsData.items,

            data: {
              ...state.itemsData.items.data,

              [action.item.id]: action.item,
            },
          },
        },

        updatePOSRepresentationImageForm: {
          ...state.createPOSRepresentationImageForm,

          error: {
            message: '',
            data: {},
            code: null,
          },

          updatingPOSRepresentationImage: false,
          updatePOSRepresentationImageFailed: false,
          updatePOSRepresentationImageSucceeded: true,
        },
      }
    case types.UPDATE_POS_REPRESENTATION_IMAGE_RESET:
      return {
        ...state,

        updatePOSRepresentationImageForm: {
          ...state.updatePOSRepresentationImageForm,

          posImage: {},

          error: {
            message: '',
            data: {},
            code: null,
          },

          updatingPOSRepresentationImage: false,
          updatePOSRepresentationImageFailed: false,
          updatePOSRepresentationImageSucceeded: false,
        },
      }
    default:
      return state
  }
}


export default reduceReducers(updatePOSRepresentationImageReducer)
