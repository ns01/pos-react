import * as types from './types'


export const updatePOSRepresentationImage = (
  itemId,
  posImage,
) => ({
  type: types.UPDATE_POS_REPRESENTATION_IMAGE,
  itemId,
  posImage,
})

export const updatePOSRepresentationImageFail = (data = {}, message = '', code = null) => ({
  type: types.UPDATE_POS_REPRESENTATION_IMAGE_FAIL,
  error: {
    data,
    message,
    code,
  },
})

export const updatePOSRepresentationImageSuccess = item => ({
  type: types.UPDATE_POS_REPRESENTATION_IMAGE_SUCCESS,
  item,
})

export const updatePOSRepresentationImageReset = () => ({
  type: types.UPDATE_POS_REPRESENTATION_IMAGE_RESET,
})
