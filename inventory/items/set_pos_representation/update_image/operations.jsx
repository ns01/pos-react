import {
  updatePOSRepresentationImage,
  updatePOSRepresentationImageFail,
  updatePOSRepresentationImageSuccess,
  updatePOSRepresentationImageReset,
} from './actions'


export {
  updatePOSRepresentationImage,
  updatePOSRepresentationImageFail,
  updatePOSRepresentationImageSuccess,
  updatePOSRepresentationImageReset,
}
