import reducer from './reducers'

import * as updatePOSRepresentationImageOperations from './operations'


export { updatePOSRepresentationImageOperations }

export default reducer
