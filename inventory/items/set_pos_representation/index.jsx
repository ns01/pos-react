import reducer from './reducers'

import * as setPOSRepresentationOperations from './operations'


export { setPOSRepresentationOperations }

export default reducer
