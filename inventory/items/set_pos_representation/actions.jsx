import * as types from './types'

export const setItem = item => ({
  type: types.SET_ITEM,
  item,
})

export const changeFormPOSColor = posColor => ({
  type: types.CHANGE_FORM_POS_COLOR,
  posColor,
})

export const changeFormPOSShape = posShape => ({
  type: types.CHANGE_FORM_POS_SHAPE,
  posShape,
})

export const changeFormPOSImage = posImage => ({
  type: types.CHANGE_FORM_POS_IMAGE,
  posImage,
})

export const setPOSRepresentationReset = () => ({
  type: types.SET_POS_REPRESENTATION_RESET,
})

export const openSetPOSRepresentationDialog = () => ({
  type: types.OPEN_SET_POS_REPRESENTATION_DIALOG,
})

export const closeSetPOSRepresentationDialog = () => ({
  type: types.CLOSE_SET_POS_REPRESENTATION_DIALOG,
})
