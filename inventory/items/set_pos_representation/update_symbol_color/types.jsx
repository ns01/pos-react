export const UPDATE_POS_REPRESENTATION_SYMBOL_COLOR = 'inventory/items/set_pos_representation/update_symbol_color/UPDATE_POS_REPRESENTATION_SYMBOL_COLOR'
export const UPDATE_POS_REPRESENTATION_SYMBOL_COLOR_FAIL = 'inventory/items/set_pos_representation/update_symbol_color/UPDATE_POS_REPRESENTATION_SYMBOL_COLOR_FAIL'
export const UPDATE_POS_REPRESENTATION_SYMBOL_COLOR_SUCCESS = 'inventory/items/set_pos_representation/update_symbol_color/UPDATE_POS_REPRESENTATION_SYMBOL_COLOR_SUCCESS'
export const UPDATE_POS_REPRESENTATION_SYMBOL_COLOR_RESET = 'inventory/items/set_pos_representation/update_symbol_color/UPDATE_POS_REPRESENTATION_SYMBOL_COLOR_RESET'
