import * as types from './types'


export const updatePOSRepresentationSymbolColor = (
  itemId,
  posColor,
  posShape,
) => ({
  type: types.UPDATE_POS_REPRESENTATION_SYMBOL_COLOR,
  itemId,
  posColor,
  posShape,
})

export const updatePOSRepresentationSymbolColorFail = (data = {}, message = '', code = null) => ({
  type: types.UPDATE_POS_REPRESENTATION_SYMBOL_COLOR_FAIL,
  error: {
    data,
    message,
    code,
  },
})

export const updatePOSRepresentationSymbolColorSuccess = item => ({
  type: types.UPDATE_POS_REPRESENTATION_SYMBOL_COLOR_SUCCESS,
  item,
})

export const updatePOSRepresentationSymbolColorReset = () => ({
  type: types.UPDATE_POS_REPRESENTATION_SYMBOL_COLOR_RESET,
})
