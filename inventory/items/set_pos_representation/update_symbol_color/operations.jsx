import {
  updatePOSRepresentationSymbolColor,
  updatePOSRepresentationSymbolColorFail,
  updatePOSRepresentationSymbolColorSuccess,
  updatePOSRepresentationSymbolColorReset,
} from './actions'


export {
  updatePOSRepresentationSymbolColor,
  updatePOSRepresentationSymbolColorFail,
  updatePOSRepresentationSymbolColorSuccess,
  updatePOSRepresentationSymbolColorReset,
}
