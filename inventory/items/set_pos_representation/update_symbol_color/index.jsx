import reducer from './reducers'

import * as updatePOSRepresentationSymbolColorOperations from './operations'


export { updatePOSRepresentationSymbolColorOperations }

export default reducer
