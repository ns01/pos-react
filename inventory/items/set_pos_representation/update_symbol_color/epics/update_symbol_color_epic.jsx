// Core
import api from 'posModules/app/axios'
import { Observable } from 'rxjs'
import { mergeMap, debounceTime } from 'rxjs/operators'
import { ofType } from 'redux-observable'
import camelcaseKeys from 'camelcase-keys'

// Operations
import { updatePOSRepresentationSymbolColorSuccess, updatePOSRepresentationSymbolColorFail } from '../operations'

// Types
import { UPDATE_POS_REPRESENTATION_SYMBOL_COLOR } from '../types';


const setPOSRepresentationEpic = action$ => action$.pipe(
  ofType(UPDATE_POS_REPRESENTATION_SYMBOL_COLOR),
  debounceTime(1000),
  mergeMap(action => (
    Observable.create((observer) => {
      api.put(`/v1/items/${action.itemId}/representation`, {
        pos_representation_color: action.posColor,
        pos_representation_shape: action.posShape,
      }).then((response) => {
        const item = camelcaseKeys(response.data.data, { deep: true })

        observer.next(updatePOSRepresentationSymbolColorSuccess(item))
        observer.complete()
      }).catch((error) => {
        const errorResponse = camelcaseKeys(error.response.data.errors, { deep: true })

        const errors = {
          data: errorResponse.data || {},
          message: errorResponse.message,
          code: errorResponse.code,
        }

        observer.next(updatePOSRepresentationSymbolColorFail(
          errors.data,
          errors.message,
          errors.code,
        ))
        observer.complete()
      })
    })
  )),
)

export default setPOSRepresentationEpic
