// Core
import reduceReducers from 'reduce-reducers'

// Action types
import * as types from './types'


const updatePOSRepresentationSymbolColorReducer = (state, action) => {
  switch (action.type) {
    case types.UPDATE_POS_REPRESENTATION_SYMBOL_COLOR:
      return {
        ...state,

        updatePOSRepresentationSymbolColorForm: {
          ...state.updatePOSRepresentationSymbolColorForm,

          error: {
            message: '',
            data: {},
            code: null,
          },

          updatingPOSRepresentationSymbolColor: true,
          updatePOSRepresentationSymbolColorFailed: false,
          updatePOSRepresentationSymbolColorSucceeded: false,
        },
      }
    case types.UPDATE_POS_REPRESENTATION_SYMBOL_COLOR_FAIL:
      return {
        ...state,

        updatePOSRepresentationSymbolColorForm: {
          ...state.updatePOSRepresentationSymbolColorForm,

          error: {
            message: action.error.message,
            data: action.error.data,
            code: action.error.code,
          },

          updatingPOSRepresentationSymbolColor: false,
          updatePOSRepresentationSymbolColorFailed: true,
          updatePOSRepresentationSymbolColorSucceeded: false,
        },
      }
    case types.UPDATE_POS_REPRESENTATION_SYMBOL_COLOR_SUCCESS:
      return {
        ...state,

        itemsData: {
          ...state.itemsData,

          items: {
            ...state.itemsData.items,

            data: {
              ...state.itemsData.items.data,

              [action.item.id]: action.item,
            },
          },
        },

        updatePOSRepresentationSymbolColorForm: {
          ...state.updatePOSRepresentationSymbolColorForm,

          error: {
            message: '',
            data: {},
            code: null,
          },

          updatingPOSRepresentationSymbolColor: false,
          updatePOSRepresentationSymbolColorFailed: false,
          updatePOSRepresentationSymbolColorSucceeded: true,
        },
      }
    case types.UPDATE_POS_REPRESENTATION_SYMBOL_COLOR_RESET:
      return {
        ...state,

        updatePOSRepresentationSymbolColorForm: {
          ...state.updatePOSRepresentationSymbolColorForm,

          item: {},
          posColor: '',
          posShape: '',

          error: {
            message: '',
            data: {},
            code: null,
          },

          updatingPOSRepresentationSymbolColor: false,
          updatePOSRepresentationSymbolColorFailed: false,
          updatePOSRepresentationSymbolColorSucceeded: false,
        },
      }
    default:
      return state
  }
}


export default reduceReducers(updatePOSRepresentationSymbolColorReducer)
