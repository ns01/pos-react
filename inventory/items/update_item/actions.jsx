import * as types from './types'


export const updateItem = (
  itemId,

  name,
  categoryId,
  sku,
  barCode,
  forSaleChecked,
  quantityInStock,
  unit,
  price,
  cost,
  description,
) => ({
  type: types.UPDATE_ITEM,
  itemId,

  name,
  categoryId,
  sku,
  barCode,
  forSaleChecked,
  quantityInStock,
  unit,
  price,
  cost,
  description,
})

export const updateItemFail = (data = {}, message = '', code = null) => ({
  type: types.UPDATE_ITEM_FAIL,
  error: {
    data,
    message,
    code,
  },
})

export const updateItemSuccess = item => ({
  type: types.UPDATE_ITEM_SUCCESS,
  item,
})

export const updateItemReset = () => ({
  type: types.UPDATE_ITEM_RESET,
})

export const setItem = item => ({
  type: types.SET_ITEM,
  item,
})

export const changeFormName = name => ({
  type: types.CHANGE_FORM_NAME,
  name,
})

export const changeFormForSale = forSaleChecked => ({
  type: types.CHANGE_FORM_FOR_SALE,
  forSaleChecked,
})

export const changeFormQuantityInStock = quantityInStock => ({
  type: types.CHANGE_FORM_QUANTITY_IN_STOCK,
  quantityInStock,
})

export const changeFormUnit = unit => ({
  type: types.CHANGE_FORM_UNIT,
  unit,
})

export const changeFormPrice = price => ({
  type: types.CHANGE_FORM_PRICE,
  price,
})

export const changeFormCategory = categoryId => ({
  type: types.CHANGE_FORM_CATEGORY,
  categoryId,
})

export const changeFormCost = cost => ({
  type: types.CHANGE_FORM_COST,
  cost,
})

export const changeFormSKU = sku => ({
  type: types.CHANGE_FORM_SKU,
  sku,
})

export const changeFormBarCode = barCode => ({
  type: types.CHANGE_FORM_BAR_CODE,
  barCode,
})

export const changeFormDescription = description => ({
  type: types.CHANGE_FORM_DESCRIPTION,
  description,
})

export const openUpdateItemDialog = () => ({
  type: types.OPEN_UPDATE_ITEM_DIALOG,
})

export const closeUpdateItemDialog = () => ({
  type: types.CLOSE_UPDATE_ITEM_DIALOG,
})
