// Core
import api from 'posModules/app/axios'
import { Observable } from 'rxjs'
import { mergeMap, debounceTime } from 'rxjs/operators'
import { ofType } from 'redux-observable'
import camelcaseKeys from 'camelcase-keys'

// Operations
import { updateItemSuccess, updateItemFail } from '../operations'

// Types
import { UPDATE_ITEM } from '../types';


const updateItemEpic = action$ => action$.pipe(
  ofType(UPDATE_ITEM),
  debounceTime(1000),
  mergeMap(action => (
    Observable.create((observer) => {
      const formErrors = {}

      if (action.name.trim() === '') {
        formErrors.name = 'name is required'
      }

      if (Object.getOwnPropertyNames(formErrors).length > 0) {
        observer.next(updateItemFail(formErrors))
        observer.complete()
        return
      }

      api.put(`/v1/items/${action.itemId}`, {
        name: action.name,
        category: action.categoryId !== '0' ? action.categoryId : '',
        is_available: action.forSaleChecked,
        unit: action.unit,
        quantity_in_stock: action.quantityInStock || 0,
        price: action.price || 0,
        cost: action.cost || 0,
        sku: action.sku,
        bar_code: action.barCode,
        description: action.description,
      }).then((response) => {
        const item = camelcaseKeys(response.data.data, { deep: true })

        observer.next(updateItemSuccess(item))
        observer.complete()
      }).catch((error) => {
        const errorResponse = camelcaseKeys(error.response.data.errors, { deep: true })

        const errors = {
          data: errorResponse.data || {},
          message: errorResponse.message,
          code: errorResponse.code,
        }

        observer.next(updateItemFail(errors.data, errors.message, errors.code))
        observer.complete()
      })
    })
  )),
)

export default updateItemEpic
