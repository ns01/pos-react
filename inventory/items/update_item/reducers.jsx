// Core
import reduceReducers from 'reduce-reducers'

// Action types
import * as types from './types'


const updateItemReducer = (state, action) => {
  switch (action.type) {
    case types.UPDATE_ITEM:
      return {
        ...state,

        updateItemForm: {
          ...state.updateItemForm,

          error: {
            message: '',
            data: {},
            code: null,
          },

          updatingItem: true,
          updateItemFailed: false,
          updateItemSucceeded: false,
        },
      }
    case types.UPDATE_ITEM_FAIL: {
      return {
        ...state,

        updateItemForm: {
          ...state.updateItemForm,

          error: {
            message: action.error.message,
            data: action.error.data,
            code: action.error.code,
          },

          updatingItem: false,
          updateItemFailed: true,
          updateItemSucceeded: false,
        },
      }
    }
    case types.UPDATE_ITEM_SUCCESS: {
      return {
        ...state,

        itemsData: {
          ...state.itemsData,

          items: {
            ...state.itemsData.items,

            data: {
              ...state.itemsData.items.data,

              [action.item.id]: action.item,
            },
          },
        },

        updateItemForm: {
          ...state.updateItemForm,

          error: {
            message: '',
            data: {},
            code: null,
          },

          updatingItem: false,
          updateItemFailed: false,
          updateItemSucceeded: true,
        },
      }
    }
    case types.UPDATE_ITEM_RESET:
      return {
        ...state,

        updateItemForm: {
          isUpdateItemDialogOpened: false,

          item: {},
          name: '',
          forSaleChecked: false,
          unit: '',
          quantityInStock: '',
          price: '',
          categoryId: '',
          cost: '',
          sku: '',
          barCode: '',
          description: '',

          error: {
            message: '',
            data: {},
            code: null,
          },

          updatingItem: false,
          updateItemFailed: false,
          updateItemSucceeded: false,
        },
      }
    case types.SET_ITEM:
      return {
        ...state,

        updateItemForm: {
          ...state.updateItemForm,

          item: action.item,
        },
      }
    case types.CHANGE_FORM_NAME:
      return {
        ...state,

        updateItemForm: {
          ...state.updateItemForm,

          name: action.name,
        },
      }
    case types.CHANGE_FORM_FOR_SALE:
      return {
        ...state,

        updateItemForm: {
          ...state.updateItemForm,

          forSaleChecked: action.forSaleChecked,
        },
      }
    case types.CHANGE_FORM_QUANTITY_IN_STOCK:
      return {
        ...state,

        updateItemForm: {
          ...state.updateItemForm,

          quantityInStock: action.quantityInStock,
        },
      }
    case types.CHANGE_FORM_UNIT:
      return {
        ...state,

        updateItemForm: {
          ...state.updateItemForm,

          unit: action.unit,
        },
      }
    case types.CHANGE_FORM_PRICE:
      return {
        ...state,

        updateItemForm: {
          ...state.updateItemForm,

          price: action.price,
        },
      }
    case types.CHANGE_FORM_CATEGORY:
      return {
        ...state,

        updateItemForm: {
          ...state.updateItemForm,

          categoryId: action.categoryId,
        },
      }
    case types.CHANGE_FORM_COST:
      return {
        ...state,

        updateItemForm: {
          ...state.updateItemForm,

          cost: action.cost,
        },
      }
    case types.CHANGE_FORM_SKU:
      return {
        ...state,

        updateItemForm: {
          ...state.updateItemForm,

          sku: action.sku,
        },
      }
    case types.CHANGE_FORM_BAR_CODE:
      return {
        ...state,

        updateItemForm: {
          ...state.updateItemForm,

          barCode: action.barCode,
        },
      }
    case types.CHANGE_FORM_DESCRIPTION:
      return {
        ...state,

        updateItemForm: {
          ...state.updateItemForm,

          description: action.description,
        },
      }
    case types.OPEN_UPDATE_ITEM_DIALOG:
      return {
        ...state,

        updateItemForm: {
          ...state.updateItemForm,

          isUpdateItemDialogOpened: true,
        },
      }
    case types.CLOSE_UPDATE_ITEM_DIALOG:
      return {
        ...state,

        updateItemForm: {
          ...state.updateItemForm,

          isUpdateItemDialogOpened: false,
        },
      }
    default:
      return state
  }
}


export default reduceReducers(updateItemReducer)
