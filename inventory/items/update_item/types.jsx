export const UPDATE_ITEM = 'inventory/items/update_item/UPDATE_ITEM'
export const UPDATE_ITEM_FAIL = 'inventory/items/update_item/UPDATE_ITEM_FAIL'
export const UPDATE_ITEM_SUCCESS = 'inventory/items/update_item/UPDATE_ITEM_SUCCESS'
export const UPDATE_ITEM_RESET = 'inventory/items/update_item/UPDATE_ITEM_RESET'

export const SET_ITEM = 'inventory/items/update_item/SET_ITEM'

// Inputs
export const CHANGE_FORM_NAME = 'inventory/items/update_item/CHANGE_FORM_NAME'

// Conditional
export const CHANGE_FORM_FOR_SALE = 'inventory/items/update_item/CHANGE_FORM_FOR_SALE'
export const CHANGE_FORM_QUANTITY_IN_STOCK = 'inventory/items/update_item/CHANGE_FORM_QUANTITY_IN_STOCK'
export const CHANGE_FORM_UNIT = 'inventory/items/update_item/CHANGE_FORM_UNIT'
export const CHANGE_FORM_PRICE = 'inventory/items/update_item/CHANGE_FORM_PRICE'

// Optional
export const CHANGE_FORM_CATEGORY = 'inventory/items/update_item/CHANGE_FORM_CATEGORY'
export const CHANGE_FORM_COST = 'inventory/items/update_item/CHANGE_FORM_COST'
export const CHANGE_FORM_SKU = 'inventory/items/update_item/CHANGE_FORM_SKU'
export const CHANGE_FORM_BAR_CODE = 'inventory/items/update_item/CHANGE_FORM_BAR_CODE'
export const CHANGE_FORM_DESCRIPTION = 'inventory/items/update_item/CHANGE_FORM_DESCRIPTION'

export const OPEN_UPDATE_ITEM_DIALOG = 'inventory/items/update_item/OPEN_UPDATE_ITEM_DIALOG'
export const CLOSE_UPDATE_ITEM_DIALOG = 'inventory/items/update_item/CLOSE_UPDATE_ITEM_DIALOG'
