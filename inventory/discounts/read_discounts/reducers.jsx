// Core
import reduceReducers from 'reduce-reducers'

// Action types
import * as types from './types'


const readDiscountsReducer = (state, action) => {
  switch (action.type) {
    case types.READ_DISCOUNTS:
      return {
        ...state,

        discountsData: {
          ...state.discountsData,

          error: {
            message: '',
            data: {},
            code: null,
          },

          readingDiscounts: true,
          readDiscountsFailed: false,
          readDiscountsSucceeded: false,
        },
      }
    case types.READ_DISCOUNTS_FAIL:
      return {
        ...state,

        discountsData: {
          ...state.discountsData,

          error: {
            message: action.errors.message,
            data: action.errors.data,
            code: action.errors.code,
          },

          readingDiscounts: false,
          readDiscountsFailed: true,
          readDiscountsSucceeded: false,
        },
      }
    case types.READ_DISCOUNTS_SUCCESS:
      return {
        ...state,

        discountsData: {
          ...state.discountsData,

          discounts: {
            result: action.discountsData.result,
            data: action.discountsData.data,
          },

          error: {
            message: '',
            data: {},
            code: null,
          },

          readingDiscounts: false,
          readDiscountsFailed: false,
          readDiscountsSucceeded: true,
        },
      }
    default:
      return state
  }
}


export default reduceReducers(readDiscountsReducer)
