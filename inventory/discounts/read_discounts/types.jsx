export const READ_DISCOUNTS = 'inventory/discounts/read_discounts/READ_DISCOUNTS'
export const READ_DISCOUNTS_FAIL = 'inventory/discounts/read_discounts/READ_DISCOUNTS_FAIL'
export const READ_DISCOUNTS_SUCCESS = 'inventory/discounts/read_discounts/READ_DISCOUNTS_SUCCESS'
