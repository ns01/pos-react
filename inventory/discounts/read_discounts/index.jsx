import reducer from './reducers'

import * as readDiscountsOperations from './operations'


export { readDiscountsOperations }

export default reducer
