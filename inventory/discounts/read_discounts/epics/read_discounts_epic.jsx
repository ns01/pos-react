// Core
import api from 'posModules/app/axios'
import { Observable } from 'rxjs'
import { mergeMap } from 'rxjs/operators'
import { ofType } from 'redux-observable'
import normalize from 'posModules/app/helper'
import camelcaseKeys from 'camelcase-keys'

// Types
import { READ_DISCOUNTS } from '../types';

// Operations
import { readDiscountsSuccess, readDiscountsFail } from '../operations'


const readDiscountsEpic = action$ => action$.pipe(
  ofType(READ_DISCOUNTS),
  mergeMap(() => (
    Observable.create((observer) => {
      api.get('/v1/items/discounts').then((response) => {
        const discounts = camelcaseKeys(response.data.data, { deep: true })
        const discountsData = normalize(discounts, 'id')

        // stringify number values
        const data = {}

        discountsData.result.forEach((discountId) => {
          const value = `${discountsData.data[discountId].value}`

          data[discountId] = {
            ...discountsData.data[discountId],
            value,
          }
        })

        discountsData.data = data

        observer.next(readDiscountsSuccess(discountsData))
        observer.complete()
      }).catch((error) => {
        const errorResponse = camelcaseKeys(error.response.data.errors, { deep: true })

        const errors = {
          data: errorResponse.data || {},
          message: errorResponse.message,
          code: errorResponse.code,
        }

        observer.next(readDiscountsFail(errors.data, errors.message, errors.code))
        observer.complete()
      })
    })
  )),
)


export default readDiscountsEpic
