import * as types from './types'


export const readDiscounts = () => ({
  type: types.READ_DISCOUNTS,
})


export const readDiscountsFail = (data = {}, message = '', code = null) => ({
  type: types.READ_DISCOUNTS_FAIL,
  errors: {
    data,
    message,
    code,
  },
})


export const readDiscountsSuccess = discountsData => ({
  type: types.READ_DISCOUNTS_SUCCESS,
  discountsData,
})
