// Core
import reduceReducers from 'reduce-reducers'

// Reducers
import createDiscountReducer from './create_discount'
import readDiscountsReducer from './read_discounts'
import updateDiscountReducer from './update_discount'
import deleteDiscountReducer from './delete_discount'


// State shape
const initialState = {
  discountsData: {
    // Data
    discounts: {
      result: [],
      data: {},
    },

    // Error
    error: {
      message: '',
      data: {},
      code: null,
    },

    // Status
    readingDiscounts: false,
    readDiscountsFailed: false,
    readDiscountsSucceeded: false,
  },

  createDiscountForm: {
    isAddDiscountDialogOpened: false,

    // Input
    name: '',
    type: '',
    value: '',

    // Error
    error: {
      message: '',
      data: {},
      code: null,
    },

    // Status
    creatingDiscount: false,
    createDiscountFailed: false,
    createDiscountSucceeded: false,
  },

  updateDiscountForm: {
    isUpdateDiscountDialogOpened: false,

    // Input
    discount: {},

    name: '',
    type: '',
    value: '',

    // Error
    error: {
      message: '',
      data: {},
      code: null,
    },

    // Status
    updatingDiscount: false,
    updateDiscountFailed: false,
    updateDiscountSucceeded: false,
  },

  deleteDiscountForm: {
    isRemoveDiscountDialogOpened: false,

    discount: {},

    // Error
    error: {
      message: '',
      data: {},
      code: null,
    },

    // Status
    deletingDiscount: false,
    deleteDiscountFailed: false,
    deleteDiscountSucceeded: false,
  },
}

// Initial reducer
const initialReducer = (state = initialState) => (
  state
)

export default reduceReducers(
  initialReducer,
  createDiscountReducer,
  readDiscountsReducer,
  updateDiscountReducer,
  deleteDiscountReducer,
)
