export const CREATE_DISCOUNT = 'inventory/discounts/create_discount/CREATE_DISCOUNT'
export const CREATE_DISCOUNT_FAIL = 'inventory/discounts/create_discount/CREATE_DISCOUNT_FAIL'
export const CREATE_DISCOUNT_SUCCESS = 'inventory/discounts/create_discount/CREATE_DISCOUNT_SUCCESS'
export const CREATE_DISCOUNT_RESET = 'inventory/discounts/create_discount/CREATE_DISCOUNT_RESET'

// Inputs
export const CHANGE_FORM_NAME = 'inventory/discounts/create_discount/CHANGE_FORM_NAME'

// Optional
export const CHANGE_FORM_TYPE = 'inventory/discounts/create_discount/CHANGE_FORM_TYPE'
export const CHANGE_FORM_VALUE = 'inventory/discounts/create_discount/CHANGE_FORM_VALUE'

export const OPEN_CREATE_DISCOUNT_DIALOG = 'inventory/discounts/create_discount/OPEN_CREATE_DISCOUNT_DIALOG'
export const CLOSE_CREATE_DISCOUNT_DIALOG = 'inventory/discounts/create_discount/CLOSE_CREATE_DISCOUNT_DIALOG'
