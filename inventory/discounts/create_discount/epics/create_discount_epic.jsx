// Core
import api from 'posModules/app/axios'
import { Observable } from 'rxjs'
import { mergeMap, debounceTime } from 'rxjs/operators'
import { ofType } from 'redux-observable'
import camelcaseKeys from 'camelcase-keys'

// Operations
import { createDiscountSuccess, createDiscountFail } from '../operations'

// Types
import { CREATE_DISCOUNT } from '../types';

const createDiscountEpic = action$ => action$.pipe(
  ofType(CREATE_DISCOUNT),
  debounceTime(1000),
  mergeMap(action => (
    Observable.create((observer) => {
      const formErrors = {}

      if (action.name.trim() === '') {
        formErrors.name = 'name is required'
      }

      if (Object.getOwnPropertyNames(formErrors).length > 0) {
        observer.next(createDiscountFail(formErrors))
        observer.complete()
        return
      }

      api.post('/v1/items/discounts', {
        name: action.name,
        type: action.discountType,
        value: action.value || 0,
      }).then((response) => {
        const discount = camelcaseKeys(response.data.data, { deep: true })

        observer.next(createDiscountSuccess(discount))
        observer.complete()
      }).catch((error) => {
        const errorResponse = camelcaseKeys(error.response.data.errors, { deep: true })

        const errors = {
          data: errorResponse.data || {},
          message: errorResponse.message,
          code: errorResponse.code,
        }

        observer.next(createDiscountFail(errors.data, errors.message, errors.code))
        observer.complete()
      })
    })
  )),
)

export default createDiscountEpic
