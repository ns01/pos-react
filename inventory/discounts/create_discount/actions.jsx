import * as types from './types'


export const createDiscount = (
  name,
  discountType,
  value,
) => ({
  type: types.CREATE_DISCOUNT,
  name,
  discountType,
  value,
})

export const createDiscountFail = (data = {}, message = '', code = null) => ({
  type: types.CREATE_DISCOUNT_FAIL,
  error: {
    data,
    message,
    code,
  },
})

export const createDiscountSuccess = discount => ({
  type: types.CREATE_DISCOUNT_SUCCESS,
  discount,
})

export const createDiscountReset = () => ({
  type: types.CREATE_DISCOUNT_RESET,
})

export const changeFormName = name => ({
  type: types.CHANGE_FORM_NAME,
  name,
})

export const changeFormType = discountType => ({
  type: types.CHANGE_FORM_TYPE,
  discountType,
})

export const changeFormValue = value => ({
  type: types.CHANGE_FORM_VALUE,
  value,
})

export const openAddDiscountDialog = () => ({
  type: types.OPEN_CREATE_DISCOUNT_DIALOG,
})

export const closeAddDiscountDialog = () => ({
  type: types.CLOSE_CREATE_DISCOUNT_DIALOG,
})
