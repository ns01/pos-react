// Core
import reduceReducers from 'reduce-reducers'

// Action types
import * as types from './types'


const createDiscountReducer = (state, action) => {
  switch (action.type) {
    case types.CREATE_DISCOUNT:
      return {
        ...state,

        createDiscountForm: {
          ...state.createDiscountForm,

          error: {
            message: '',
            data: {},
            code: null,
          },

          creatingDiscount: true,
          createDiscountFailed: false,
          createDiscountSucceeded: false,
        },
      }
    case types.CREATE_DISCOUNT_FAIL:
      return {
        ...state,

        createDiscountForm: {
          ...state.createDiscountForm,

          error: {
            message: action.error.message,
            data: action.error.data,
            code: action.error.code,
          },

          creatingDiscount: false,
          createDiscountFailed: true,
          createDiscountSucceeded: false,
        },
      }
    case types.CREATE_DISCOUNT_SUCCESS: {
      const { result } = state.discountsData.discounts

      result.unshift(action.discount.id)

      return {
        ...state,

        discountsData: {
          ...state.discountsData,

          discounts: {
            data: {
              ...state.discountsData.discounts.data,

              [action.discount.id]: action.discount,
            },

            result,
          },
        },

        createDiscountForm: {
          ...state.createDiscountForm,

          error: {
            message: '',
            data: {},
            code: null,
          },

          creatingDiscount: false,
          createDiscountFailed: false,
          createDiscountSucceeded: true,
        },
      }
    }
    case types.CREATE_DISCOUNT_RESET:
      return {
        ...state,

        createDiscountForm: {
          isAddDiscountDialogOpened: false,

          name: '',
          type: '',
          value: '',

          error: {
            message: '',
            data: {},
            code: null,
          },

          creatingDiscount: false,
          createDiscountFailed: false,
          createDiscountSucceeded: false,
        },
      }
    case types.CHANGE_FORM_NAME:
      return {
        ...state,

        createDiscountForm: {
          ...state.createDiscountForm,

          name: action.name,
        },
      }
    case types.CHANGE_FORM_TYPE:
      return {
        ...state,

        createDiscountForm: {
          ...state.createDiscountForm,

          type: action.discountType,
        },
      }
    case types.CHANGE_FORM_VALUE:
      return {
        ...state,

        createDiscountForm: {
          ...state.createDiscountForm,

          value: action.value,
        },
      }
    case types.OPEN_CREATE_DISCOUNT_DIALOG:
      return {
        ...state,

        createDiscountForm: {
          ...state.createDiscountForm,

          isAddDiscountDialogOpened: true,
        },
      }
    case types.CLOSE_CREATE_DISCOUNT_DIALOG:
      return {
        ...state,

        createDiscountForm: {
          ...state.createDiscountForm,

          isAddDiscountDialogOpened: false,
        },
      }
    default:
      return state
  }
}


export default reduceReducers(createDiscountReducer)
