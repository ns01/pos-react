export const DELETE_DISCOUNT = 'inventory/discounts/delete_discount/DELETE_DISCOUNT'
export const DELETE_DISCOUNT_FAIL = 'inventory/discounts/delete_discount/DELETE_DISCOUNT_FAIL'
export const DELETE_DISCOUNT_SUCCESS = 'inventory/discounts/delete_discount/DELETE_DISCOUNT_SUCCESS'
export const DELETE_DISCOUNT_RESET = 'inventory/discounts/delete_discount/DELETE_DISCOUNT_RESET'

export const SET_DISCOUNT = 'inventory/discounts/delete_discount/SET_DISCOUNT'

export const OPEN_DELETE_DISCOUNT_DIALOG = 'inventory/discounts/delete_discount/OPEN_DELETE_DISCOUNT_DIALOG'
export const CLOSE_DELETE_DISCOUNT_DIALOG = 'inventory/discounts/delete_discount/CLOSE_DELETE_DISCOUNT_DIALOG'
