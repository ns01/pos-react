// Core
import reduceReducers from 'reduce-reducers'

// Action types
import * as types from './types'


const deleteDiscountReducer = (state, action) => {
  switch (action.type) {
    case types.DELETE_DISCOUNT:
      return {
        ...state,

        deleteDiscountForm: {
          ...state.deleteDiscountForm,

          error: {
            message: '',
            data: {},
            code: null,
          },

          deletingDiscount: true,
          deleteDiscountFailed: false,
          deleteDiscountSucceeded: false,
        },
      }
    case types.DELETE_DISCOUNT_FAIL: {
      return {
        ...state,

        deleteDiscountForm: {
          ...state.deleteDiscountForm,

          error: {
            message: action.error.message,
            data: action.error.data,
            code: action.error.code,
          },

          deletingDiscount: false,
          deleteDiscountFailed: true,
          deleteDiscountSucceeded: false,
        },
      }
    }
    case types.DELETE_DISCOUNT_SUCCESS: {
      const { data, result: discountResult } = state.discountsData.discounts
      delete data[action.discountId]

      const result = discountResult.filter(discountId => discountId !== action.discountId)

      return {
        ...state,

        discountsData: {
          ...state.discountsData,

          discounts: {
            data,
            result,
          },
        },

        deleteDiscountForm: {
          ...state.deleteDiscountForm,

          error: {
            message: '',
            data: {},
            code: null,
          },

          deletingDiscount: false,
          deleteDiscountFailed: false,
          deleteDiscountSucceeded: true,
        },
      }
    }
    case types.DELETE_DISCOUNT_RESET:
      return {
        ...state,

        deleteDiscountForm: {
          isRemoveDiscountDialogOpened: false,
          discount: {},

          error: {
            message: '',
            data: {},
            code: null,
          },

          deletingDiscount: false,
          deleteDiscountFailed: false,
          deleteDiscountSucceeded: false,
        },
      }
    case types.SET_DISCOUNT:
      return {
        ...state,

        deleteDiscountForm: {
          ...state.deleteDiscountForm,

          discount: action.discount,
        },
      }
    case types.OPEN_DELETE_DISCOUNT_DIALOG:
      return {
        ...state,

        deleteDiscountForm: {
          ...state.deleteDiscountForm,

          isRemoveDiscountDialogOpened: true,
        },
      }
    case types.CLOSE_DELETE_DISCOUNT_DIALOG:
      return {
        ...state,

        deleteDiscountForm: {
          ...state.deleteDiscountForm,

          isRemoveDiscountDialogOpened: false,
        },
      }
    default:
      return state
  }
}


export default reduceReducers(deleteDiscountReducer)
