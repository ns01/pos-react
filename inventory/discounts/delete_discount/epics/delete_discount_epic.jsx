// Core
import api from 'posModules/app/axios'
import { Observable } from 'rxjs'
import { mergeMap, debounceTime } from 'rxjs/operators'
import { ofType } from 'redux-observable'
import camelcaseKeys from 'camelcase-keys'

// Operations
import { deleteDiscountSuccess, deleteDiscountFail } from '../operations'

// Types
import { DELETE_DISCOUNT } from '../types';


const deleteDiscountEpic = action$ => action$.pipe(
  ofType(DELETE_DISCOUNT),
  debounceTime(1000),
  mergeMap(action => (
    Observable.create((observer) => {
      api.delete(`/v1/items/discounts/${action.discountId}`).then(() => {
        observer.next(deleteDiscountSuccess(action.discountId))
        observer.complete()
      }).catch((error) => {
        const errorResponse = camelcaseKeys(error.response.data.errors, { deep: true })

        const errors = {
          data: errorResponse.data || {},
          message: errorResponse.message,
          code: errorResponse.code,
        }

        observer.next(deleteDiscountFail(errors.data, errors.message, errors.code))
        observer.complete()
      })
    })
  )),
)

export default deleteDiscountEpic
