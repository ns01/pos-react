import * as types from './types'


export const deleteDiscount = discountId => ({
  type: types.DELETE_DISCOUNT,
  discountId,
})

export const deleteDiscountFail = (data = {}, message = '', code = null) => ({
  type: types.DELETE_DISCOUNT_FAIL,
  error: {
    data,
    message,
    code,
  },
})

export const deleteDiscountSuccess = discountId => ({
  type: types.DELETE_DISCOUNT_SUCCESS,
  discountId,
})

export const deleteDiscountReset = () => ({
  type: types.DELETE_DISCOUNT_RESET,
})

export const setDiscount = discount => ({
  type: types.SET_DISCOUNT,
  discount,
})

export const openDeleteDiscountDialog = () => ({
  type: types.OPEN_DELETE_DISCOUNT_DIALOG,
})

export const closeDeleteDiscountDialog = () => ({
  type: types.CLOSE_DELETE_DISCOUNT_DIALOG,
})
