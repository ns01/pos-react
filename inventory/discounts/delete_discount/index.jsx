import reducer from './reducers'

import * as deleteDiscountOperations from './operations'


export { deleteDiscountOperations }

export default reducer
