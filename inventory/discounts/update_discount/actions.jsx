import * as types from './types'


export const updateDiscount = (
  discountId,

  name,
  discountType,
  value,
) => ({
  type: types.UPDATE_DISCOUNT,
  discountId,

  name,
  discountType,
  value,
})

export const updateDiscountFail = (data = {}, message = '', code = null) => ({
  type: types.UPDATE_DISCOUNT_FAIL,
  error: {
    data,
    message,
    code,
  },
})

export const updateDiscountSuccess = discount => ({
  type: types.UPDATE_DISCOUNT_SUCCESS,
  discount,
})

export const updateDiscountReset = () => ({
  type: types.UPDATE_DISCOUNT_RESET,
})

export const setDiscount = discount => ({
  type: types.SET_DISCOUNT,
  discount,
})

export const changeFormName = name => ({
  type: types.CHANGE_FORM_NAME,
  name,
})

export const changeFormType = discountType => ({
  type: types.CHANGE_FORM_TYPE,
  discountType,
})

export const changeFormValue = value => ({
  type: types.CHANGE_FORM_VALUE,
  value,
})

export const openUpdateDiscountDialog = () => ({
  type: types.OPEN_UPDATE_DISCOUNT_DIALOG,
})

export const closeUpdateDiscountDialog = () => ({
  type: types.CLOSE_UPDATE_DISCOUNT_DIALOG,
})
