import reducer from './reducers'

import * as updateDiscountOperations from './operations'


export { updateDiscountOperations }

export default reducer
