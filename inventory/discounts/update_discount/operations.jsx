import {
  updateDiscount,
  updateDiscountFail,
  updateDiscountSuccess,
  updateDiscountReset,

  setDiscount,

  changeFormName,
  changeFormType,
  changeFormValue,

  openUpdateDiscountDialog,
  closeUpdateDiscountDialog,
} from './actions'


export {
  updateDiscount,
  updateDiscountFail,
  updateDiscountSuccess,
  updateDiscountReset,

  setDiscount,

  changeFormName,
  changeFormType,
  changeFormValue,

  openUpdateDiscountDialog,
  closeUpdateDiscountDialog,
}
