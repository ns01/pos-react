// Core
import reduceReducers from 'reduce-reducers'

// Action types
import * as types from './types'


const updateDiscountReducer = (state, action) => {
  switch (action.type) {
    case types.UPDATE_DISCOUNT:
      return {
        ...state,

        updateDiscountForm: {
          ...state.updateDiscountForm,

          error: {
            message: '',
            data: {},
            code: null,
          },

          updatingDiscount: true,
          updateDiscountFailed: false,
          updateDiscountSucceeded: false,
        },
      }
    case types.UPDATE_DISCOUNT_FAIL: {
      return {
        ...state,

        updateDiscountForm: {
          ...state.updateDiscountForm,

          error: {
            message: action.error.message,
            data: action.error.data,
            code: action.error.code,
          },

          updatingDiscount: false,
          updateDiscountFailed: true,
          updateDiscountSucceeded: false,
        },
      }
    }
    case types.UPDATE_DISCOUNT_SUCCESS: {
      return {
        ...state,

        discountsData: {
          ...state.discountsData,

          discounts: {
            ...state.discountsData.discounts,

            data: {
              ...state.discountsData.discounts.data,

              [action.discount.id]: action.discount,
            },
          },
        },

        updateDiscountForm: {
          ...state.updateDiscountForm,

          error: {
            message: '',
            data: {},
            code: null,
          },

          updatingDiscount: false,
          updateDiscountFailed: false,
          updateDiscountSucceeded: true,
        },
      }
    }
    case types.UPDATE_DISCOUNT_RESET:
      return {
        ...state,

        updateDiscountForm: {
          isUpdateDiscountDialogOpened: false,

          discount: {},
          name: '',
          type: '',
          value: '',

          error: {
            message: '',
            data: {},
            code: null,
          },

          updatingDiscount: false,
          updateDiscountFailed: false,
          updateDiscountSucceeded: false,
        },
      }
    case types.SET_DISCOUNT:
      return {
        ...state,

        updateDiscountForm: {
          ...state.updateDiscountForm,

          discount: action.discount,
        },
      }
    case types.CHANGE_FORM_NAME:
      return {
        ...state,

        updateDiscountForm: {
          ...state.updateDiscountForm,

          name: action.name,
        },
      }
    case types.CHANGE_FORM_TYPE:
      return {
        ...state,

        updateDiscountForm: {
          ...state.updateDiscountForm,

          type: action.discountType,
        },
      }
    case types.CHANGE_FORM_VALUE:
      return {
        ...state,

        updateDiscountForm: {
          ...state.updateDiscountForm,

          value: action.value,
        },
      }
    case types.OPEN_UPDATE_DISCOUNT_DIALOG:
      return {
        ...state,

        updateDiscountForm: {
          ...state.updateDiscountForm,

          isUpdateDiscountDialogOpened: true,
        },
      }
    case types.CLOSE_UPDATE_DISCOUNT_DIALOG:
      return {
        ...state,

        updateDiscountForm: {
          ...state.updateDiscountForm,

          isUpdateDiscountDialogOpened: false,
        },
      }
    default:
      return state
  }
}


export default reduceReducers(updateDiscountReducer)
