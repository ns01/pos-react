// Core
import api from 'posModules/app/axios'
import { Observable } from 'rxjs'
import { mergeMap, debounceTime } from 'rxjs/operators'
import { ofType } from 'redux-observable'
import camelcaseKeys from 'camelcase-keys'

// Operations
import { updateDiscountSuccess, updateDiscountFail } from '../operations'

// Types
import { UPDATE_DISCOUNT } from '../types';


const updateDiscountEpic = action$ => action$.pipe(
  ofType(UPDATE_DISCOUNT),
  debounceTime(1000),
  mergeMap(action => (
    Observable.create((observer) => {
      const formErrors = {}

      if (action.name.trim() === '') {
        formErrors.name = 'name is required'
      }

      if (action.discountType === 'P' && +action.value > 100) {
        formErrors.value = 'percentage value cannot be greater than 100'
      }

      if (action.discountType === 'A' && +action.value > 100000) {
        formErrors.value = 'amount value cannot be greater than 100,000.00'
      }

      if (Object.getOwnPropertyNames(formErrors).length > 0) {
        observer.next(updateDiscountFail(formErrors))
        observer.complete()
        return
      }

      api.put(`/v1/items/discounts/${action.discountId}`, {
        name: action.name,
        type: action.discountType,
        value: action.value || 0,
      }).then((response) => {
        const discount = camelcaseKeys(response.data.data, { deep: true })

        observer.next(updateDiscountSuccess(discount))
        observer.complete()
      }).catch((error) => {
        const errorResponse = camelcaseKeys(error.response.data.errors, { deep: true })

        const errors = {
          data: errorResponse.data.discountForm || {},
          message: errorResponse.message,
          code: errorResponse.code,
        }

        if (errors.data.value === 'Ensure this value has at most 15 characters (it has 16).') {
          errors.data.value = 'Amount too large.'
        }

        observer.next(updateDiscountFail(errors.data, errors.message, errors.code))
        observer.complete()
      })
    })
  )),
)


export default updateDiscountEpic
