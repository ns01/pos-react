export const UPDATE_DISCOUNT = 'inventory/discounts/update_discount/UPDATE_DISCOUNT'
export const UPDATE_DISCOUNT_FAIL = 'inventory/discounts/update_discount/UPDATE_DISCOUNT_FAIL'
export const UPDATE_DISCOUNT_SUCCESS = 'inventory/discounts/update_discount/UPDATE_DISCOUNT_SUCCESS'
export const UPDATE_DISCOUNT_RESET = 'inventory/discounts/update_discount/UPDATE_DISCOUNT_RESET'

export const SET_DISCOUNT = 'inventory/discounts/update_discount/SET_DISCOUNT'

// Inputs
export const CHANGE_FORM_NAME = 'inventory/discounts/update_discount/CHANGE_FORM_NAME'

// Optional
export const CHANGE_FORM_TYPE = 'inventory/discounts/update_discount/CHANGE_FORM_TYPE'
export const CHANGE_FORM_VALUE = 'inventory/discounts/update_discount/CHANGE_FORM_VALUE'

export const OPEN_UPDATE_DISCOUNT_DIALOG = 'inventory/discounts/update_discount/OPEN_UPDATE_DISCOUNT_DIALOG'
export const CLOSE_UPDATE_DISCOUNT_DIALOG = 'inventory/discounts/update_discount/CLOSE_UPDATE_DISCOUNT_DIALOG'
