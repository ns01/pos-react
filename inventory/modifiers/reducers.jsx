// Core
import reduceReducers from 'reduce-reducers'

// Reducers
import createModifierReducer from './create_modifier'
import readModifiersReducer from './read_modifiers'
import updateModifierReducer from './update_modifier'
import deleteModifierReducer from './delete_modifier'
import setOptionsReducer from './set_options'
import createOptionReducer from './set_options/create_option'
import readOptionsReducer from './set_options/read_options'
import updateOptionReducer from './set_options/update_option'
import deleteOptionReducer from './set_options/delete_option'

// State shape
const initialState = {
  modifiersData: {
    // Data
    modifiers: {
      result: [],
      data: {},
    },

    // Error
    error: {
      message: '',
      data: {},
      code: null,
    },

    // Status
    readingModifiers: false,
    readModifiersFailed: false,
    readModifiersSucceeded: false,
  },

  createModifierForm: {
    isAddModifierDialogOpened: false,

    // Input
    name: '',

    error: {
      message: '',
      data: {},
      code: null,
    },

    creatingModifier: false,
    createModifierFailed: false,
    createModifierSucceeded: false,
  },

  updateModifierForm: {
    isUpdateModifierDialogOpened: false,

    modifier: {},
    name: '',

    error: {
      message: '',
      data: {},
      code: null,
    },

    updatingModifier: false,
    updateModifierFailed: false,
    updateModifierSucceeded: false,
  },

  deleteModifierForm: {
    isRemoveModifierDialogOpened: false,
    modifier: {},

    error: {
      message: '',
      data: {},
      code: null,
    },

    deletingModifier: false,
    deleteModifierFailed: false,
    deleteModifierSucceeded: false,
  },

  optionsData: {
    // Data
    options: {
      result: [],
      data: {},
    },

    // Error
    error: {
      message: '',
      data: {},
      code: null,
    },

    // Status
    readingOptions: false,
    readOptionsFailed: false,
    readOptionsSucceeded: false,
  },

  setOptionsForm: {
    isSetOptionsDialogOpened: false,

    modifier: {},

    // Input
    name: '',
    price: '',
  },

  createOptionForm: {
    isAddOptionDialogOpened: false,

    error: {
      message: '',
      data: {},
      code: null,
    },

    creatingOption: false,
    createOptionFailed: false,
    createOptionSucceeded: false,
  },

  updateOptionForm: {
    isUpdateOptionDialogOpened: false,

    option: {},
    error: {
      message: '',
      data: {},
      code: null,
    },

    updatingOption: false,
    updateOptionFailed: false,
    updateOptionSucceeded: false,
  },

  deleteOptionForm: {
    isRemoveOptionDialogOpened: false,
    option: {},

    error: {
      message: '',
      data: {},
      code: null,
    },

    deletingOption: false,
    deleteOptionFailed: false,
    deleteOptionSucceeded: false,
  },
}

// Initial reducer
const initialReducer = (state = initialState) => (
  state
)

export default reduceReducers(
  initialReducer,
  createModifierReducer,
  readModifiersReducer,
  updateModifierReducer,
  deleteModifierReducer,

  setOptionsReducer,
  createOptionReducer,
  readOptionsReducer,
  updateOptionReducer,
  deleteOptionReducer,
)
