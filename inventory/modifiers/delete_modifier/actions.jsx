import * as types from './types'


export const deleteModifier = modifierId => ({
  type: types.DELETE_MODIFIER,
  modifierId,
})

export const deleteModifierFail = (data = {}, message = '', code = null) => ({
  type: types.DELETE_MODIFIER_FAIL,
  error: {
    data,
    message,
    code,
  },
})

export const deleteModifierSuccess = modifierId => ({
  type: types.DELETE_MODIFIER_SUCCESS,
  modifierId,
})

export const deleteModifierReset = () => ({
  type: types.DELETE_MODIFIER_RESET,
})

export const setModifier = modifier => ({
  type: types.SET_MODIFIER,
  modifier,
})

export const openDeleteModifierDialog = () => ({
  type: types.OPEN_DELETE_MODIFIER_DIALOG,
})

export const closeDeleteModifierDialog = () => ({
  type: types.CLOSE_DELETE_MODIFIER_DIALOG,
})
