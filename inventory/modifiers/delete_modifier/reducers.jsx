// Core
import reduceReducers from 'reduce-reducers'

// Action types
import * as types from './types'


const deleteModifierReducer = (state, action) => {
  switch (action.type) {
    case types.DELETE_MODIFIER:
      return {
        ...state,

        deleteModifierForm: {
          ...state.deleteModifierForm,

          error: {
            message: '',
            data: {},
            code: null,
          },

          deletingModifier: true,
          deleteModifierFailed: false,
          deleteModifierSucceeded: false,
        },
      }
    case types.DELETE_MODIFIER_FAIL: {
      return {
        ...state,

        deleteModifierForm: {
          ...state.deleteModifierForm,

          error: {
            message: action.error.message,
            data: action.error.data,
            code: action.error.code,
          },

          deletingModifier: false,
          deleteModifierFailed: true,
          deleteModifierSucceeded: false,
        },
      }
    }
    case types.DELETE_MODIFIER_SUCCESS: {
      const { data, result: modifierResult } = state.modifiersData.modifiers
      delete data[action.modifierId]

      const result = modifierResult.filter(modifierId => modifierId !== action.modifierId)

      return {
        ...state,

        modifiersData: {
          ...state.modifiersData,

          modifiers: {
            data,
            result,
          },
        },

        deleteModifierForm: {
          ...state.deleteModifierForm,

          error: {
            message: '',
            data: {},
            code: null,
          },

          deletingModifier: false,
          deleteModifierFailed: false,
          deleteModifierSucceeded: true,
        },
      }
    }
    case types.DELETE_MODIFIER_RESET:
      return {
        ...state,

        deleteModifierForm: {
          isRemoveModifierDialogOpened: false,
          modifier: {},

          error: {
            message: '',
            data: {},
            code: null,
          },

          deletingModifier: false,
          deleteModifierFailed: false,
          deleteModifierSucceeded: false,
        },
      }
    case types.SET_MODIFIER:
      return {
        ...state,

        deleteModifierForm: {
          ...state.deleteModifierForm,

          modifier: action.modifier,
        },
      }
    case types.OPEN_DELETE_MODIFIER_DIALOG:
      return {
        ...state,

        deleteModifierForm: {
          ...state.deleteModifierForm,

          isRemoveModifierDialogOpened: true,
        },
      }
    case types.CLOSE_DELETE_MODIFIER_DIALOG:
      return {
        ...state,

        deleteModifierForm: {
          ...state.deleteModifierForm,

          isRemoveModifierDialogOpened: false,
        },
      }
    default:
      return state
  }
}


export default reduceReducers(deleteModifierReducer)
