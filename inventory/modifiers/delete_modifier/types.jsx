export const DELETE_MODIFIER = 'inventory/modifiers/delete_modifier/DELETE_MODIFIER'
export const DELETE_MODIFIER_FAIL = 'inventory/modifiers/delete_modifier/DELETE_MODIFIER_FAIL'
export const DELETE_MODIFIER_SUCCESS = 'inventory/modifiers/delete_modifier/DELETE_MODIFIER_SUCCESS'
export const DELETE_MODIFIER_RESET = 'inventory/modifiers/delete_modifier/DELETE_MODIFIER_RESET'

export const SET_MODIFIER = 'inventory/modifiers/delete_modifier/SET_MODIFIER'

export const OPEN_DELETE_MODIFIER_DIALOG = 'inventory/modifiers/delete_modifier/OPEN_DELETE_MODIFIER_DIALOG'
export const CLOSE_DELETE_MODIFIER_DIALOG = 'inventory/modifiers/delete_modifier/CLOSE_DELETE_MODIFIER_DIALOG'
