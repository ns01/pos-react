// Core
import api from 'posModules/app/axios'
import { Observable } from 'rxjs'
import { mergeMap, debounceTime } from 'rxjs/operators'
import { ofType } from 'redux-observable'
import camelcaseKeys from 'camelcase-keys'

// Operations
import { deleteModifierSuccess, deleteModifierFail } from '../operations'

// Types
import { DELETE_MODIFIER } from '../types';


const deleteModifierEpic = action$ => action$.pipe(
  ofType(DELETE_MODIFIER),
  debounceTime(1000),
  mergeMap(action => (
    Observable.create((observer) => {
      api.delete(`/v1/items/modifiers/${action.modifierId}`).then(() => {
        observer.next(deleteModifierSuccess(action.modifierId))
        observer.complete()
      }).catch((error) => {
        const errorResponse = camelcaseKeys(error.response.data.errors, { deep: true })

        const errors = {
          data: errorResponse.data || {},
          message: errorResponse.message,
          code: errorResponse.code,
        }

        observer.next(deleteModifierFail(errors.data, errors.message, errors.code))
        observer.complete()
      })
    })
  )),
)

export default deleteModifierEpic
