export const CREATE_MODIFIER = 'inventory/modifiers/create_modifier/CREATE_MODIFIER'
export const CREATE_MODIFIER_FAIL = 'inventory/modifiers/create_modifier/CREATE_MODIFIER_FAIL'
export const CREATE_MODIFIER_SUCCESS = 'inventory/modifiers/create_modifier/CREATE_MODIFIER_SUCCESS'
export const CREATE_MODIFIER_RESET = 'inventory/modifiers/create_modifier/CREATE_MODIFIER_RESET'

// Inputs
export const CHANGE_FORM_NAME = 'inventory/modifiers/create_modifier/CHANGE_FORM_NAME'

export const OPEN_CREATE_MODIFIER_DIALOG = 'inventory/modifiers/create_item/OPEN_CREATE_MODIFIER_DIALOG'
export const CLOSE_CREATE_MODIFIER_DIALOG = 'inventory/modifiers/create_item/CLOSE_CREATE_MODIFIER_DIALOG'
