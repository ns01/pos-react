import * as types from './types'


export const createModifier = name => ({
  type: types.CREATE_MODIFIER,
  name,
})

export const createModifierFail = (data = {}, message = '', code = null) => ({
  type: types.CREATE_MODIFIER_FAIL,
  error: {
    data,
    message,
    code,
  },
})

export const createModifierSuccess = modifier => ({
  type: types.CREATE_MODIFIER_SUCCESS,
  modifier,
})

export const createModifierReset = () => ({
  type: types.CREATE_MODIFIER_RESET,
})

export const changeFormName = name => ({
  type: types.CHANGE_FORM_NAME,
  name,
})

export const openAddModifierDialog = () => ({
  type: types.OPEN_CREATE_MODIFIER_DIALOG,
})

export const closeAddModifierDialog = () => ({
  type: types.CLOSE_CREATE_MODIFIER_DIALOG,
})
