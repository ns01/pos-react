// Core
import reduceReducers from 'reduce-reducers'

// Action types
import * as types from './types'


const createModifierReducer = (state, action) => {
  switch (action.type) {
    case types.CREATE_MODIFIER:
      return {
        ...state,

        createModifierForm: {
          ...state.createModifierForm,

          error: {
            message: '',
            data: {},
            code: null,
          },

          creatingModifier: true,
          createModifierFailed: false,
          createModifierSucceeded: false,
        },
      }
    case types.CREATE_MODIFIER_FAIL:
      return {
        ...state,

        createModifierForm: {
          ...state.createModifierForm,

          error: {
            message: action.error.message,
            data: action.error.data,
            code: action.error.code,
          },

          creatingModifier: false,
          createModifierFailed: true,
          createModifierSucceeded: false,
        },
      }
    case types.CREATE_MODIFIER_SUCCESS: {
      const { result } = state.modifiersData.modifiers

      result.unshift(action.modifier.id)

      return {
        ...state,

        modifiersData: {
          ...state.modifiersData,

          modifiers: {
            data: {
              ...state.modifiersData.modifiers.data,

              [action.modifier.id]: action.modifier,
            },

            result,
          },
        },


        createModifierForm: {
          ...state.createModifierForm,

          error: {
            message: '',
            data: {},
            code: null,
          },

          creatingModifier: false,
          createModifierFailed: false,
          createModifierSucceeded: true,
        },
      }
    }
    case types.CREATE_MODIFIER_RESET:
      return {
        ...state,

        createModifierForm: {
          name: '',

          error: {
            message: '',
            data: {},
            code: null,
          },

          creatingModifier: false,
          createModifierFailed: false,
          createModifierSucceeded: false,
        },
      }
    case types.CHANGE_FORM_NAME:
      return {
        ...state,

        createModifierForm: {
          ...state.createModifierForm,

          name: action.name,
        },
      }
    case types.OPEN_CREATE_MODIFIER_DIALOG:
      return {
        ...state,

        createModifierForm: {
          ...state.createModifierForm,

          isAddModifierDialogOpened: true,
        },
      }
    case types.CLOSE_CREATE_MODIFIER_DIALOG:
      return {
        ...state,

        createModifierForm: {
          ...state.createModifierForm,

          isAddModifierDialogOpened: false,
        },
      }
    default:
      return state
  }
}


export default reduceReducers(createModifierReducer)
