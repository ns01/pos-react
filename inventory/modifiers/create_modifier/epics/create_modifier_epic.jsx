// Core
import api from 'posModules/app/axios'
import { Observable } from 'rxjs'
import { mergeMap, debounceTime } from 'rxjs/operators'
import { ofType } from 'redux-observable'
import camelcaseKeys from 'camelcase-keys'

// Operations
import { createModifierSuccess, createModifierFail } from '../operations'

// Types
import { CREATE_MODIFIER } from '../types';


const createModifierEpic = action$ => action$.pipe(
  ofType(CREATE_MODIFIER),
  debounceTime(1000),
  mergeMap(action => (
    Observable.create((observer) => {
      const formErrors = {}

      if (action.name.trim() === '') {
        formErrors.name = 'name is required'
      }

      if (Object.getOwnPropertyNames(formErrors).length > 0) {
        observer.next(createModifierFail(formErrors))
        observer.complete()
        return
      }

      api.post('/v1/items/modifiers', {
        name: action.name,
        price: 0,
      }).then((response) => {
        const modifier = camelcaseKeys(response.data.data, { deep: true })

        observer.next(createModifierSuccess(modifier))
        observer.complete()
      }).catch((error) => {
        const errorResponse = camelcaseKeys(error.response.data.errors, { deep: true })

        const errors = {
          data: errorResponse.data || {},
          message: errorResponse.message,
          code: errorResponse.code,
        }

        observer.next(createModifierFail(errors.data, errors.message, errors.code))
        observer.complete()
      })
    })
  )),
)

export default createModifierEpic
