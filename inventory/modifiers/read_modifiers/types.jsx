export const READ_MODIFIERS = 'inventory/modifiers/read_modifiers/READ_MODIFIERS'
export const READ_MODIFIERS_FAIL = 'inventory/modifiers/read_modifiers/READ_MODIFIERS_FAIL'
export const READ_MODIFIERS_SUCCESS = 'inventory/modifiers/read_modifiers/READ_MODIFIERS_SUCCESS'
