import * as types from './types'


export const readModifiers = () => ({
  type: types.READ_MODIFIERS,
})


export const readModifiersFail = (data = {}, message = '', code = null) => ({
  type: types.READ_MODIFIERS_FAIL,
  errors: {
    data,
    message,
    code,
  },
})


export const readModifiersSuccess = modifiersData => ({
  type: types.READ_MODIFIERS_SUCCESS,
  modifiersData,
})
