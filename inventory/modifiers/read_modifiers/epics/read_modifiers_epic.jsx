// Core
import api from 'posModules/app/axios'
import camelcaseKeys from 'camelcase-keys'
import { Observable } from 'rxjs'
import { mergeMap } from 'rxjs/operators'
import { ofType } from 'redux-observable'
import normalize from 'posModules/app/helper'

// Types
import { READ_MODIFIERS } from '../types';

// Operations
import { readModifiersSuccess, readModifiersFail } from '../operations'


const readModifiersEpic = action$ => action$.pipe(
  ofType(READ_MODIFIERS),
  mergeMap(() => (
    Observable.create((observer) => {
      api.get('/v1/items/modifiers/').then((response) => {
        const modifiers = camelcaseKeys(response.data.data, { deep: true })
        const modifiersData = normalize(modifiers, 'id')

        console.log(modifiersData)

        observer.next(readModifiersSuccess(modifiersData))
        observer.complete()
      }).catch((error) => {
        const errorResponse = camelcaseKeys(error.response.data.errors, { deep: true })

        const errors = {
          data: errorResponse.data || {},
          message: errorResponse.message,
          code: errorResponse.code,
        }

        observer.next(readModifiersFail(errors.data, errors.message, errors.code))
        observer.complete()
      })
    })
  )),
)


export default readModifiersEpic
