// Core
import reduceReducers from 'reduce-reducers'

// Action types
import * as types from './types'


const readModifiersReducer = (state, action) => {
  switch (action.type) {
    case types.READ_MODIFIERS:
      return {
        ...state,

        modifiersData: {
          ...state.modifiersData,

          error: {
            message: '',
            data: {},
            code: null,
          },

          readingModifiers: true,
          readModifiersFailed: false,
          readModifiersSucceeded: false,
        },
      }
    case types.READ_MODIFIERS_FAIL:
      return {
        ...state,

        modifiersData: {
          ...state.modifiersData,

          error: {
            message: action.errors.message,
            data: action.errors.data,
            code: action.errors.code,
          },

          readingModifiers: false,
          readModifiersFailed: true,
          readModifiersSucceeded: false,
        },
      }
    case types.READ_MODIFIERS_SUCCESS:
      return {
        ...state,

        modifiersData: {
          ...state.modifiersData,

          modifiers: {
            result: action.modifiersData.result,
            data: action.modifiersData.data,
          },

          error: {
            message: '',
            data: {},
            code: null,
          },

          readingModifiers: false,
          readModifiersFailed: false,
          readModifiersSucceeded: true,
        },
      }
    default:
      return state
  }
}


export default reduceReducers(readModifiersReducer)
