// Core
import api from 'posModules/app/axios'
import { Observable } from 'rxjs'
import { mergeMap, debounceTime } from 'rxjs/operators'
import { ofType } from 'redux-observable'
import camelcaseKeys from 'camelcase-keys'

// Operations
import { updateModifierSuccess, updateModifierFail } from '../operations'

// Types
import { UPDATE_MODIFIER } from '../types';


const updateModifierEpic = action$ => action$.pipe(
  ofType(UPDATE_MODIFIER),
  debounceTime(1000),
  mergeMap(action => (
    Observable.create((observer) => {
      const formErrors = {}

      if (action.name.trim() === '') {
        formErrors.name = 'name is required'
      }

      if (Object.getOwnPropertyNames(formErrors).length > 0) {
        observer.next(updateModifierFail(formErrors))
        observer.complete()
        return
      }

      api.put(`/v1/items/modifiers/${action.modifierId}`, { name: action.name }).then((response) => {
        const modifier = camelcaseKeys(response.data.data, { deep: true })

        observer.next(updateModifierSuccess(modifier))
        observer.complete()
      }).catch((error) => {
        const errorResponse = camelcaseKeys(error.response.data.errors, { deep: true })

        const errors = {
          data: errorResponse.data || {},
          message: errorResponse.message,
          code: errorResponse.code,
        }

        if (errorResponse.code === 422) {
          errors.data = errorResponse.data.modifier_form
        }


        observer.next(updateModifierFail(errors.data, errors.message, errors.code))
        observer.complete()
      })
    })
  )),
)

export default updateModifierEpic
