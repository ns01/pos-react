// Core
import reduceReducers from 'reduce-reducers'

// Action types
import * as types from './types'


const updateModifierReducer = (state, action) => {
  switch (action.type) {
    case types.UPDATE_MODIFIER:
      return {
        ...state,

        updateModifierForm: {
          ...state.updateModifierForm,

          error: {
            message: '',
            data: {},
            code: null,
          },

          updatingModifier: true,
          updateModifierFailed: false,
          updateModifierSucceeded: false,
        },
      }
    case types.UPDATE_MODIFIER_FAIL: {
      return {
        ...state,

        updateModifierForm: {
          ...state.updateModifierForm,

          error: {
            message: action.error.message,
            data: action.error.data,
            code: action.error.code,
          },

          updatingModifier: false,
          updateModifierFailed: true,
          updateModifierSucceeded: false,
        },
      }
    }
    case types.UPDATE_MODIFIER_SUCCESS: {
      return {
        ...state,

        modifiersData: {
          ...state.modifiersData,

          modifiers: {
            ...state.modifiersData.modifiers,

            data: {
              ...state.modifiersData.modifiers.data,

              [action.modifier.id]: action.modifier,
            },
          },
        },

        updateModifierForm: {
          ...state.updateModifierForm,

          error: {
            message: '',
            data: {},
            code: null,
          },

          updatingModifier: false,
          updateModifierFailed: false,
          updateModifierSucceeded: true,
        },
      }
    }
    case types.UPDATE_MODIFIER_RESET:
      return {
        ...state,

        updateModifierForm: {
          isUpdateModifierDialogOpened: false,

          modifier: {},
          name: '',

          error: {
            message: '',
            data: {},
            code: null,
          },

          updatingModifier: false,
          updateModifierFailed: false,
          updateModifierSucceeded: false,
        },
      }
    case types.SET_MODIFIER:
      return {
        ...state,

        updateModifierForm: {
          ...state.updateModifierForm,

          modifier: action.modifier,
        },
      }
    case types.CHANGE_FORM_NAME:
      return {
        ...state,

        updateModifierForm: {
          ...state.updateModifierForm,

          name: action.name,
        },
      }
    case types.OPEN_UPDATE_MODIFIER_DIALOG:
      return {
        ...state,

        updateModifierForm: {
          ...state.updateModifierForm,

          isUpdateModifierDialogOpened: true,
        },
      }
    case types.CLOSE_UPDATE_MODIFIER_DIALOG:
      return {
        ...state,

        updateModifierForm: {
          ...state.updateModifierForm,

          isUpdateModifierDialogOpened: false,
        },
      }
    default:
      return state
  }
}


export default reduceReducers(updateModifierReducer)
