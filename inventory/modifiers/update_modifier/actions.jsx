import * as types from './types'


export const updateModifier = (modifierId, name) => ({
  type: types.UPDATE_MODIFIER,
  modifierId,
  name,
})

export const updateModifierFail = (data = {}, message = '', code = null) => ({
  type: types.UPDATE_MODIFIER_FAIL,
  error: {
    data,
    message,
    code,
  },
})

export const updateModifierSuccess = modifier => ({
  type: types.UPDATE_MODIFIER_SUCCESS,
  modifier,
})

export const updateModifierReset = () => ({
  type: types.UPDATE_MODIFIER_RESET,
})

export const setModifier = modifier => ({
  type: types.SET_MODIFIER,
  modifier,
})

export const changeFormName = name => ({
  type: types.CHANGE_FORM_NAME,
  name,
})

export const openUpdateModifierDialog = () => ({
  type: types.OPEN_UPDATE_MODIFIER_DIALOG,
})

export const closeUpdateModifierDialog = () => ({
  type: types.CLOSE_UPDATE_MODIFIER_DIALOG,
})
