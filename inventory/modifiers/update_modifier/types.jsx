export const UPDATE_MODIFIER = 'inventory/modifiers/update_modifier/UPDATE_MODIFIER'
export const UPDATE_MODIFIER_FAIL = 'inventory/modifiers/update_modifier/UPDATE_MODIFIER_FAIL'
export const UPDATE_MODIFIER_SUCCESS = 'inventory/modifiers/update_modifier/UPDATE_MODIFIER_SUCCESS'
export const UPDATE_MODIFIER_RESET = 'inventory/modifiers/update_modifier/UPDATE_MODIFIER_RESET'

// Input
export const CHANGE_FORM_NAME = 'inventory/modifiers/update_modifier/CHANGE_FORM_NAME'
export const SET_MODIFIER = 'inventory/modifiers/update_modifier/SET_MODIFIER'

// Dialog
export const OPEN_UPDATE_MODIFIER_DIALOG = 'inventory/modifiers/update_modifier/OPEN_UPDATE_MODIFIER_DIALOG'
export const CLOSE_UPDATE_MODIFIER_DIALOG = 'inventory/modifiers/update_modifier/CLOSE_UPDATE_MODIFIER_DIALOG'
