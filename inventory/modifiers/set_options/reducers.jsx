// Core
import reduceReducers from 'reduce-reducers'

// Action types
import * as types from './types'


const setOptionsReducer = (state, action) => {
  switch (action.type) {
    case types.SET_OPTIONS_RESET:
      return {
        ...state,

        setOptionsForm: {
          ...state.setOptionsForm,

          modifier: {},
          name: '',
          price: '',
        },
      }
    case types.SET_MODIFIER:
      return {
        ...state,

        setOptionsForm: {
          ...state.setOptionsForm,

          modifier: action.modifier,
        },
      }
    case types.CHANGE_FORM_NAME:
      return {
        ...state,

        setOptionsForm: {
          ...state.setOptionsForm,

          name: action.name,
        },
      }
    case types.CHANGE_FORM_PRICE:
      return {
        ...state,

        setOptionsForm: {
          ...state.setOptionsForm,

          price: action.price,
        },
      }
    case types.OPEN_SET_OPTIONS_DIALOG:
      return {
        ...state,

        setOptionsForm: {
          ...state.setOptionsForm,

          isSetOptionsDialogOpened: true,
        },
      }
    case types.CLOSE_SET_OPTIONS_DIALOG:
      return {
        ...state,

        setOptionsForm: {
          ...state.setOptionsForm,

          isSetOptionsDialogOpened: false,
        },
      }
    default:
      return state
  }
}


export default reduceReducers(setOptionsReducer)
