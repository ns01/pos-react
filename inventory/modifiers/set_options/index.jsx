import reducer from './reducers'

import * as setOptionsOperations from './operations'


export { setOptionsOperations }

export default reducer
