// Inputs
export const SET_OPTIONS_RESET = 'inventory/modifiers/set_options/SET_OPTIONS_RESET'

export const SET_MODIFIER = 'inventory/modifiers/set_options/SET_MODIFIER'

export const CHANGE_FORM_NAME = 'inventory/modifiers/set_options/CHANGE_FORM_NAME'
export const CHANGE_FORM_PRICE = 'inventory/modifiers/set_options/CHANGE_FORM_PRICE'

// Dialog
export const OPEN_SET_OPTIONS_DIALOG = 'inventory/modifiers/set_options/OPEN_SET_OPTIONS_DIALOG'
export const CLOSE_SET_OPTIONS_DIALOG = 'inventory/modifiers/set_options/CLOSE_SET_OPTIONS_DIALOG'
