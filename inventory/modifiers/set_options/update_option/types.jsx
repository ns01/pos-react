export const UPDATE_OPTION = 'inventory/modifiers/set_options/update_option/UPDATE_OPTION'
export const UPDATE_OPTION_FAIL = 'inventory/modifiers/set_options/update_option/UPDATE_OPTION_FAIL'
export const UPDATE_OPTION_SUCCESS = 'inventory/modifiers/set_options/update_option/UPDATE_OPTION_SUCCESS'
export const UPDATE_OPTION_RESET = 'inventory/modifiers/set_options/update_option/UPDATE_OPTION_RESET'

export const SET_OPTION = 'inventory/modifiers/set_options/update_option/SET_OPTION'

export const OPEN_UPDATE_OPTION_DIALOG = 'inventory/modifiers/set_options/update_option/OPEN_UPDATE_OPTION_DIALOG'
export const CLOSE_UPDATE_OPTION_DIALOG = 'inventory/modifiers/set_options/update_option/CLOSE_UPDATE_OPTION_DIALOG'
