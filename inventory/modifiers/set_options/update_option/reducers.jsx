// Core
import reduceReducers from 'reduce-reducers'

// Action types
import * as types from './types'


const updateOptionReducer = (state, action) => {
  switch (action.type) {
    case types.UPDATE_OPTION:
      return {
        ...state,

        updateOptionForm: {
          ...state.updateOptionForm,

          error: {
            message: '',
            data: {},
            code: null,
          },

          updatingOption: true,
          updateOptionFailed: false,
          updateOptionSucceeded: false,
        },
      }
    case types.UPDATE_OPTION_FAIL:
      return {
        ...state,

        updateOptionForm: {
          ...state.updateOptionForm,

          error: {
            message: action.error.message,
            data: action.error.data,
            code: action.error.code,
          },

          updatingOption: false,
          updateOptionFailed: true,
          updateOptionSucceeded: false,
        },
      }
    case types.UPDATE_OPTION_SUCCESS:
      return {
        ...state,

        optionsData: {
          ...state.optionsData,

          options: {
            ...state.optionsData.options,

            data: {
              ...state.optionsData.options.data,

              [action.option.id]: action.option,
            },
          },
        },

        updateOptionForm: {
          error: {
            message: '',
            data: {},
            code: null,
          },

          updatingOption: false,
          updateOptionFailed: false,
          updateOptionSucceeded: true,
        },
      }
    case types.UPDATE_OPTION_RESET:
      return {
        ...state,

        updateOptionForm: {
          isUpdateOptionDialogOpened: false,

          option: {},

          error: {
            message: '',
            data: {},
            code: null,
          },

          updatingOption: false,
          updateOptionFailed: false,
          updateOptionSucceeded: false,
        },
      }
    case types.SET_OPTION:
      return {
        ...state,

        updateOptionForm: {
          ...state.updateOptionsForm,

          option: action.option,
        },
      }
    case types.OPEN_UPDATE_OPTION_DIALOG:
      return {
        ...state,

        updateOptionForm: {
          ...state.updateOptionForm,

          isUpdateOptionDialogOpened: true,
        },
      }
    case types.CLOSE_UPDATE_OPTION_DIALOG:
      return {
        ...state,

        updateOptionForm: {
          ...state.updateOptionForm,

          isUpdateOptionDialogOpened: false,
        },
      }
    default:
      return state
  }
}


export default reduceReducers(updateOptionReducer)
