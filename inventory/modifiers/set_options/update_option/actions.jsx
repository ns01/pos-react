import * as types from './types'


export const updateOption = (
  modifierId,
  optionId,
  name,
  price,
) => ({
  type: types.UPDATE_OPTION,
  modifierId,
  optionId,
  name,
  price,
})

export const updateOptionFail = (data = {}, message = '', code = null) => ({
  type: types.UPDATE_OPTION_FAIL,
  error: {
    data,
    message,
    code,
  },
})

export const updateOptionSuccess = option => ({
  type: types.UPDATE_OPTION_SUCCESS,
  option,
})

export const updateOptionReset = () => ({
  type: types.UPDATE_OPTION_RESET,
})

export const setOption = option => ({
  type: types.SET_OPTION,
  option,
})

export const openUpdateOptionDialog = () => ({
  type: types.OPEN_UPDATE_OPTION_DIALOG,
})

export const closeUpdateOptionDialog = () => ({
  type: types.CLOSE_UPDATE_OPTION_DIALOG,
})
