// Core
import api from 'posModules/app/axios'
import { Observable } from 'rxjs'
import { mergeMap, debounceTime } from 'rxjs/operators'
import { ofType } from 'redux-observable'
import camelcaseKeys from 'camelcase-keys'

// Operations
import { updateOptionSuccess, updateOptionFail } from '../operations'

// Types
import { UPDATE_OPTION } from '../types';


const updateOptionEpic = action$ => action$.pipe(
  ofType(UPDATE_OPTION),
  debounceTime(1000),
  mergeMap(action => (
    Observable.create((observer) => {
      const formErrors = {}

      if (action.name.trim() === '') {
        formErrors.name = 'name is required'
      }

      if (Object.getOwnPropertyNames(formErrors).length > 0) {
        observer.next(updateOptionFail(formErrors))
        observer.complete()
        return
      }

      api.put(`/v1/items/modifiers/${action.modifierId}/options/${action.optionId}`, {
        option_name: action.name,
        price: action.price || 0,
      }).then((response) => {
        const option = camelcaseKeys(response.data.data, { deep: true })

        observer.next(updateOptionSuccess(option))
        observer.complete()
      }).catch((error) => {
        const errorResponse = camelcaseKeys(error.response.data.errors, { deep: true })

        const errors = {
          data: errorResponse.data || {},
          message: errorResponse.message,
          code: errorResponse.code,
        }

        observer.next(updateOptionFail(errors.data, errors.message, errors.code))
        observer.complete()
      })
    })
  )),
)

export default updateOptionEpic
