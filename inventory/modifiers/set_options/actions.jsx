import * as types from './types'

export const setOptionsReset = () => ({
  type: types.SET_OPTIONS_RESET,
})

export const setModifier = modifier => ({
  type: types.SET_MODIFIER,
  modifier,
})

export const changeFormName = name => ({
  type: types.CHANGE_FORM_NAME,
  name,
})

export const changeFormPrice = price => ({
  type: types.CHANGE_FORM_PRICE,
  price,
})

export const openSetOptionsDialog = () => ({
  type: types.OPEN_SET_OPTIONS_DIALOG,
})

export const closeSetOptionsDialog = () => ({
  type: types.CLOSE_SET_OPTIONS_DIALOG,
})
