// Image
export const CREATE_OPTION = 'inventory/modifiers/set_options/create_option/CREATE_OPTION'
export const CREATE_OPTION_FAIL = 'inventory/modifiers/set_options/create_option/CREATE_OPTION_FAIL'
export const CREATE_OPTION_SUCCESS = 'inventory/modifiers/set_options/create_option/CREATE_OPTION_SUCCESS'
export const CREATE_OPTION_RESET = 'inventory/modifiers/set_options/create_option/CREATE_OPTION_RESET'

export const OPEN_ADD_OPTION_DIALOG = 'inventory/modifiers/set_options/create_option/OPEN_ADD_OPTION_DIALOG'
export const CLOSE_ADD_OPTION_DIALOG = 'inventory/modifiers/set_options/create_option/CLOSE_ADD_OPTION_DIALOG'
