import * as types from './types'


export const createOption = (
  modifierId,
  name,
  price,
) => ({
  type: types.CREATE_OPTION,
  modifierId,
  name,
  price,
})

export const createOptionFail = (data = {}, message = '', code = null) => ({
  type: types.CREATE_OPTION_FAIL,
  error: {
    data,
    message,
    code,
  },
})

export const createOptionSuccess = option => ({
  type: types.CREATE_OPTION_SUCCESS,
  option,
})

export const createOptionReset = () => ({
  type: types.CREATE_OPTION_RESET,
})

export const openAddOptionDialog = () => ({
  type: types.OPEN_ADD_OPTION_DIALOG,
})

export const closeAddOptionDialog = () => ({
  type: types.CLOSE_ADD_OPTION_DIALOG,
})
