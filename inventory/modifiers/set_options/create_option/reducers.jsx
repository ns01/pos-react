// Core
import reduceReducers from 'reduce-reducers'

// Action types
import * as types from './types'


const createOptionReducer = (state, action) => {
  switch (action.type) {
    case types.CREATE_OPTION:
      return {
        ...state,

        createOptionForm: {
          ...state.createOptionForm,

          error: {
            message: '',
            data: {},
            code: null,
          },

          creatingOption: true,
          createOptionFailed: false,
          createOptionSucceeded: false,
        },
      }
    case types.CREATE_OPTION_FAIL:
      return {
        ...state,

        createOptionForm: {
          ...state.createOptionForm,

          error: {
            message: action.error.message,
            data: action.error.data,
            code: action.error.code,
          },

          creatingOption: false,
          createOptionFailed: true,
          createOptionSucceeded: false,
        },
      }
    case types.CREATE_OPTION_SUCCESS:
      return {
        ...state,

        optionsData: {
          ...state.optionsData,

          options: {
            ...state.optionsData.options,

            data: {
              ...state.optionsData.options.data,

              [action.option.id]: action.option,
            },
          },
        },

        createOptionForm: {
          ...state.createOptionForm,

          error: {
            message: '',
            data: {},
            code: null,
          },

          creatingOption: false,
          createOptionFailed: false,
          createOptionSucceeded: true,
        },
      }
    case types.CREATE_OPTION_RESET:
      return {
        ...state,

        createOptionForm: {
          isAddOptionDialogOpened: false,

          error: {
            message: '',
            data: {},
            code: null,
          },

          creatingOption: false,
          createOptionFailed: false,
          createOptionSucceeded: false,
        },
      }
    case types.OPEN_ADD_OPTION_DIALOG:
      return {
        ...state,

        createOptionForm: {
          ...state.createOptionForm,

          isAddOptionDialogOpened: true,
        },
      }
    case types.CLOSE_ADD_OPTION_DIALOG:
      return {
        ...state,

        createOptionForm: {
          ...state.createOptionForm,

          isAddOptionDialogOpened: false,
        },
      }
    default:
      return state
  }
}


export default reduceReducers(createOptionReducer)
