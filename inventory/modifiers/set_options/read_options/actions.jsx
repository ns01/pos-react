import * as types from './types'


export const readOptions = modifierId => ({
  type: types.READ_OPTIONS,
  modifierId,
})


export const readOptionsFail = (data = {}, message = '', code = null) => ({
  type: types.READ_OPTIONS_FAIL,
  errors: {
    data,
    message,
    code,
  },
})


export const readOptionsSuccess = optionsData => ({
  type: types.READ_OPTIONS_SUCCESS,
  optionsData,
})
