// Core
import reduceReducers from 'reduce-reducers'

// Action types
import * as types from './types'


const readOptionsReducer = (state, action) => {
  switch (action.type) {
    case types.READ_OPTIONS:
      return {
        ...state,

        optionsData: {
          ...state.optionsData,

          error: {
            message: '',
            data: {},
            code: null,
          },

          readingOptions: true,
          readOptionsFailed: false,
          readOptionsSucceeded: false,
        },
      }
    case types.READ_OPTIONS_FAIL:
      return {
        ...state,

        optionsData: {
          ...state.optionsData,

          error: {
            message: action.errors.message,
            data: action.errors.data,
            code: action.errors.code,
          },

          readingOptions: false,
          readOptionsFailed: true,
          readOptionsSucceeded: false,
        },
      }
    case types.READ_OPTIONS_SUCCESS:
      return {
        ...state,

        optionsData: {
          ...state.optionsData,

          options: {
            result: action.optionsData.result,
            data: action.optionsData.data,
          },

          error: {
            message: '',
            data: {},
            code: null,
          },

          readingOptions: false,
          readOptionsFailed: false,
          readOptionsSucceeded: true,
        },
      }
    default:
      return state
  }
}


export default reduceReducers(readOptionsReducer)
