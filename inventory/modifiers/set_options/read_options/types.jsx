export const READ_OPTIONS = 'inventory/modifiers/set_options/read_options/READ_OPTIONS'
export const READ_OPTIONS_FAIL = 'inventory/modifiers/set_options/read_options/READ_OPTIONS_FAIL'
export const READ_OPTIONS_SUCCESS = 'inventory/modifiers/set_options/read_options/READ_OPTIONS_SUCCESS'
