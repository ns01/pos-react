// Core
import api from 'posModules/app/axios'
import { Observable } from 'rxjs'
import { mergeMap } from 'rxjs/operators'
import { ofType } from 'redux-observable'
import normalize from 'posModules/app/helper'
import camelcaseKeys from 'camelcase-keys'

// Types
import { READ_OPTIONS } from '../types';

// Operations
import { readOptionsSuccess, readOptionsFail } from '../operations'


const readOptionsEpic = action$ => action$.pipe(
  ofType(READ_OPTIONS),
  mergeMap(action => (
    Observable.create((observer) => {
      api.get(`/v1/items/modifiers/${action.modifierId}/options`).then((response) => {
        const options = camelcaseKeys(response.data.data, { deep: true })
        const optionsData = normalize(options, 'id')

        // stringify number values
        const data = {}

        optionsData.result.forEach((optionId) => {
          const price = `${optionsData.data[optionId].price}`

          data[optionId] = {
            ...optionsData.data[optionId],
            price,
          }
        })

        optionsData.data = data

        observer.next(readOptionsSuccess(optionsData))
        observer.complete()
      }).catch((error) => {
        const errorResponse = camelcaseKeys(error.response.data.errors, { deep: true })

        const errors = {
          data: errorResponse.data || {},
          message: errorResponse.message,
          code: errorResponse.code,
        }

        observer.next(readOptionsFail(errors.data, errors.message, errors.code))
        observer.complete()
      })
    })
  )),
)


export default readOptionsEpic
