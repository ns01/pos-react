// Core
import reduceReducers from 'reduce-reducers'

// Action types
import * as types from './types'


const deleteOptionReducer = (state, action) => {
  switch (action.type) {
    case types.DELETE_OPTION:
      return {
        ...state,

        deleteOptionForm: {
          ...state.deleteOptionForm,

          error: {
            message: '',
            data: {},
            code: null,
          },

          deletingOption: true,
          deleteOptionFailed: false,
          deleteOptionSucceeded: false,
        },
      }
    case types.DELETE_OPTION_FAIL:
      return {
        ...state,

        deleteOptionForm: {
          ...state.deleteOptionForm,

          error: {
            message: action.error.message,
            data: action.error.data,
            code: action.error.code,
          },

          deletingOption: false,
          deleteOptionFailed: true,
          deleteOptionSucceeded: false,
        },
      }
    case types.DELETE_OPTION_SUCCESS: {
      const { data, result: optionsResult } = state.optionsData.options
      delete data[action.optionId]

      const result = optionsResult.filter(optionId => optionId !== action.optionId)

      return {
        ...state,

        optionsData: {
          ...state.optionsData,

          options: {
            data,
            result,
          },
        },

        deleteOptionForm: {
          ...state.deleteOptionForm,

          error: {
            message: '',
            data: {},
            code: null,
          },

          deletingOption: false,
          deleteOptionFailed: false,
          deleteOptionSucceeded: true,
        },
      }
    }
    case types.DELETE_OPTION_RESET:
      return {
        ...state,

        deleteOptionForm: {
          isRemoveOptionDialogOpened: false,
          option: {},

          error: {
            message: '',
            data: {},
            code: null,
          },

          deletingOption: false,
          deleteOptionFailed: false,
          deleteOptionSucceeded: false,
        },
      }
    case types.SET_OPTION:
      return {
        ...state,

        deleteOptionForm: {
          ...state.deleteOptionsForm,

          option: action.option,
        },
      }
    case types.OPEN_REMOVE_OPTION_DIALOG:
      return {
        ...state,

        deleteOptionForm: {
          ...state.deleteOptionForm,

          isRemoveOptionDialogOpened: true,
        },
      }
    case types.CLOSE_REMOVE_OPTION_DIALOG:
      return {
        ...state,

        deleteOptionForm: {
          ...state.deleteOptionForm,

          isRemoveOptionDialogOpened: false,
        },
      }
    default:
      return state
  }
}


export default reduceReducers(deleteOptionReducer)
