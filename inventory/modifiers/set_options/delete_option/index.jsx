import reducer from './reducers'

import * as deleteOptionOperations from './operations'


export { deleteOptionOperations }

export default reducer
