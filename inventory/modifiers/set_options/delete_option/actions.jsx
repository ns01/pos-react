import * as types from './types'

export const deleteOption = (modifierId, optionId) => ({
  type: types.DELETE_OPTION,
  modifierId,
  optionId,
})

export const deleteOptionFail = (data = {}, message = '', code = null) => ({
  type: types.DELETE_OPTION_FAIL,
  error: {
    data,
    message,
    code,
  },
})

export const deleteOptionSuccess = optionId => ({
  type: types.DELETE_OPTION_SUCCESS,
  optionId,
})

export const deleteOptionReset = () => ({
  type: types.DELETE_OPTION_RESET,
})

export const setOption = option => ({
  type: types.SET_OPTION,
  option,
})

export const openRemoveOptionDialog = () => ({
  type: types.OPEN_REMOVE_OPTION_DIALOG,
})

export const closeRemoveOptionDialog = () => ({
  type: types.CLOSE_REMOVE_OPTION_DIALOG,
})
