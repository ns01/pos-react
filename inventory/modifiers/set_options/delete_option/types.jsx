export const DELETE_OPTION = 'inventory/modifiers/set_options/delete_option/DELETE_OPTION'
export const DELETE_OPTION_FAIL = 'inventory/modifiers/set_options/delete_option/DELETE_OPTION_FAIL'
export const DELETE_OPTION_SUCCESS = 'inventory/modifiers/set_options/delete_option/DELETE_OPTION_SUCCESS'
export const DELETE_OPTION_RESET = 'inventory/modifiers/set_options/delete_option/DELETE_OPTION_RESET'

export const SET_OPTION = 'inventory/modifiers/set_options/delete_option/SET_OPTION'

export const OPEN_REMOVE_OPTION_DIALOG = 'inventory/modifiers/set_options/delete_option/OPEN_REMOVE_OPTION_DIALOG'
export const CLOSE_REMOVE_OPTION_DIALOG = 'inventory/modifiers/set_options/delete_option/CLOSE_REMOVE_OPTION_DIALOG'
