// Core
import api from 'posModules/app/axios'
import { Observable } from 'rxjs'
import { mergeMap, debounceTime } from 'rxjs/operators'
import { ofType } from 'redux-observable'
import camelcaseKeys from 'camelcase-keys'

// Operations
import { deleteOptionSuccess, deleteOptionFail } from '../operations'

// Types
import { DELETE_OPTION } from '../types';


const deleteOptionEpic = action$ => action$.pipe(
  ofType(DELETE_OPTION),
  debounceTime(1000),
  mergeMap(action => (
    Observable.create((observer) => {
      api.delete(`v1/items/modifiers/${action.modifierId}/options/${action.optionId}`).then(() => {
        observer.next(deleteOptionSuccess(action.optionId))
        observer.complete()
      }).catch((error) => {
        console.log(error)

        const errorResponse = camelcaseKeys(error.response.data.errors, { deep: true })

        const errors = {
          data: errorResponse.data || {},
          message: errorResponse.message,
          code: errorResponse.code,
        }

        observer.next(deleteOptionFail(errors.data, errors.message, errors.code))
        observer.complete()
      })
    })
  )),
)

export default deleteOptionEpic
