// Core
import api from 'posModules/app/axios'
import { Observable } from 'rxjs'
import { mergeMap } from 'rxjs/operators'
import { ofType } from 'redux-observable'
import normalize from 'posModules/app/helper'
import camelcaseKeys from 'camelcase-keys'

// Types
import { READ_USER } from '../types';

// Operations
import { readUserSuccess, readUserFail } from '../operations'


const readUserEpic = action$ => action$.pipe(
  ofType(READ_USER),
  mergeMap(() => (
    Observable.create((observer) => {
      api.get('/v1/user/').then((response) => {
        const userData = camelcaseKeys(response.data.data, { deep: true })

        observer.next(readUserSuccess(userData))
        observer.complete()
      }).catch((error) => {
        const errorResponse = camelcaseKeys(error.response.data.errors, { deep: true })

        const errors = {
          data: errorResponse.data || {},
          message: errorResponse.message,
          code: errorResponse.code,
        }

        observer.next(readUserFail(errors.data, errors.message, errors.code))
        observer.complete()
      })
    })
  )),
)


export default readUserEpic
