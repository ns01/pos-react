import * as types from './types'


export const readUser = () => ({
  type: types.READ_USER,
})


export const readUserFail = (data = {}, message = '', code = null) => ({
  type: types.READ_USER_FAIL,
  errors: {
    data,
    message,
    code,
  },
})


export const readUserSuccess = user => ({
  type: types.READ_USER_SUCCESS,
  user,
})
