// Core
import reduceReducers from 'reduce-reducers'

// Action types
import * as types from './types'


const readUserReducer = (state, action) => {
  switch (action.type) {
    case types.READ_USER:
      return {
        ...state,

        userData: {
          ...state.userData,

          error: {
            message: '',
            data: {},
            code: null,
          },

          readingUser: true,
          readUserFailed: false,
          readUserSucceeded: false,
        },
      }
    case types.READ_USER_FAIL:
      return {
        ...state,

        userData: {
          ...state.userData,

          error: {
            message: action.errors.message,
            data: action.errors.data,
            code: action.errors.code,
          },

          readingUser: false,
          readUserFailed: true,
          readUserSucceeded: false,
        },
      }
    case types.READ_USER_SUCCESS:
      return {
        ...state,

        userData: {
          ...state.userData,

          user: action.user,

          error: {
            message: '',
            data: {},
            code: null,
          },

          readingUser: false,
          readUserFailed: false,
          readUserSucceeded: true,
        },
      }
    default:
      return state
  }
}


export default reduceReducers(readUserReducer)
