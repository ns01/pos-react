export const READ_USER = 'inventory/user/read_user/READ_USER'
export const READ_USER_FAIL = 'inventory/user/read_user/READ_USER_FAIL'
export const READ_USER_SUCCESS = 'inventory/user/read_user/READ_USER_SUCCESS'
