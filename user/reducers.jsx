// Core
import reduceReducers from 'reduce-reducers'

// Reducers
import readUserReducer from './read_user'


// State shape
const initialState = {
  userData: {
    // Data
    user: {},

    // Error
    error: {
      message: '',
      data: {},
      code: null,
    },

    // Status
    readingUser: false,
    readUserFailed: false,
    readUserSucceeded: false,
  },
}

// Initial reducer
const initialReducer = (state = initialState) => (
  state
)

export default reduceReducers(
  initialReducer,
  readUserReducer,
)
