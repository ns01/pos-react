// Core
import reduceReducers from 'reduce-reducers'

// Auth
import authReducer from 'posModules/auth'

// POS
import posCustomersReducer from 'posModules/pos/customers'

// Inventory
import inventoryItemsReducer from 'posModules/inventory/items'
import inventoryCategoriesReducer from 'posModules/inventory/categories'
import inventoryModifiersReducer from 'posModules/inventory/modifiers'
import inventoryDiscountsReducer from 'posModules/inventory/discounts'

// Settings
import settingsPaymentsReducer from 'posModules/settings/payments'
import settingsTaxesReducer from 'posModules/settings/taxes'
import settingsGeneralReducer from 'posModules/settings/general'

// Company
import companiesReducer from 'posModules/company/companies'

// User
import userReducer from 'posModules/user'

// Types
import { LOGOUT } from 'posModules/auth/logout/types'


// State shape
const initialState = {}

// Initial reducer
const appReducer = (state = initialState, action) => {
  switch (action.type) {
    case LOGOUT:
      return {}
    default: return state
  }
}


export default reduceReducers(
  appReducer,

  authReducer,

  posCustomersReducer,

  inventoryItemsReducer,
  inventoryCategoriesReducer,
  inventoryModifiersReducer,
  inventoryDiscountsReducer,
  settingsPaymentsReducer,
  settingsTaxesReducer,
  settingsGeneralReducer,

  userReducer,

  companiesReducer,
)
