// Core
import { combineEpics } from 'redux-observable'

// Auth
import registerEpic from 'posModules/auth/register/epics/register_epic'
import validateEmailEpic from 'posModules/auth/register/epics/validate_email_epic'
import verifyEpic from 'posModules/auth/verify/epics/verify_epic'
import resendVerificationCodeEpic from 'posModules/auth/resend_verification_code/epics/resend_verification_code_epic'
import logoutEpic from 'posModules/auth/logout/epics/logout_epic'
import loginEpic from 'posModules/auth/login/epics/login_epic'


// POS
// Customers
import createCustomerEpic from 'posModules/pos/customers/create_customer/epics/create_customer_epic'
import readCustomersEpic from 'posModules/pos/customers/read_customers/epics/read_customers_epic'
import updateCustomerEpic from 'posModules/pos/customers/update_customer/epics/update_customer_epic'
import deleteCustomerEpic from 'posModules/pos/customers/delete_customer/epics/delete_customer_epic'

// Inventory
// Items
import createItemEpic from 'posModules/inventory/items/create_item/epics/create_item_epic'
import readItemsEpic from 'posModules/inventory/items/read_items/epics/read_items_epic'
import updateItemEpic from 'posModules/inventory/items/update_item/epics/update_item_epic'
import deleteItemEpic from 'posModules/inventory/items/delete_item/epics/delete_item_epic'
import updatePOSRepresentationSymbolColorEpic from 'posModules/inventory/items/set_pos_representation/update_symbol_color/epics/update_symbol_color_epic'
import createPOSRepresentationImageEpic from 'posModules/inventory/items/set_pos_representation/create_image/epics/create_image_epic'
import updatePOSRepresentationImageEpic from 'posModules/inventory/items/set_pos_representation/update_image/epics/update_image_epic'
import deletePOSRepresentationImageEpic from 'posModules/inventory/items/set_pos_representation/delete_image/epics/delete_image_epic'

// Categories
import createCategoryEpic from 'posModules/inventory/categories/create_category/epics/create_category_epic'
import readCategoriesEpic from 'posModules/inventory/categories/read_categories/epics/read_categories_epic'
import updateCategoryEpic from 'posModules/inventory/categories/update_category/epics/update_category_epic'
import deleteCategoryEpic from 'posModules/inventory/categories/delete_category/epics/delete_category_epic'

// Modifiers
import createModifierEpic from 'posModules/inventory/modifiers/create_modifier/epics/create_modifier_epic'
import readModifiersEpic from 'posModules/inventory/modifiers/read_modifiers/epics/read_modifiers_epic'
import updateModifierEpic from 'posModules/inventory/modifiers/update_modifier/epics/update_modifier_epic'
import deleteModifierEpic from 'posModules/inventory/modifiers/delete_modifier/epics/delete_modifier_epic'

// Modifier Options
import createOptionEpic from 'posModules/inventory/modifiers/set_options/create_option/epics/create_option_epic'
import readOptionsEpic from 'posModules/inventory/modifiers/set_options/read_options/epics/read_options_epic'
import updateOptionEpic from 'posModules/inventory/modifiers/set_options/update_option/epics/update_option_epic'
import deleteOptionEpic from 'posModules/inventory/modifiers/set_options/delete_option/epics/delete_option_epic'

// Discounts
import createDiscountEpic from 'posModules/inventory/discounts/create_discount/epics/create_discount_epic'
import readDiscountsEpic from 'posModules/inventory/discounts/read_discounts/epics/read_discounts_epic'
import updateDiscountEpic from 'posModules/inventory/discounts/update_discount/epics/update_discount_epic'
import deleteDiscountEpic from 'posModules/inventory/discounts/delete_discount/epics/delete_discount_epic'

// Payments
import createPaymentEpic from 'posModules/settings/payments/create_payment/epics/create_payment_epic'
import readPaymentsEpic from 'posModules/settings/payments/read_payments/epics/read_payments_epic'
import updatePaymentEpic from 'posModules/settings/payments/update_payment/epics/update_payment_epic'
import deletePaymentEpic from 'posModules/settings/payments/delete_payment/epics/delete_payment_epic'

// Taxes
import createTaxEpic from 'posModules/settings/taxes/create_tax/epics/create_tax_epic'
import readTaxesEpic from 'posModules/settings/taxes/read_taxes/epics/read_taxes_epic'
import updateTaxEpic from 'posModules/settings/taxes/update_tax/epics/update_tax_epic'
import deleteTaxEpic from 'posModules/settings/taxes/delete_tax/epics/delete_tax_epic'

// Settings
// General
import readUserEmailEpic from 'posModules/settings/general/read_user_email/epics/read_user_email_epic'
import updateUserEmailEpic from 'posModules/settings/general/update_user_email/epics/update_user_email_epic'
import updateBusinessNameEpic from 'posModules/settings/general/update_business_name/epics/update_business_name_epic'

// User
import readUserEpic from 'posModules/user/read_user/epics/read_user_epic'
// Company
import readCompaniesEpic from 'posModules/company/companies/read_companies/epics/read_companies_epic'

export default combineEpics(
  registerEpic,
  validateEmailEpic,
  verifyEpic,
  resendVerificationCodeEpic,
  logoutEpic,
  loginEpic,

  readUserEpic,

  createCustomerEpic,
  readCustomersEpic,
  updateCustomerEpic,
  deleteCustomerEpic,

  createItemEpic,
  readItemsEpic,
  updateItemEpic,
  deleteItemEpic,
  updatePOSRepresentationSymbolColorEpic,
  createPOSRepresentationImageEpic,
  updatePOSRepresentationImageEpic,
  deletePOSRepresentationImageEpic,

  createCategoryEpic,
  readCategoriesEpic,
  updateCategoryEpic,
  deleteCategoryEpic,

  createModifierEpic,
  readModifiersEpic,
  updateModifierEpic,
  deleteModifierEpic,

  createOptionEpic,
  readOptionsEpic,
  updateOptionEpic,
  deleteOptionEpic,

  createDiscountEpic,
  readDiscountsEpic,
  updateDiscountEpic,
  deleteDiscountEpic,

  createPaymentEpic,
  readPaymentsEpic,
  updatePaymentEpic,
  deletePaymentEpic,

  createTaxEpic,
  readTaxesEpic,
  updateTaxEpic,
  deleteTaxEpic,

  readUserEmailEpic,
  updateUserEmailEpic,

  updateBusinessNameEpic,

  readCompaniesEpic,

  // Pos
  // createCompanyEpic // for company
)
