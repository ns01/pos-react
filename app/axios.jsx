// Core
import axios from 'axios'
import configs from 'posConfigs'


const api = axios.create({
  baseURL: `${configs.url}/api`,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  },
})


export const setAuthorizationHeader = (jwtToken) => {
  api.defaults.headers.common.Authorization = `Bearer ${jwtToken}`

  if (jwtToken === null) {
    api.defaults.headers.common.Authorization = null
  }
}


export default api
