// Core
import { createStore, applyMiddleware, compose } from 'redux'
import { createEpicMiddleware } from 'redux-observable'

// Reducer
import reducer from './reducer'

// Epics
import epic from './epic'


const epicMiddleware = createEpicMiddleware()

/* eslint-disable no-underscore-dangle */
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
/* eslint-enable */

const store = createStore(
  reducer,
  composeEnhancers(applyMiddleware(epicMiddleware)),
)

epicMiddleware.run(epic)


export default store
