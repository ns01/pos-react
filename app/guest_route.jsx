// Core
import React from 'react'
import PropTypes from 'prop-types'
import { Route, Redirect } from 'react-router-dom'

// Store
import store from './store'


const GuestRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props => (
      store.getState().isAuthenticated ? (
        <Redirect
          to={{
            pathname: '/',
            from: props.location,
          }}
        />
      ) : (
        <Component {...props} />
      )
    )}
  />
)


GuestRoute.propTypes = {
  component: PropTypes.func.isRequired,
  location: PropTypes.shape({
    pathname: PropTypes.string.isRequired,
  }),
}


GuestRoute.defaultProps = {
  location: {
    pathname: '/',
  },
}

export default GuestRoute
