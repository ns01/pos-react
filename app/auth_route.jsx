// Core
import React from 'react'
import PropTypes from 'prop-types'
import { Route, Redirect } from 'react-router-dom'

// Store
import store from './store'

const AuthRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props => (
      store.getState().isAuthenticated
        ? <Component {...props} />
        : <Redirect to={{ pathname: '/login', from: props.location }} />
    )}
  />
)


AuthRoute.propTypes = {
  component: PropTypes.func.isRequired,
  location: PropTypes.shape({
    pathname: PropTypes.string.isRequired,
  }),
}


AuthRoute.defaultProps = {
  location: {
    pathname: '/',
  },
}


export default AuthRoute
